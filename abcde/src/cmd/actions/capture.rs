//! Functions that perform actions for the capture subcommand.

use std::io::Write;
// Import top-level objects from crate.
use crate::*;

/// Perform the capture action/command described by `opts`.
pub fn capture(opts: cmd::opts::CaptureOpts) -> Result<(), BoxError> {
    // Parse code.
    let code = parse_code(&opts)?;
    
    // Expand code.
    if opts.common.verbose {
        eprint!("Expanding code...");
        std::io::stderr().lock().flush()?;
    }
    let code = lang::ExpandedCode::new(code)?;
    if opts.common.verbose {
        eprintln!(" done.\nCode expanded as:");
        for stmt in code.code() {
            eprintln!("{}", stmt);
        }
    }
    
    // Validate code.
    if opts.common.verbose {
        eprint!("Validating code...");
        std::io::stderr().lock().flush()?;
    }
    let code = lang::ValidCode::new(code)?;
    if opts.common.verbose {
        eprintln!(" done.\nThere are {} select statements.\nThe requested queries are:", code.select_count());
        for query in code.queries() {
            eprintln!("{}", query);
        }
    }
    
    // Make sure the number of select statements == the number of outputs.
    {
        let error = cmd::Error{
            what: format!("Number of select statements in code {} is not equal to number of output files specified on command line {}.", code.select_count(), opts.capture.outputs.len()),
            source: None,
        };
        if code.select_count() == 1 {
            if opts.capture.outputs.len() > 1 {
                return Err(error.into());
            }
        }
        else if code.select_count() != opts.capture.outputs.len() {
            return Err(error.into());
        }
    }
    
    // Construct the intepreter, which will perform the queries.
    if opts.common.verbose {
        eprint!("Performing queries, this could take a while...");
        std::io::stderr().lock().flush()?;
    }
    let mut code = lang::Interpreter::new(code, &opts.common.data_dir)?;
    if opts.common.verbose {
        eprintln!(" done.");
    }
    
    // Evaluate each select statement.
    if opts.capture.outputs.is_empty() {
        if opts.common.verbose {
            eprint!("Writing output of select statement to stdout...");
            std::io::stderr().lock().flush()?;
        }
        
        // If there is just one select statement and no output files were
        // specified then output to stdout.
        if let Some(table) = code.evaluate()? {
            do_capture_output(&opts, table, std::io::stdout())?;
        }
        
        if opts.common.verbose {
            eprintln!(" done.");
        }
    }
    else {
        // Iterate over output files and output the next select statement to
        // each one.
        let len = opts.capture.outputs.len();
        for (index, output_file) in opts.capture.outputs.iter().enumerate() {
            if let Some(table) = code.evaluate()? {
                if opts.common.verbose {
                    eprint!("Writing output of select statement {} of {} to {}...", index+1, len, output_file.display());
                    std::io::stderr().lock().flush()?;
                }
        
                let of = std::fs::File::create(output_file)?;
                do_capture_output(&opts, table, of)?;
                
                if opts.common.verbose {
                    eprintln!(" done.");
                }
            }
        }
    }
    
    Ok(())
}

fn parse_code(opts: &cmd::opts::CaptureOpts) -> Result<lang::ast::Code, BoxError> {
    if opts.common.verbose {
        eprint!("Parsing code...");
        std::io::stderr().lock().flush()?;
    }
    
    // Get code.
    let mut buffer = String::new();
    let code = if let Some(code) = &opts.capture.code {
        code
    }
    else if let Some(code_file) = &opts.capture.code_file {
        let mut f = std::fs::File::open(code_file)?;
        f.read_to_string(&mut buffer)?;
        &buffer
    }
    else {
        return Err(cmd::Error{
            what: "Missing code or code-file.".into(),
            source: None,
        }.into());
    };
    
    // Parse.
    let code = lang::parser::CodeParser::new().parse(code);
    let code = match code {
        Ok(code) => code?,
        Err(lalr_error) =>
            return Err(lang::ast::ParseError::from(lalr_error).into()),
    };
    if opts.common.verbose {
        eprintln!(" done.\nCode parsed as:");
        for stmt in &code {
            eprintln!("{}", stmt);
        }
    }
    
    Ok(code)
}

fn do_capture_output<T: std::io::Write>(
    opts: &cmd::opts::CaptureOpts,
    table: lang::OutputTable,
    out: T)
-> Result<(), BoxError> {
    if opts.capture.fmt_json { do_capture_output_json(opts, table, out) }
    else { do_capture_output_csv(opts, table, out) }
}

fn do_capture_output_json<T: std::io::Write>(
    opts: &cmd::opts::CaptureOpts,
    table: lang::OutputTable,
    mut out: T)
-> Result<(), BoxError> {
    // Save types and names of headers so that we can infer types of missing values.
    let header: Vec<&lang::ast::Symbol> = table.header().collect();
    
    // Render json depending on what type was requested.
    match opts.capture.json_type.as_ref().unwrap_or(&cmd::opts::JsonType::Array) {
        cmd::opts::JsonType::Array => {
            // Create a root json object into which we will fold an array for each column.
            let mut json_object = serde_json::Map::new();
            
            // Create an empty array for each column in the header.
            for symbol in &header {
                json_object.insert(symbol.name.to_string(), serde_json::Value::Array(Vec::new()));
            }
            
            // Insert rows into json object.
            for row in table.rows() {
                let row = row?;
                let row = row.columns().enumerate();
                for (index, col) in row {
                    // Push row contents onto arrays.
                    let mut out: Vec<u8> = Vec::new();
                    write_cell(opts, header[index].kind.unwrap(), col, &mut out)?;
                    json_object.get_mut(&header[index].name.to_string()).unwrap().as_array_mut().unwrap().push(serde_json::from_slice(&out)?);
                    // TODO refactor to avoid multiple string allocations
                } 
            }
            
            // Write json to output.
            serde_json::to_writer(&mut out, &json_object)?;
        },
        cmd::opts::JsonType::Stream => {
            // Generate a separate json object for each row.
            for row in table.rows() {
                let row = row?;
                let row = row.columns().enumerate();
                let mut json_object = serde_json::Map::new();
                for (index, col) in row {
                    // Insert row contents into object.
                    let mut out: Vec<u8> = Vec::new();
                    write_cell(opts, header[index].kind.unwrap(), col, &mut out)?;
                    json_object.insert(header[index].name.to_string(), serde_json::from_slice(&out)?);
                }
                
                // Write json object from this row to the stream.
                serde_json::to_writer(&mut out, &json_object)?;
            }
        }
    }
    Ok(())
}

fn do_capture_output_csv<T: std::io::Write>(
    opts: &cmd::opts::CaptureOpts,
    table: lang::OutputTable,
    mut out: T)
-> Result<(), BoxError> {
    // Output header.
    let mut header = table.header();
    if let Some(symbol) = header.nth(0) {
        write!(out, "{}{}{}", opts.capture.header_delim, symbol.name, opts.capture.header_delim)?;
    }
    for symbol in header {
        write!(out, "{}{}{}{}", opts.capture.sep, opts.capture.header_delim, symbol.name, opts.capture.header_delim)?;
    }
    write!(out, "\n")?;
    
    // Save types of headers so that we can infer types of missing values.
    let header: Vec<lang::ast::Type> = table.header().map(|symbol| symbol.kind.unwrap()).collect();
    
    // Output rows.
    for row in table.rows() {
        let row = row?;
        let mut row = row.columns().enumerate();
        if let Some((index, col)) = row.nth(0) {
            write_cell(opts, header[index], col, &mut out)?;
        }
        for (index, col) in row {
            write!(out, "{}", opts.capture.sep)?;
            write_cell(opts, header[index], col, &mut out)?;
        }
        write!(out, "\n")?; 
    }
    
    // Done.
    Ok(())
}

fn write_cell<T: std::io::Write>(
    opts: &cmd::opts::CaptureOpts,
    cell_type: lang::ast::Type,
    cell_contents: &Option<lang::ast::Value>,
    out: &mut T)
-> Result<(), BoxError> {
    match cell_contents {
        Some(data) => match data {
            lang::ast::Value::Bool(data) => match data {
                true => write!(out, "{}", opts.capture.true_str)?,
                false => write!(out, "{}", opts.capture.false_str)?,
            }
            lang::ast::Value::U32(data) => write!(out, "{}{}{}", opts.capture.num_delim, data, opts.capture.num_delim)?,
            lang::ast::Value::I32(data) => write!(out, "{}{}{}", opts.capture.num_delim, data, opts.capture.num_delim)?,
            lang::ast::Value::F32(data) => write!(out, "{}{}{}", opts.capture.num_delim, data, opts.capture.num_delim)?,
            lang::ast::Value::String(data) => write!(out, "{}{}{}", opts.capture.text_delim, data, opts.capture.text_delim)?,
            lang::ast::Value::Date(data) => write!(out, "{}{}{}", opts.capture.text_delim, data.format("%-m/%-d/%Y"), opts.capture.text_delim)?,
            lang::ast::Value::NoneLiteral => write!(out, "{}", opts.capture.missing_text)?,
        },
        None => {
            match cell_type {
                // Missing numeric value
                lang::ast::Type::Bool | lang::ast::Type::U32 |
                lang::ast::Type::I32 | lang::ast::Type::F32 =>
                    write!(out, "{}", opts.capture.missing_num)?,
                
                // Missing text value
                lang::ast::Type::String | lang::ast::Type::Date |
                lang::ast::Type::NoneLiteral =>
                    write!(out, "{}", opts.capture.missing_text)?,
            }
        },
    }
    Ok(())
}
