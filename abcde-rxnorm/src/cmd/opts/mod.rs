//! Module for command line parsing.

// Actual parsing is done in submodule using structopt crate.
mod structopt;

/// ABCD data release, e.g. "Two" or "Three".
pub use self::structopt::Release;

/// Structure for parsing all command line options using structopt.
pub use self::structopt::Opts;

/// Errors that may occur during command line parsing.
pub use self::structopt::OptsError;

/// Enumeration over subcommands.
pub use self::structopt::OptCmd;

/// Convenience structure combining common arguments with those for the generate
/// subcommand.
pub struct GenerateOpts<'a> {
    pub common: &'a self::structopt::CommonOpts,
    pub generate: &'a mut self::structopt::GenerateOpts,
}
