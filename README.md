# ABCDE

ABCDE stands for [ABCD](https://www.addictionresearch.nih.gov/abcd-study) Boolean Capture Data Explorer.  It is a command-line driven data tool designed for the ABCD dataset and written in the Rust programming language.  With abcde you can explore the available data like this:

```
abcde explore --search height
```
```
Query: abcd_ant01.anthro_1_height_in
Description:
STANDING HEIGHT #1 (in inches)

Query: abcd_ant01.anthro_2_height_in
Description:
STANDING HEIGHT #2 (in inches)

Query: abcd_ant01.anthro_3_height_in
Description:
STANDING HEIGHT #3 (in inches)

Query: abcd_ant01.anthro_height_calc
Description:
Standing Height Average (inches): If three measurements were obtained, the two closest measurements will be averaged. Should the third measurement fall equally between the first two measurements, all three will be averaged.
```

And generate output tables containing the data you are interested in like this:

```
abcde capture --code '
let height = abcd_ant01.anthro_height_calc as f32;
let weight = abcd_ant01.anthro_weight_calc as f32;
let bmi = 703.0 * weight / (height * height);
select bmi where true;'
```
```
".subjectkey" ".interview_date" "bmi"
"NDAR_INVAAAAAAAA" "10/1/2018" 20.48054
"NDAR_INVBBBBBBBB" "4/22/2018" 22.022085
"NDAR_INVCCCCCCCC" "2/21/2017" 18.234287
...
```

## Getting Started

**If you are unsure where to start, [go to the abcde package](./abcde/README.md).**

## Organization

The abcde source tree is organized into a [workspace](https://doc.rust-lang.org/cargo/reference/manifest.html#the-workspace-section) comprised of multiple packages.  The [abcde package](./abcde/README.md) contains the abcde tool, which is the primary tool for exploring and capturing data.  Over time we will be adding additional helper packages such as [abcde-rxnorm](./abcde-rxnorm/README.md), each located inside its own subdirectory.

