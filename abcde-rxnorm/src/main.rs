// Split additional command-line-binary-specific code out into additional
// modules/files. 
mod cmd;

fn main() -> Result<(), cmd::BoxError> {
    match try_main() {
        Err(e) => {
            if let Some(e) = e.downcast_ref::<cmd::opts::OptsError>() {
				eprintln!("{}", e);
			}
			else if let Some(e) = e.downcast_ref::<std::io::Error>() {
			    eprintln!("ABCDE encountered an error and had to exit.\n{:?}", e.kind());
			    if let Some(e) = e.get_ref() {
			        eprintln!("{}", e);
			    } 
			}
            else {
                eprintln!("ABCDE encountered an error and had to exit.\n{}", e);
            }
            std::process::exit(1)
        },
        Ok(_) => Ok(()),
    }
}

fn try_main() -> Result<(), cmd::BoxError> {
    // Parse command line arguments.
    let cmd_opts = cmd::opts::Opts::from_args()?;
    
    // Branch execution based on subcommand.
    match cmd_opts.cmd {
        cmd::opts::OptCmd::Generate(mut generate_opts) => cmd::actions::generate(cmd::opts::GenerateOpts{
            common: &cmd_opts.common,
            generate: &mut generate_opts,
        }),
    }
}
