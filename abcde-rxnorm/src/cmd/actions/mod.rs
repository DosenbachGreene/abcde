//! Actions that can be performed by the command line driven abcde binary.

mod generate;
/// Function that performs generate action/command.
pub use generate::generate;
