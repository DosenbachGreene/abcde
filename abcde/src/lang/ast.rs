/// Code is a vector of statements.
pub type Code = Vec<Stmt>;

/// Kind of statement.
#[derive(Debug, Clone, Eq, PartialEq)]
pub enum StmtKind {
    Comment,
    Let,
    Select,
    Include,
}

/// Statement
#[derive(Debug)]
pub enum Stmt {
    /// Comment string.  May be deprecated/not stored in the AST in the future.
    Comment(String),
    /// Let statement assigning to a variable.
    Let(Let),
    /// Select statement specifying which symbols to capture and when.
    Select(Select),
    /// File to parse and insert into the AST.
    Include(std::path::PathBuf),
}
impl Stmt {
    pub fn kind(&self) -> StmtKind {
        match self {
            Self::Comment(_) => StmtKind::Comment,
            Self::Let(_) => StmtKind::Let,
            Self::Select(_) => StmtKind::Select,
            Self::Include(_) => StmtKind::Include,
        }
    }
}
impl std::fmt::Display for Stmt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Let(l) => write!(f, "{}", l),
            Self::Select(s) => write!(f, "{}", s),
            Self::Comment(c) => write!(f, "{}", c),
            Self::Include(ff) => write!(f, "include \"{}\";", ff.display()),
        }
    }
}

/// Let Statement
#[derive(Debug)]
pub struct Let {
    pub lhs: Variable,
    pub rhs: Box<Expr>,
}
impl std::fmt::Display for Let {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "let {} = {};", self.lhs, self.rhs)
    }
}

/// Select statement
#[derive(Debug)]
pub struct Select {
    pub symbols: Vec<Symbol>,
    pub predicate: Box<Expr>,
}
impl std::fmt::Display for Select {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "select {} where {};", self.symbols.iter().map(|s| s.to_string()).collect::<Vec<String>>().join(", "), self.predicate)
    }
}

/// Database query
#[derive(Debug, Clone, Eq, PartialEq, Hash, Ord, PartialOrd)]
pub struct Query {
    /// Name of file/table.
    /// If empty then assume a special query like `.subjectkey`.
    pub file: Option<std::string::String>,
    /// Name of field/column within file
    pub field: std::string::String,
}
impl std::fmt::Display for Query {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(file) = &self.file {
            write!(f, "{}", file)?;
        }
        write!(f, ".{}", self.field)
    }
}
impl Query {
    /// Consumes a query and checks if it is valid.  Returns the query if valid,
    /// otherwise returns an error.
    pub fn validate(self) -> Result<Self, ParseQueryError> {
        if !self.has_file() && !self.is_special() {
            Err(ParseQueryError{field: self.field})
        }
        else {
            Ok(self)
        }
    }

    /// Returns true if the query has `Some(String)` in its file field.
    pub fn has_file(&self) -> bool {
        self.file.is_some()
    }

    /// Returns true if the query is one of the recognized special queries
    /// .subjectkey or .interview_date).
    pub fn is_special(&self) -> bool {
        match self.has_file() {
            true => false,
            false => match self.field.as_str() {
                "subjectkey" => true,
                "interview_date" => true,
                "eventname" => true,
                _ => false,
            }
        }
    }

    // Returns the type of a special query, or `None` if the query is not
    // special.
    pub fn special_type(&self) -> Option<Type> {
        if self.has_file() {
            None
        }
        else {
            match self.field.as_str() {
                "subjectkey" => Some(Type::String),
                "interview_date" => Some(Type::Date),
                "eventname" => Some(Type::String),
                _ => None,
            }
        }
    }
}

/// Variable name
#[derive(Debug, Eq, PartialEq, Hash, Ord, PartialOrd, Clone)]
pub struct Variable(pub std::string::String);
impl std::fmt::Display for Variable {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

/// Kind of a symbol
#[derive(Debug, Clone, Copy)]
pub enum SymbolNameKind {
    /// Query
    Query,
    /// Variable
    Variable,
}
impl std::fmt::Display for SymbolNameKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Variable => write!(f, "variable"),
            Self::Query => write!(f, "query")
        }
    }
}

/// Name of a symbol
#[derive(Debug, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum SymbolName {
    /// The symbol is a query.
    Query(Query),

    /// The symbol is a variable.
    Variable(Variable),
}
impl SymbolName {
    /// The kind of the symbol: query or variable.
    pub fn kind(&self) -> SymbolNameKind {
        match self {
            SymbolName::Query(_) => SymbolNameKind::Query,
            SymbolName::Variable(_) => SymbolNameKind::Variable,
        }
    }
}
impl std::fmt::Display for SymbolName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Query(q) => {
                if let Some(ref file) = q.file {
                    write!(f, "{}", file)?;
                }
                write!(f, ".{}", q.field)
            },
            Self::Variable(v) => write!(f, "{}", v),
        }
    }
}

/// A symbol that resolves to a value.
/// Can refer to a query or a variable.
/// At the time of code evaluation all symbols have a known type.
/// During intermediate points in code interpretation the type may not be known.
#[derive(Debug, Clone)]
pub struct Symbol {
    /// The name of the symbol.
    pub name: SymbolName,

    /// The type (aka kind) of the symbol.
    pub kind: Option<Type>,
}
impl std::fmt::Display for Symbol {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)?;
        if let Some(kind) = self.kind {
            write!(f, " as {}", kind)?;
        }
        Ok(())
    }
}

/// Function
/// Not yet implemented
#[derive(Debug)]
pub struct Function {
    pub name: String,
    pub self_arg: Option<Box<Expr>>,
    pub other_args: Vec<Box<Expr>>,
}

/// The kind of an expression
#[derive(Debug, Copy, Clone)]
pub enum ExprKind {
    BinaryOp(Opcode),
    UnaryOp(Opcode),
    If,
    Function,
    Value,
    Symbol(SymbolNameKind),
}
impl std::fmt::Display for ExprKind {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::BinaryOp(opcode) => write!(f, "binary operation {}", opcode),
            Self::UnaryOp(opcode) => write!(f, "unary operation {}", opcode),
            Self::If => write!(f, "if expression"),
            Self::Function => unimplemented!(),
            Self::Value => write!(f, "literal value"),
            Self::Symbol(kind) => write!(f, "{}", kind),
        }
    }
}

/// Expression
#[derive(Debug)]
pub enum Expr {
    /// Expression that takes two expressions separated by a binary operand.
    BinaryOp(Box<Expr>, Opcode, Box<Expr>),

    /// Expression that takes one expression and a unary operand.
    UnaryOp(Opcode, Box<Expr>),

    /// If expression =.
    If(Box<If>),

    /// Function, not yet implemented
    Function(Function),

    /// Literal value
    Value(Value),

    /// Symbol, which can be a query or variable.
    Symbol(Symbol),
}
impl Expr {
    pub fn kind(&self) -> ExprKind {
        match self {
            Self::BinaryOp(_, opcode, _) => ExprKind::BinaryOp(*opcode),
            Self::UnaryOp(opcode, _) => ExprKind::UnaryOp(*opcode),
            Self::If(_) => ExprKind::If,
            Self::Function(_) => ExprKind::Function,
            Self::Value(_) => ExprKind::Value,
            Self::Symbol(symbol) => ExprKind::Symbol(symbol.name.kind()),
        }
    }
}
impl std::fmt::Display for Expr {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::BinaryOp(lhs, opcode, rhs) =>
                write!(f, "( {} {} {} )", lhs, opcode, rhs),
            Self::UnaryOp(opcode, operand) =>
                write!(f, "{}( {} )", opcode, operand),
            Self::If(i) => write!(f, "{}", i),
            Self::Function(func) => {
                match &func.self_arg {
                    Some(self_arg) => { write!(f, "{}.", self_arg)?; }
                    None => (),
                };
                write!(
                    f,
                    "{}({})",
                    func.name,
                    func.other_args.iter().map(|arg| arg.to_string()).collect::<Vec<String>>().join(", ")
                )
            },
            Self::Value(v) => write!(f, "{}", v),
            Self::Symbol(s) => write!(f, "{}", s),
        }
    }
}

/// Describes an `if` expression with optional branching `else if` and `else`
/// expressions that evaluates to an inner expression enclosed by curly braces.
/// For example, the following (contrived) statement sets `equal` to `true`.
///
/// `let equal = if 1 < 1 { false } else if 1 > 1 { false } else { true }`
#[derive(Debug)]
pub struct If {
    /// Some predicate expression of an if or else if expression.
    /// None for an else expression.
    pub predicate: Option<Box<Expr>>,

    /// The expression to evaluate if the predicate is true.
    pub expression: Box<Expr>,

    /// Another if statement to evaluate if the predicate is false.
    /// None if there is no subsequent `else if` or `else` statement.
    pub alternative: Option<Box<If>>
}
impl std::fmt::Display for If {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if let Some(ref p) = self.predicate {
            write!(f, "if {} ", p)?;
        }
        write!(f, "{{ {} }}", self.expression)?;
        if let Some(ref a) = self.alternative {
            write!(f, " else {}", a)?;
        }
        Ok(())
    }
}

/// Operation Code
#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum Opcode {
    /// ||
    OR,
    /// &&
    AND,
    /// !
    NOT,
    /// <
    LT,
    /// >
    GT,
    /// ==
    EQ,
    /// !=
    NE,
    /// <=
    LE,
    /// >=
    GE,
    /// +
    PLUS,
    /// -
    MINUS,
    /// *
    MUL,
    /// /
    DIV,
}
impl std::fmt::Display for Opcode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Opcode::OR    => write!(f, "||"),
            Opcode::AND   => write!(f, "&&"),
            Opcode::NOT   => write!(f, "!"),
            Opcode::LT    => write!(f, "<"),
            Opcode::GT    => write!(f, ">"),
            Opcode::EQ    => write!(f, "=="),
            Opcode::NE    => write!(f, "!="),
            Opcode::LE    => write!(f, "<="),
            Opcode::GE    => write!(f, ">="),
            Opcode::PLUS  => write!(f, "+"),
            Opcode::MINUS => write!(f, "-"),
            Opcode::MUL   => write!(f, "*"),
            Opcode::DIV   => write!(f, "/"),
        }
    }
}

// Comment out support for differently-sized numerics like i8, i16, etc for now
// to help simplify type checking.
/// Value
#[derive(Debug, Clone)]
pub enum Value {
    Bool(bool),
//    I8(i8),
//    U8(u8),
//    I16(i16),
//    U16(u16),
    I32(i32),
    U32(u32),
//    I64(i64),
//    U64(u64),
    F32(f32),
//    F64(f64),
    String(std::string::String),
    Date(chrono::NaiveDate),
    NoneLiteral,
}
impl Value {
    pub fn kind(&self) -> Type {
        match self {
            Value::Bool(_) => Type::Bool,
            Value::I32(_) => Type::I32,
            Value::U32(_) => Type::U32,
            Value::F32(_) => Type::F32,
            Value::String(_) => Type::String,
            Value::Date(_) => Type::Date,
            Value::NoneLiteral => Type::NoneLiteral,
        }
    }
}
impl std::fmt::Display for Value {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Bool(v) => write!(f, "{}", v),
            Self::I32(v) => write!(f, "{} as i32", v),
            Self::U32(v) => write!(f, "{} as u32", v),
            Self::F32(v) => write!(f, "{} as f32", v),
            Self::String(v) => write!(f, "\"{}\" as String", v),
            Self::Date(v) => write!(f, "\"{}\" as Date", v),
            Self::NoneLiteral => write!(f, "None"),
        }
    }
}

/// Type of value.
#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Type {
    /// Boolean, equivalent to Rust `bool`
    Bool,
    /// 32-bit signed integer, equivalent to Rust `i32`
    I32,
    /// 32-bit unsigned integer, equivalent to Rust `u32`
    U32,
    /// Single-precision (32-bit) float, equivalent to Rust `f32`
    F32,
    /// UTF-8 encoded string, equivalent to Rust `std::string::String`
    String,
    /// M/D/Y formatted date, equivalent to `chrono::NaiveDate`
    Date,
    /// Type of `Value::None`
    /// Analagous to Rust `None` but not directly equivalent
    NoneLiteral,
}
impl std::fmt::Display for Type {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Type::Bool => write!(f, "bool"),
            Type::I32 => write!(f, "i32"),
            Type::U32 => write!(f, "u32"),
            Type::F32 => write!(f, "f32"),
            Type::String => write!(f, "String"),
            Type::Date => write!(f, "Date"),
            Type::NoneLiteral => write!(f, "NoneLiteral"),
        }
    }
}

/// Error type for LALRPOP parsing errors.
/// The built-in error type contains tokens that reference the input string of
/// code.  Unfortunately this makes error handling with the `?` operator
/// complicated because the the built-in error type cannot be boxed, because
/// that would cause the tokens to have lifetimes that outlive the input string.
/// To facilitate more ergnomic error handling, this error type stores the
/// Fmt renderings of the error as a string.  If you want access to the actual
/// underlying error then you will need to write code that calls
/// `CodeParser::new().parse(&'input my_input_str)` directly and deal with the
/// nested result, which has type:
///
/// ```ignore
/// Result<Result<Code, ParseError>, lalrpop_util::ParseError<usize, lang::parser::Token<'input>, &'static str>>
/// ```
pub struct LALRError {
    pub display: String,
    pub debug: String,
}
impl std::fmt::Display for LALRError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.display)
    }
}
impl std::fmt::Debug for LALRError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.display)
    }
}
impl std::error::Error for LALRError { }
impl <'input> From<lalrpop_util::ParseError<usize, super::parser::Token<'input>, &'static str>> for LALRError {
    fn from(error: lalrpop_util::ParseError<usize, super::parser::Token<'input>, &'static str>) -> Self {
        Self {
            display: format!("{}", error),
            debug: format!("{:?}", error),
        }
    }
}

/// Enumerable type to store all possible errors that could occur during parsing
/// of the AST outside of the LALRPOP parser itself.
#[derive(Debug)]
pub enum ParseError {
    /// Error from LALRPOP.
    LALRError(LALRError),
    /// Error parsing a bool.
    ParseBoolError(std::string::String),
    /// Error parsing an integer.
    ParseIntError(std::num::ParseIntError),
    /// Error parsing a float.
    ParseFloatError(std::num::ParseFloatError),
    /// Error parsing a date.
    ParseDateError(chrono::format::ParseError),
    /// Error parsing a query.
    ParseQueryError(ParseQueryError),
}
// implement From trait to convert from each error type into the enum
/*impl<L, T, E> From<lalrpop_util::ParseError<L, T, E>> for ParseError
where
    lalrpop_util::ParseError<L, T, E>: std::error::Error + std::marker::Send + std::marker::Sync,
    L: 'static,
    T: 'static,
    E: 'static,
{
    fn from(error: lalrpop_util::ParseError<L, T, E>) -> Self {
        Self::ParseError(error.to_string())
        //let b: crate::BoxError = Box::new(error);
        //Self::ParseError(b)
    }
}*/

impl <'input> From<lalrpop_util::ParseError<usize, super::parser::Token<'input>, &'static str>> for ParseError {
    fn from(error: lalrpop_util::ParseError<usize, super::parser::Token<'input>, &'static str>) -> Self {
        Self::LALRError(LALRError::from(error))
    }
}

impl From<std::num::ParseIntError> for ParseError {
    fn from(error: std::num::ParseIntError) -> Self {
        Self::ParseIntError(error)
    }
}
impl From<std::num::ParseFloatError> for ParseError {
    fn from(error: std::num::ParseFloatError) -> Self {
        Self::ParseFloatError(error)
    }
}
impl From<chrono::format::ParseError> for ParseError {
    fn from(error: chrono::format::ParseError) -> Self {
        Self::ParseDateError(error)
    }
}
impl From<ParseQueryError> for ParseError {
    fn from(error: ParseQueryError) -> Self {
        Self::ParseQueryError(error)
    }
}
// implement the Error trait on the enum itself
impl std::fmt::Display for ParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::LALRError(e) => write!(f, "Lexer/Parser error: {}", e),
            Self::ParseBoolError(s) => write!(f, "Couldn't parse bool: {}", s),
            Self::ParseIntError(e) => write!(f, "Couldn't parse integer: {}", e),
            Self::ParseFloatError(e) => write!(f, "Couldn't parse float: {}", e),
            Self::ParseDateError(e) => write!(f, "Couldn't parse date: {}", e),
            Self::ParseQueryError(e) => write!(f, "Couldn't parse query: {}", e),
        }
    }
}
impl std::error::Error for ParseError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match &self {
            Self::LALRError(e) => e.source(),
            Self::ParseBoolError(_) => None,
            Self::ParseIntError(e) => e.source(),
            Self::ParseFloatError(e) => e.source(),
            Self::ParseDateError(e) => e.source(),
            Self::ParseQueryError(e) => e.source(),
        }
    }
}

/// Error type for malformed queries in which the file part is missing but the
/// field is not one of the special queries .subjectkey, .interview_date, or
/// .eventname.
#[derive(Debug)]
pub struct ParseQueryError {
    /// The field of the malformed query.
    pub field: String,
}
impl std::fmt::Display for ParseQueryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Query .{} is missing table.  Did you mean some_table.{}?", &self.field, &self.field)
    }
}
impl std::error::Error for ParseQueryError { }

/// Strip starting and ending character.
pub fn strip(s: &str) -> &str {
    let len = s.len();
    if len >= 2 {
        &s[1..=len-2]
    }
    else {
        s
    }
}

/// Trait implemented by objects that we can attempt to convert into an abstract
/// syntax tree (AST) of code.
pub trait IntoCode {
    /// Convert argument into an abstract syntax tree of type `lang::Code`.
    fn into_code(self) -> Result<Code, ParseError>;
}
impl IntoCode for &str {
    fn into_code(self) -> Result<Code, ParseError> {
        match super::parser::CodeParser::new().parse(self) {
            Ok(code) => code,
            Err(e) => Err(e.into()),
        }
    }
}
impl IntoCode for &String {
    fn into_code(self) -> Result<Code, ParseError> {
        match super::parser::CodeParser::new().parse(self) {
            Ok(code) => code,
            Err(e) => Err(e.into()),
        }
    }
}
impl IntoCode for Code {
    fn into_code(self) -> Result<Code, ParseError> {
        Ok(self)
    }
}
