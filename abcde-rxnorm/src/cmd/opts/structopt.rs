//! Command line option parsing using the structopt crate.

use structopt::StructOpt;

#[derive(Debug, PartialEq)]
/// An ABCD data release, e.g. "2.0" or "3.0".
pub enum Release {
    Two,
    Three,
}
/// Error parsing ABCD release.
pub struct ReleaseParseError {
    pub string: String,
}
impl<'a> std::fmt::Display for ReleaseParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Couldn't parse ABCD release from string \"{}\".",
            self.string
        )
    }
}
impl std::fmt::Debug for ReleaseParseError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <Self as std::fmt::Display>::fmt(self, f)
    }
}
impl std::error::Error for ReleaseParseError {}
impl std::str::FromStr for Release {
    type Err = ReleaseParseError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "2" | "2.0" => Ok(Self::Two),
            "3" | "3.0" => Ok(Self::Three),
            _ => Err(ReleaseParseError {
                string: s.to_string(),
            }),
        }
    }
}

#[derive(Debug, StructOpt)]
#[structopt(author, after_help=concat!("Examples:\n\t", structopt::clap::crate_name!(), " --variable methylphenidate \\\n\t             --from-drug methylphenidate\n\n\t", structopt::clap::crate_name!(), " --variable methylphenidate --from-rxcui 6901\n"))]
/// Main options structure.
pub struct Opts {
    #[structopt(flatten)]
    pub common: CommonOpts,

    #[structopt(subcommand)]
    /// Subcommand
    pub cmd: OptCmd,
}

#[derive(Debug, StructOpt)]
/// Options common to all subcommands.
pub struct CommonOpts {
    /// Turn on verbose output
    #[structopt(short = "v", long = "verbose")]
    pub verbose: bool,

    /// ABCD release to generate code for.
    #[structopt(long = "release", default_value = "3.0")]
    pub release: Release,
}

#[derive(Debug, StructOpt)]
#[structopt(about, setting = structopt::clap::AppSettings::TrailingVarArg)]
/// Enumeration of possible subcommands.
pub enum OptCmd {
    /// Generate capture code for a boolean variable that is true if a subject
    /// is taking any one of a list of drugs and false otherwise.  The drug can
    /// be specified by name or by RXCUI.  By default, code is automatically
    /// generated for all drugs with the same active ingredient as the given
    /// drug(s).  You can modify this behavior with the --ingredients and
    /// --exact flags. Refer to the RxNorm website for more information:{n}https://www.nlm.nih.gov/research/umls/rxnorm/overview.html
    #[structopt(group = structopt::clap::ArgGroup::with_name("generate_group").required(true))]
    Generate(GenerateOpts),
}

#[derive(Debug, StructOpt)]
/// Options for Generate subcommand.
pub struct GenerateOpts {
    /// Generate based on drug name.
    #[structopt(long = "from-drug", group = "generate_group")]
    pub drug_name: Option<String>,

    /// Generate based on one or more RXCUIs
    #[structopt(long = "from-rxcuis", group = "generate_group")]
    pub rxcuis: Vec<u64>,

    /// Name of variable to generate
    #[structopt(long = "variable")]
    pub var_name: String,

    /// Assume the provided RXCUIs refer to the active ingredient(s) directly.
    /// Do not automatically look up the active ingredient of each RXCUI.
    #[structopt(long = "ingredients")]
    pub is_ingredients: bool,

    /// Only generate capture code for the exact RXCUIs provided.  Do not expand
    /// to a list of related RXCUIs.
    #[structopt(long = "exact")]
    pub exact: bool,
}

impl Opts {
    /// Struct method to override the trait method `StructOpt::from_args()`.
    ///
    /// The trait version `StructOpt::from_args()` prints out the help or version
    /// message to `stderr` if the program was called with -h/--help or
    /// -V/--version, respectively.  It prints out an error message and some
    /// usage hints if the program was called with invalid arguments.  After
    /// either of these cases it terminates the program with `std::process::exit()`.
    /// Otherwise it returns a new `Opts` struct.  There is no need for the
    /// struct to be wrapped in a `Result` since any error will have caused the
    /// program to terminate.
    ///
    /// Although simple, the problem with the above approach is twofold:
    /// 1. You might not want your program to terminate immediately.
    /// 2. Terminating with `std::process::exit()` does not unwind the stack.
    ///
    /// This struct version overrides the unsafe trait version of `from_args()`
    /// and calls `StructOpt::from_iter_safe(std::env::args())` internally.  In
    /// cases where the program would have terminated it prints out nothing and
    /// returns a custom error type, `OptsError`.  This special error type is
    /// designed to be returned from the `main()` function at which point Rust
    /// will print out the help, version, or error/usage message for you.  If
    /// you need access to the underlying `clap::Error` it is stored in
    /// `OptsError::error`.
    ///
    /// Example:
    ///
    /// ```ignore
    /// fn main() -> Result<(), OptsError> {
    /// 	let cmd_opts = Opts::from_args()?;
    ///		// ...do things...
    /// 	return Ok(());
    /// }
    /// ```
    pub fn from_args() -> Result<Opts, OptsError> {
        match Opts::from_iter_safe(std::env::args()) {
            Ok(opts) => Ok(opts),
            Err(e) => Err(OptsError { error: e }),
        }
    }
}

/// Custom error type to return when command line parsing has gone wrong.
///
/// Why not just use `clap::Error`?  The anticipated use case is that this error
/// will be returned from `main()` when command line parsing fails.  Rust will
/// then do something like `eprinteln!("Error: {:?}", error)` just before the
/// program terminates.  The pretty-formatted help, version, or error/usage
/// message is generated by the `Display` trait for `clap::Error`, whereas its
/// `Debug` trait would be quite indecipherable to the end user!
///
/// This custom error type stores an inner `clap::Error`, which is freely
/// accessible via its public member `OptsError::error`.  Its `Display` and
/// `Debug` traits both use VT100 ANSI escape codes to erase the "Error: "
/// message printed by Rust, since it is misleading when printout out
/// help/version information and redundant when printout out pretty-formatted
/// error/usage information.  It then renders `clap::Error` using its `Display`
/// trait regardless of whether `OptsError` is being rendered via `Display`
/// or `Debug`.
///
/// See the documentation of `Opts::from_args()` for more information.
pub struct OptsError {
    pub error: structopt::clap::Error,
}
impl std::fmt::Display for OptsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        // Use VT100 ANSI escape codes to erase current line and move cursor to the
        // beginning of the line (on the left).
        write!(f, "\x1B[2K\x1B[1000D{}", self.error)
    }
}
impl std::fmt::Debug for OptsError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        <OptsError as std::fmt::Display>::fmt(self, f)
    }
}
impl std::error::Error for OptsError {}
