//! Command line option parsing using the structopt crate.

use crate::cmd::Password;
use structopt::StructOpt;
use structopt::clap::arg_enum;

#[derive(Debug, StructOpt)]
#[structopt(author, after_help=concat!("Examples:\n\t", structopt::clap::crate_name!(), " -d /path/to/data explore --search mri\n\n\t", structopt::clap::crate_name!(), " -d /path/to/data capture --code-file my_code \\\n\t      -o table1.txt -o table2.txt\n"))]
/// Main options structure.
pub struct Opts {
    #[structopt(flatten)]
    pub common: CommonOpts,
	
	#[structopt(subcommand)]
	/// Subcommand
	pub cmd: OptCmd,
}

#[derive(Debug, StructOpt)]
/// Options common to all subcommands.
pub struct CommonOpts {
    /// Turn on verbose output
	#[structopt(short = "v", long = "verbose")]
	pub verbose: bool,
	
	/// Where the ABCD data is located
	#[structopt(short= "d", long = "data-dir", env="ABCDE_DATA_DIR")]
	pub data_dir: std::path::PathBuf,
}

#[derive(Debug, StructOpt)]
#[structopt(about, setting = structopt::clap::AppSettings::TrailingVarArg)]
/// Enumeration of possible subcommands.
pub enum OptCmd {
	/// Search for available fields/columns in the data
	#[structopt(group = structopt::clap::ArgGroup::with_name("explore_group").required(true))]
	Explore(ExploreOpts),
	
	/// Generate a custom table from a subset of the data
	#[structopt(group = structopt::clap::ArgGroup::with_name("code_group").required(true))]
	Capture(CaptureOpts),
	
	/// Download the data from NDA, the NIMH Data Archive{n}Size is ~ 7 GB
	Download(DownloadOpts),
}

#[derive(Debug, StructOpt)]
/// Options for Explore subcommand.
pub struct ExploreOpts {
    /// Limit search to a single table{n}Example: abcd_mri01
    #[structopt(long="table", conflicts_with="query")]
    pub table: Option<String>,
    
    /// List all the fields of a file/table.
    #[structopt(long="list", requires("table"), group="explore_group")]
    pub list: bool,
    
    /// Search for fields matching a regular expression.
    #[structopt(long="search", group="explore_group")]
    pub regex: Option<String>,
    
    /// Display details about a query{n}Example: abcd_mri01.mri_info_studydate
    #[structopt(long="info", group="explore_group")]
    pub query: Option<String>,
}

// How to render json, as a stream (one object for each row in output)
// or as a folded array (one root object containing an array for each column) 
arg_enum! {
    #[derive(Debug)]
    pub enum JsonType {
        Stream,
        Array,
    }
}

#[derive(Debug, StructOpt)]
/// Options for Capture subcommand.
pub struct CaptureOpts {
    /// Code describing capture to perform{n}See LANGUAGE.md for details
    #[structopt(long="code", group="code_group")]
    pub code: Option<String>,
    
    /// File containing code.
    #[structopt(long="code-file", group="code_group")]
    pub code_file: Option<std::path::PathBuf>,
    
    /// File to which output table will be written{n}Must
    /// specify this option once for each select statement{n}If there is exactly one
    /// select statement and this option is omitted then the output table
    /// will be written to stdout.
    #[structopt(name="out-file", short = "o", long="output", number_of_values=1, multiple=true)]
    pub outputs: Vec<std::path::PathBuf>,
    
    /// The first row of output defaults to a header of field names.  Specify
    /// this option to skip writing the header.
    #[structopt(long="skip-header", default_value_ifs=&[("fmt-json", None, "true")])]
    pub skip_header: bool,
    
    /// Column separator.
    #[structopt(long="sep", default_value=" ", default_value_ifs=&[("fmt-matlab", None, "\t")])]
    pub sep: String,
    
    /// Delimeter for fields in the header.
    #[structopt(name="hdelim", long="header-delim", default_value="\"", conflicts_with="skip-header")]
    pub header_delim: String,
    
    /// Delimeter for text (non-numeric) values.{n}This includes strings and dates.
    #[structopt(name="delim", long="text-delim", default_value="\"", default_value_ifs=&[("fmt-matlab", None, "")])]
    pub text_delim: String,
    
    /// Delimeter for numeric values.
    #[structopt(name="ndelim", long="num-delim", default_value="")]
    pub num_delim: String,
    
    /// How to render missing text values.{n}Note that missing values will not
    /// have delimeters automatically placed around them.
    #[structopt(name="miss", long="missing-text", default_value="None", default_value_ifs=&[("fmt-matlab", None, "NaN"), ("fmt-json", None, "null")])]
    pub missing_text: String,
    
    /// How to render missing numeric value.{n}Note that missing values will not
    /// have delimeters automatically placed around them.
    #[structopt(name="nmiss", long="missing-num", default_value="None", default_value_ifs=&[("fmt-matlab", None, "NaN"), ("fmt-json", None, "null")])]
    pub missing_num: String,
    
    /// How to render boolean true.{n}Note that boolean values will not have
    /// delimeters automatically placed around them.
    #[structopt(long="true", default_value="true", default_value_ifs=&[("fmt-matlab", None, "1")])]
    pub true_str: String,
    
    /// How to render boolean false.{n}Note that boolean values will not have
    /// delimeters automatically placed around them.
    #[structopt(long="false", default_value="false", default_value_ifs=&[("fmt-matlab", None, "0")])]
    pub false_str: String,
    
    /// Render output in a format that is comprehensible to Matlab's readtable()
    #[structopt(name="fmt-matlab", long="fmt-matlab")]
    pub fmt_matlab: bool,
    
    /// Render output as json
    #[structopt(name="fmt-json", long="fmt-json", conflicts_with_all=&["fmt-matlab", "sep"])]
    pub fmt_json: bool,
    
    /// Type of json to produce{n}Array renders one object with an array of rows for each column{n}Stream renders a stream of objects, one for each row of{n}[default: Array] 
    #[structopt(name="json-type", long="json-type", requires="fmt-json", possible_values=&JsonType::variants(), case_insensitive=true, default_value_ifs=&[("fmt-json", None, "array")])]
    pub json_type: Option<JsonType>,
}

#[derive(Debug, StructOpt)]
/// Options for download subcommand.
pub struct DownloadOpts {
    /// NDA user name
    #[structopt(short="u", long="user")]
    pub user: Option<String>,
    
    /// NDA password{n}BE CAREFUL of supplying your password on the commandline
    /// or placing it in scripts, as this may allow others to learn your
    /// password by reading your script file or .bash_history.  If you omit this
    /// option then you will be prompted for your password interactively.
    #[structopt(short="p", long="pass")]
    pub pass: Option<Password>,
    
    /// Package ID of NDA data set.  See available
    /// packages:{n}https://nda.nih.gov/user/dashboard/packages.html?dataset=Adolescent+Brain+Cognitive+Development+Study+%28ABCD%29&type=shared_packages{n}Defaults
    /// to latest data release tested with this software.
    #[structopt(name="id", long="package", default_value="1181898")]
    pub package_id: u64,
}

impl Opts {
	/// Struct method to override the trait method `StructOpt::from_args()`.
	///
	/// The trait version `StrucOpt::from_args()` prints out the help or version
	/// message to `stderr` if the program was called with -h/--help or
	/// -V/--version, respectively.  It prints out an error message and some
	/// usage hints if the program was called with invalid arguments.  After
	/// either of these cases it terminates the program with `std::process::exit()`.
	/// Otherwise it returns a new `Opts` struct.  There is no need for the
	/// struct to be wrapped in a `Result` since any error will have caused the
	/// program to terminate.
	/// 
	/// Although simple, the problem with the above approach is twofold:
	/// 1. You might not want your program to terminate immediately.
	/// 2. Terminating with `std::process::exit()` does not unwind the stack.
	/// 
	/// This struct version overrides the unsafe trait version of `from_args()`
	/// and calls `StructOpt::from_iter_safe(std::env::args())` internally.  In
	/// cases where the program would have terminated it prints out nothing and
	/// returns a custom error type, `OptsError`.  This special error type is
	/// designed to be returned from the `main()` function at which point Rust
	/// will print out the help, version, or error/usage message for you.  If
	/// you need access to the underlying `clap::Error` it is stored in
	/// `OptsError::error`.
	/// 
	/// Example:
	/// 
	/// ```ignore
	/// fn main() -> Result<(), OptsError> {
	/// 	let cmd_opts = Opts::from_args()?;
	///		// ...do things...
	/// 	return Ok(());
	/// }
	/// ```
	pub fn from_args() -> Result<Opts, OptsError> {
		match Opts::from_iter_safe(std::env::args()) {
			Ok(opts) => Ok(opts),
			Err(e) => Err(OptsError { error: e })
		}
	}
}

/// Custom error type to return when command line parsing has gone wrong.
/// 
/// Why not just use `clap::Error`?  The anticipated use case is that this error
/// will be returned from `main()` when command line parsing fails.  Rust will
/// then do something like `eprinteln!("Error: {:?}", error)` just before the
/// program terminates.  The pretty-formatted help, version, or error/usage
/// message is generated by the `Display` trait for `clap::Error`, whereas its
/// `Debug` trait would be quite indecipherable to the end user!
/// 
/// This custom error type stores an inner `clap::Error`, which is freely
/// accessible via its public member `OptsError::error`.  Its `Display` and
/// `Debug` traits both use VT100 ANSI escape codes to erase the "Error: "
/// message printed by Rust, since it is misleading when printout out
/// help/version information and redundant when printout out pretty-formatted
/// error/usage information.  It then renders `clap::Error` using its `Display`
/// trait regardless of whether `OptsError` is being rendered via `Display`
/// or `Debug`.
/// 
/// See the documentation of `Opts::from_args()` for more information. 
pub struct OptsError {
	pub error: structopt::clap::Error
}
impl std::fmt::Display for OptsError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		// Use VT100 ANSI escape codes to erase current line and move cursor to the
		// beginning of the line (on the left).
		write!(f, "\x1B[2K\x1B[1000D{}", self.error)
	}
}
impl std::fmt::Debug for OptsError {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		<OptsError as std::fmt::Display>::fmt(self, f)
	}
}
impl std::error::Error for OptsError { }
