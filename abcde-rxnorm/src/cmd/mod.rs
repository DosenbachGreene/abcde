//! Modules for the command line driven binary program `abcde`.
//! These modules are only used in the binary and are not part of the library
//! crate.

/// Command line parsing.
pub mod opts;

/// Perform actions/commands described on command line.
pub mod actions;

mod error;
/// Generic error type.
pub use error::Error;
/// Type-erased error.
pub use error::BoxError;
