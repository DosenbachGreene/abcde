# ABCDE-RXNORM

The ABCD data contain detailed information about medication use.  Writing code to analyze this data by hand is tedious.  The abcde-rxnorm tool automatically generates code for:

 * If a subject is taking a medication
 * If the medication was taken in the last 24 hours

> Note that the subjects were asked if they took the medication within 24 hours of the `.interview_date`, which is not always the same day as the MRI scan.

For example:

```
abcde-rxnorm generate \
	--from-drug methylphenidate \
	--variable methylphenidate
```

Generates code like this:

```
// true if taking, false otherwise
let methylphenidate = ... ;
// true if taken in last 24 hours,
// false if not taken in last 24 hours,
// None if no response
let methylphenidate_24 = ... ;
```

## Installation

Refer to the [abcde README](../abcde/README.md) for instructions on installing Rust and downloading the abcde source code.  Once you have completed these steps, proceed with the instructions for compilation below.

### Compiling

This example assumes the abcde source tree is located in `~/src/abcde` as described in the [abcde README](../abcde/README.md).  Go to the root of the source tree and use cargo to compile abcde-rxnorm.

```
cd ~/src/abcde
cargo build -p abcde-rxnorm --release
```

Recall that `--release` instructs cargo to build an optimized version of abcde-rxnorm.  If you wish to compile a version for debugging simply omit the `--release` flag.

The `-p` or `--package` flag tells cargo which package within the workspace to build.  Cargo will default to building the abcde package.  By specifying `-p` we instruct it to build the abcde-rxnorm package instead.

### Installing

After you have compiled abcde-rxnorm, installation is similar to the process described in the [abcde README](../abcde/README.md).  Assuming you have already set up a binary directory in your shell's path at `~/bin/abcde`, you can just create a symlink.

```
ln -s $HOME/src/abcde/target/release/abcde-rxnorm ~/bin/abcde-rxnorm
```

## Medications in ABCD

The ABCD study contains data suitable for a wide variety of purposes, but the impetus for the creation was the desire by certain members of congress to measure the effect of illicit drug use on childhood neurodevelopment.  The study data contain detailed psychometric assessments and comprehensive questionnaires about drug use, both legal and illicit.  This abcde-rxnorm tool used together with the [abcde README](../abcde/README.md) tool is designed to help you capture data tables about legal drug use.

### Relevant Queries

Most of the medication data is found in table `medsy01`.  The principle queries of interest are as follows where `*` is an integer from 1 to 15.

* `medsy01.medinv_plus_rxnorm_med*_p as String` prescription medication, e.g. "6901 Methylphenidate"
* `medsy01.medinv_plus_otc_med*_rxnorm_p as String` over-the-counter medication, e.g. "50121 Fluticasone propionate"
* `medsy01.medinv_plus_rx_med*_24_p as bool` true if prescription medicine number `*` was taken in last 24 hours
* `medsy01.medinv_plus_otc_med*_24_p as bool` true if over-the-counter medication number `*` was taken in last 24 hours

Essentially, subjects and their parents were asked these same 4 questions 15 times, allowing them to report up to 30 medications and whether or not each medication was taken in the last 24 hours.

> Due to a mistake in ABCD data curation, the names of the fields are different in the ABCD 2.0 and 3.0 releases.  The names shown above are for the 2.0 release.  In the 3.0 release, all data appear to be pooled in the table `medsy01` (i.e. there is no table `medsy02`), but data from different events can be disambiguated by their `eventname` field (see the [documentation for abcde](../abcde/README.md) for more), and data for the second imaging follow-up visit appear to be missing completely.  The field names for the 3.0 release are as below:
> 
> * `medsy01.med*_rxnorm_p`
> * `medsy01.med_otc_*_rxnorm_p`
> * `medsy01.rx_med*_24`
> * `medsy01.otc_med*_24`

### Identifying Medications

The medications are identified by a unique number called an _RXCUI_ followed by a space and then a human-readable medication name.  These standardized RXCUIs and corresponding names are taken from the NIH's [RxNorm](https://www.nlm.nih.gov/research/umls/rxnorm/index.html).

What makes analysis tricky is that while one RXCUI basically refers to exactly one medication, **one medication can have multiple (sometimes hundreds) of RXCUIs that refer to it.**  This makes writing code to capture whether or not a subject is taking a given medication repetitive, tedious, and error prone.

In future versions of abcde we hope to introduce new features to the language such as macros and functions that will allow ergonomic and concise drug capture _in situ_.  In the interim, abcde-rxnorm exists to automatically generate drug capture code, which can then be copy-and-pasted into your own code.

## Using ABCDE-RXNORM

As with all the abcde family of tools, run abcde-rxnorm with no arguments or with `--help` or `help` to see usage information.

```
abcde-rxnorm help
```
```
...
USAGE:
    abcde-rxnorm [FLAGS] [release] <SUBCOMMAND>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v, --verbose    Turn on verbose output

ARGS:
    <release>    ABCD release to generate code for [default: 3.0]

SUBCOMMANDS:
    generate    Generate capture code for a boolean variable that is true if a subject is taking any one of a list
                of drugs and false otherwise.  The drug can be specified by name or by RXCUI.  By default, code is
                automatically generated for all drugs with the same active ingredient as the given drug(s).  You can
                modify this behavior with the --ingredients and --exact flags. Refer to the RxNorm website for more
                information:
                https://www.nlm.nih.gov/research/umls/rxnorm/overview.html
    help        Prints this message or the help of the given subcommand(s)

Examples:
	abcde-rxnorm --variable methylphenidate \
	             --from-drug methylphenidate

	abcde-rxnorm --variable methylphenidate --from-rxcui 6901
```

ABCDE-RXNORM has one subcommand, generate, which is used to generate code for a drug.

```
abcde-rxnorm help generate
```
```
...
USAGE:
    abcde-rxnorm generate [FLAGS] [OPTIONS] --variable <var-name> <--from-drug <drug-name>|--from-rxcuis <rxcuis>...>

FLAGS:
        --exact          Only generate capture code for the exact RXCUIs provided.  Do not expand to a list of related
                         RXCUIs
    -h, --help           Prints help information
        --ingredients    Assume the provided RXCUIs refer to the active ingredient(s) directly. Do not automatically
                         look up the active ingredient of each RXCUI
    -V, --version        Prints version information

OPTIONS:
        --from-drug <drug-name>      Generate based on drug name
        --from-rxcuis <rxcuis>...    Generate based on one or more RXCUIs
        --variable <var-name>        Name of variable to generate

```

### Minimal Example

To see and understand what abcde-rxnorm is doing, consider the following example:

> Tip: Pipe the output to a file for later use.
> 
> ```
> abcde-rxnorm ... > code.txt
> ```

```
abcde-rxnorm generate --exact --variable placebo --from-rxcuis 8375 6211
```
```
// true if taking, false otherwise
let placebo = ( if medsy01.medinv_plus_rxnorm_med1_p == None || medsy01.medinv_plus_rxnorm_med1_p != "6211 Lactose" { false } else { true } ) 
|| ( if medsy01.medinv_plus_otc_med1_rxnorm_p == None || medsy01.medinv_plus_otc_med1_rxnorm_p != "6211 Lactose" { false } else { true } )
|| ( if medsy01.medinv_plus_rxnorm_med2_p == None || medsy01.medinv_plus_rxnorm_med2_p != "6211 Lactose" { false } else { true } )
|| ( if medsy01.medinv_plus_otc_med2_rxnorm_p == None || medsy01.medinv_plus_otc_med2_rxnorm_p != "6211 Lactose" { false } else { true } )
...
|| ( if medsy01.medinv_plus_rxnorm_med15_p == None || medsy01.medinv_plus_rxnorm_med15_p != "6211 Lactose" { false } else { true } )
|| ( if medsy01.medinv_plus_otc_med15_rxnorm_p == None || medsy01.medinv_plus_otc_med15_rxnorm_p != "6211 Lactose" { false } else { true } )
|| ( if medsy01.medinv_plus_rxnorm_med1_p == None || medsy01.medinv_plus_rxnorm_med1_p != "8375 placebo" { false } else { true } )
|| ( if medsy01.medinv_plus_otc_med1_rxnorm_p == None || medsy01.medinv_plus_otc_med1_rxnorm_p != "8375 placebo" { false } else { true } )
|| ... ;
// true if taken in last 24 hours,
// false if not taken in last 24 hours,
// None if no response
let placebo_24 = ( if medsy01.medinv_plus_rxnorm_med1_p == None || medsy01.medinv_plus_rxnorm_med1_p != "6211 Lactose" { false } else { medsy01.medinv_plus_rx_med1_24_p as bool } )
|| ( if medsy01.medinv_plus_otc_med1_rxnorm_p == None || medsy01.medinv_plus_otc_med1_rxnorm_p != "6211 Lactose" { false } else { medsy01.medinv_plus_otc_med1_24_p as bool } )
|| ... ;
```

* `--variable` is the name of the variable to generate, in this case `placebo`.
* `--from-rxcuis` generates code for one or more RXCUIs, which are separated by spaces on the command line.
* `--exact` generates code for _exactly_ the RXCUIs given.  More on that in the next section.

The generated code was shorted by ellipses `...`.  We can see it starts with a comment and then the creation of a variable `let placebo = ...`.  The variable is boolean.  It checks each subjects' 15 prescription and 15 over-the-counter drugs to see if any matches the RXCUI 8375, which is for placebo drugs.  Note that if a query is left blank (i.e. `== None`) we assume the subject is _not_ taking the drug.

The process is repeated for RXCUI 6211 lactose, a type of sugar used in sugar pills (i.e. placebo).  The variable `placebo` will be true if the subject is taking either 8375 or 6211, or false otherwise (even if the subject left all the questions blank).

We then generate a second variable `placebo_24` that is true if the drug in question was taken in the last 24 hours.  Again, missing responses (`None`) are interepreted as `false`.

### Automatic Expansion

Looking up the correct list of RXCUIs by hand is tedious.  Each formulation of each medication has a different unique identifier.  For example, the generic ingredient methylphenidate has RXCUI 6901.  An 18 mg tablet of delayed-release methylphenidate manufactured by Janssen Pharmaceuticals as Concerta has the RXCUI 1091157.  A 27 mg tablet of Concerta has a different RXCUI 1091172.  Another 27 mg tablet of methylphenidate under a different brand/manufacturer would have yet another RXCUI.

Typically we are not interested in the specific formulation of a drug the subject is taking, but rather whether the subject is taking any form of the drug.  This requies matching any of possibly hundreds of RXCUIs related to that drug.  Fortunately, if we omit the `--exact` flag then abcde-rxnorm will automatically perform this expansion for us.  Either one of the following two commands generates the same code:

```
abcde-rxnorm --variable methylphenidate --from-drug concerta
abcde-rxnorm --variable methylphenidate --from-rxcui 284704
```

* The first command will look up the RXCUI of "concerta", which is 284704.  It will then behave as if we had called the second command.
* The second command will look up the active ingredient of 284704 Concerta, which is 6901 methylphenidate.
* It will then expand RXCUI 6901 to a list of all RXCUIs with that active ingredient.
* Finally, it generates code to test if the subject is taking any of those related RXCUIs.

### Specifying Ingredients

In the above example we generated code using the brand name Concerta.  Sometimes this generates ambiguity, in particular for drugs with more than one active ingredient.  You can tell abcde-rxnorm to assume the provided drug or RXCUI(s) is/are the active ingredient(s) with the `--ingredients` flag.  The following two commands generate the same code as the two commands in the section above:

```
abcde-rxnorm --variable methylphenidate --from-drug methylphenidate --ingredients
abcde-rxnorm --variable methylphenidate --from-rxcui 6901 --ingredients
```

## Using RxNorm

This section and its subsections describe how to interact with the NIH's [RxNorm](https://www.nlm.nih.gov/research/umls/rxnorm/index.html) database.  Ideally the abcde-rxnorm tool should spare you from needing to do this, but understanding how the database works may help you to use abcde-rxnorm better or deal with exceptional/corner cases.

#### RxNav

You can browse for drug names and corresponding RXCUI identifiers in the interactive web interface of the RxNorm database, which is called [RxNav](https://mor.nlm.nih.gov/RxNav/).  Follow the steps illustrated in red in the image below.

1. Enter a search string in the search bar, e.g. methylphenidate.
2. Select the active ingredient of the drug near the upper-left corner of the search results.  In this example it is methylphenidate.
3. Select the download link near the bottom left of the search results.  The link will direct you to a comma-separated value (csv) table listing all of the RXCUIs associated with the active ingredient.  In this example there are 285 RXCUIs for the ingredient methylphedniate.

> Be careful, not all the related RXCUIs are specific to the ingredient you search for.  For example, RXCUI 1151133 simply means "pill" and would be associated with all ingredients available in pill form.

![RxNav Interface](./rxnav.jpg)

#### RxNorm API

The [RxNorm API](https://rxnav.nlm.nih.gov/RxNormAPIs.html) is an alternative to [RxNav](https://mor.nlm.nih.gov/RxNav/) that you may find more convenient if you intend to interact with the database programmatically.  It also allows you to be a little bit more selective in the types of related RXCUIs you search for, i.e. excluding broad RXCUIs such as pill, oral product, etc.

RxNorm is a [REST](https://en.wikipedia.org/wiki/Representational_state_transfer) API that responds in [XML](https://en.wikipedia.org/wiki/XML).

To look up the RXCUI of a drug:

```
https://rxnav.nlm.nih.gov/REST/rxcui?name=concerta
```
```
...
<name>concerta</name>
<rxnormId>284704</rxnormId>
...
```

To look up the active ingredients given the RXCUI:

```
https://rxnav.nlm.nih.gov/REST/rxcui/284704/related?tty=IN
```
```
...
<rxcui>6901</rxcui>
<name>Methylphenidate</name>
...
```

Once you know the RXCUI of the active ingredient, you can get a list of all related RXCUIs (excluding the useless dose forms DF and dose form groups DFG).

```
https://rxnav.nlm.nih.gov/REST/rxcui/6901/related?tty=BN+BPCK+GPCK+IN+MIN+PIN+SBD+SBDC+SBDF+SBDG+SCD+SCDC+SCDF+SCDG
```
```
...
<rxcui>2168839</rxcui>
<name>Jornay</name>
...
<rxcui>224917</rxcui>
<name>Ritalin</name>
...
<rxcui>261572</rxcui>
<name>Methylin</name>
...
...
<rxcui>6901</rxcui>
<name>Methylphenidate</name>
...
```

The `BN`, `BPCK`, etc above are "term types" or TTY.  For a list of available term types see https://rxnav.nlm.nih.gov/REST/termtypes, and for glossary of their meanings https://www.nlm.nih.gov/research/umls/rxnorm/docs/appendix5.html.

#### Combination Products

The example gets a little more complicated with drugs that are part of combination products with multiple active ingredients.  Some drugs are often taken together, and it is not uncommon for the active ingredients to be mixed and sold as a single combination drug for convenience.

Take, for example, lisinopril (RXCUI 29046), a drug for lowering blood pressure:

```
https://rxnav.nlm.nih.gov/REST/rxcui/29046/related?tty=IN
```
```
...
<rxcui>29046</rxcui>
<name>lisinopril</name>
...
```

So far so good, but now try using the multiple-ingredient MIN TTY:

```
https://rxnav.nlm.nih.gov/REST/rxcui/29046/related?tty=MIN
```
```
...
<rxcui>214618</rxcui>
<name>Hydrochlorothiazide / Lisinopril</name>
...
```

Lisinopril is found in a multiple-ingredient named hydrochlorothiazide / lisinopril.  Hydrochlorothiazide is another drug for lowering blood pressure that is syngergistic with lisinopril, so it is not suprising that they would be marketed in a combination product.

For consistency, lets look up ingredients of the combination product 214618.  Sure enough it contains lisinopril and hydrochlorothiazide:

```
https://rxnav.nlm.nih.gov/REST/rxcui/214618/related?tty=IN
```
```
...
<rxcui>29046</rxcui>
<name>Lisinopril</name>
...
<rxcui>5487</rxcui>
<name>Hydrochlorothiazide</name>
...
```

Suppose now that we want to identify subjects taking lisinopril.  We can use the _same_ search for related RXCUIs as we did before.  Observe that we get RXCUIs for lisinopril _and_ for combined hydrochlorothiazide-lisinopril products.  This makes sense, since a subject taking the combined product is still taking lisinopril.

```
https://rxnav.nlm.nih.gov/REST/rxcui/29046/related?tty=BN+BPCK+GPCK+IN+MIN+PIN+SBD+SBDC+SBDF+SBDG+SCD+SCDC+SCDF+SCDG
```
```
...
<rxcui>104375</rxcui>
<name>Lisinopril 2.5 MG Oral Tablet [Zestril]</name>
...
<rxcui>207965</rxcui>
<name>
Hydrochlorothiazide 25 MG / Lisinopril 20 MG Oral Tablet [Prinzide]
</name>
...
```

If we would like to identify _only_ those subjects who are taking the combined product then we would use a slightly _different_ search.  This search gives us only the RXCUIs for the combination product.  (To do this with abcde-rxnorm use the `--ingredients` flag and `--from-rxcui 214618`.)

```
https://rxnav.nlm.nih.gov/REST/rxcui/214618/related?tty=BN+BPCK+GPCK+MIN+PIN+SBD+SBDC+SBDF+SBDG+SCD+SCDF+SCDG
```
```
...
<rxcui>207965</rxcui>
<name>
Hydrochlorothiazide 25 MG / Lisinopril 20 MG Oral Tablet [Prinzide]
</name>
...
```

Beware, however, that a subject could be taking the lisinopril and hydrochlorothiazide ingredients separately as two different pills.  If you want to capture taking both drugs either seperately or in a combined product then your best strategy is to generate a capture for lisinopril, generate another capture for hydrochlorothiazide, and then capture the boolean intersection of both:

```
let lisinopril = ...;
let hydrochlorothiazide = ...;
select where lisinopril && hydrochlorothiazide;
```
