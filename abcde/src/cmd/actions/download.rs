//! Functions that perform actions for the download subcommand.

use std::io::Write;
use sha1::{Sha1, Digest};
use futures::stream::StreamExt;
use futures::stream::TryStreamExt;
use tokio::io::AsyncWriteExt;
// Import top-level objects from crate.
use crate::*;

/// Perform the download action/command described by `opts`.
/// Uses an asynchronous event model driven by tokio.
#[tokio::main]
pub async fn download(opts: cmd::opts::DownloadOpts) -> Result<(), BoxError> {
    // Make sure data directory is empty and writable.
    check_data_dir(&opts.common.data_dir)?;
    
    // Prompt for username and/or password interactively if they were not
    // provided on the command line.
    let (user, pass) = get_credentials(&mut opts.download.user, &mut opts.download.pass).await?;
    
    // Get URIs to download for the given package.
    // There will be a set of Amazon Web Services (AWS) S3 URIs.
    // There will also be some standard URLs for data dictionary files.
    if opts.common.verbose {
        eprint!("Downloading package manifest...");
        std::io::stderr().lock().flush()?;
    }
    let Uris { s3_uris, nda_data_struct_names } = get_package_uris(opts.download.package_id).await?;
    if opts.common.verbose {
        eprintln!(" done.");
    }
    
    // Get Amazon Web Services (AWS) credentials using our NDA user and pass.
    // TODO: for now we are just generating the AWS credentials once and
    // creating a rusoto_s3::S3Client swith rusoto_credential::StaticProvider.
    // Unfortunately this means out client will stop working when the
    // credentials expire, which will typically be about 16 hours after we call
    // this function.  In the future we will want to create a custom object that
    // implements rusoto_credential::ProvideAwsCredentials and then wrap that in
    // an rusoto_credential::AutoRefreshingProvider.
    if opts.common.verbose {
        eprint!("Authenticating with Amazon Web Services...");
        std::io::stderr().lock().flush()?;
    }
    let aws_credentials = get_aws_credentials(user, pass).await?;
    if opts.common.verbose {
        eprintln!(" done.");
    }
    
    // Looks like we are good to go!
    // Create a directory into which we will download dictionary files.
    let nda_dictionary_dest = opts.common.data_dir.join("dictionary");
    std::fs::create_dir(&nda_dictionary_dest)?;
    
    // Clone/copy some local variables so that we can move them into the async
    // block.
    let uri_base = reqwest::Url::parse("https://nda.nih.gov/api/datadictionary/v2/datastructure/")?;
    let verbose = opts.common.verbose;
    
    // Start downloading from NDA server, up to 2 downloads at a time.
    let download_nda = tokio::spawn(async move {
        let client = reqwest::Client::new();
        let len = nda_data_struct_names.len();
        futures::stream::iter(nda_data_struct_names).enumerate() // TODO convert to tokio::stream once available
            .map(|(seq, data_struct_name)| {
                // Function call required pending stabilization of async
                // closures, which would allow us to specify the polymorphic
                // error type BoxError without the function call.
                download_nda_data_struct_dict(Some(seq), data_struct_name, &nda_dictionary_dest, &client, &uri_base)
            })
            .buffer_unordered(2) // 2 downloads from NDA server in parallel
            .try_for_each(|seq| async move {
                if verbose {
                    // OK to unwrap seq: Option<usize>; we are just getting back
                    // exactly the same value we put into
                    // download_nda_data_struct_dict() above.
                    eprintln!("Downloaded NDA data structure dictionary {} of {}.", seq.unwrap() + 1, len);
                }
                Ok(())
            })
            .await
    });
    
    // Download data files from AWS.
    let s3_dest = opts.common.data_dir.clone();
    let verbose = opts.common.verbose;
    let download_s3 = tokio::task::spawn(async move {
        let client =  rusoto_s3::S3Client::new_with(
            rusoto_core::request::HttpClient::new()?,
            rusoto_credential::StaticProvider::from(aws_credentials),
            rusoto_core::region::Region::default(),
        );
        let len = s3_uris.len();
        futures::stream::iter(s3_uris).enumerate()
            .map(|(seq, s3_uri)| {
                download_s3_uri(Some(seq), s3_uri, &s3_dest, &client)
            })
            .buffer_unordered(2) // 2 downloads from Amazon S3 in parallel
            .try_for_each(|seq| async move {
                if verbose {
                    eprintln!("Downloaded AWS/S3 data file {} of {}.", seq.unwrap() + 1, len);
                }
                Ok(())
            })
            .await
    }); 
    
    // Run downloads in parallel.
    download_s3.await??;
    download_nda.await??;
    
    // Done
    if verbose {
        eprintln!("All downloads completed successfully.");
    }
    Ok(())
}

async fn download_s3_uri<T: rusoto_s3::S3>(seq: Option<usize>, s3_uri: S3Uri, dest_dir: &std::path::Path, client: &T) -> Result<Option<usize>, BoxError> {
    let request = rusoto_s3::GetObjectRequest{
        bucket: s3_uri.bucket.clone(),
        key: s3_uri.key.clone(),
        ..Default::default()
    };
    
    let response = client.get_object(request).await?;
    
    match response.body {
        None => return Err(cmd::Error{
            what: format!("Response from AWS S3 server is missing body for: /{}/{}", s3_uri.bucket, s3_uri.key),
            source: None,
        }.into()),
        Some(body) => {
            // Create destination directory, if needed.
            if let Some(parent) = s3_uri.dest.parent() {
                tokio::fs::create_dir_all(dest_dir.join(parent)).await?;
            }
            
            // Create empty destination file.
            let mut outfile = tokio::fs::File::create(dest_dir.join(s3_uri.dest)).await?;
            
            // Download the file.
            tokio::io::copy(&mut body.into_async_read(), &mut outfile).await?;
        }
    }
    
    Ok(seq)
}

// Download an NDA dictionary data structure into the dictionary subdirectory
// within the data directory.  Takes an optional usize describing the sequence
// of the uri to be downloaded.  Does nothing with the sequence except to return
// it upon completion.
async fn download_nda_data_struct_dict(seq: Option<usize>, data_struct_name: String, dest_dir: &std::path::Path, client: &reqwest::Client, uri_base: &reqwest::Url) -> Result<Option<usize>, BoxError> {
    // Send request to the server and await a response.
    let url = uri_base.join(&data_struct_name)?.join("csv")?;
    let mut response = client.get(url).send().await?;
    
    // Check the status of the response.  It should be 200 OK.
    if response.status() != reqwest::StatusCode::OK {
        return Err(cmd::Error{
            what: format!("Error downloading data dictionary structure.\nGot status {} from {}.", response.status(), uri_base.join(&data_struct_name)?.join("csv")?),
            source: None,
        }.into());
    }
    
    // Create the output file and wrap it in a write buffer.
    let outfile = tokio::fs::File::create(dest_dir.join(data_struct_name).with_extension("txt")).await?;
    let mut outfile = tokio::io::BufWriter::new(outfile);
    
    // Do an asyncronous, buffered copy of the download to the output file.
    while let Some(chunk) = response.chunk().await? {
        outfile.write(&chunk).await?;
    }
    outfile.flush().await?;
    
    // Done.  Return the sequence number so the caller can keep track of which
    // download was completed.
    Ok(seq)
}

// Make sure the data directory is empty and writable.
fn check_data_dir(data_dir: &std::path::Path) -> Result<(), BoxError> {
    // Is directory empty?
    if std::fs::read_dir(data_dir)?.count() != 0 {
         return Err(cmd::Error{
            what: format!("Data download directory is not empty: {}", data_dir.display()),
            source: None,
        }.into());
    }
    
    // Is directory writeable?
    if data_dir.metadata()?.permissions().readonly() {
         return Err(cmd::Error{
            what: format!("Data download directory is read-only: {}", data_dir.display()),
            source: None,
        }.into());
    }
    
    // Passed checks.
    Ok(())
}

// Interactively prompt for username and/or password if not already provided in 
// command line options.
async fn get_credentials<'a, 'b>(user: &'a mut Option<String>, pass: &'b mut Option<cmd::Password>) -> Result<(&'a str, &'b cmd::Password), BoxError> {
    // Make copies of username and password that we can move into the async
    // block so their lifetimes will be long enough for spawn.
    let user_copy = (*user).clone();
    let pass_copy = (*pass).clone();
    
    // Since we are going to block on user input, we must spawn this task on a
    // blocking scheduler.  Unfortunately rpassword is not asynchronous.
    let (user_new, pass_new) = tokio::task::spawn_blocking(move || -> Result<_, BoxError> {
        // Prompt for user name if not already known.
        let user = match user_copy {
            Some(user) => user,
            None => {
                {
                    print!("Enter your NDA username: ");
                    std::io::stdout().lock().flush()?;
                }
                let mut user_input = String::new();
                std::io::stdin().read_line(&mut user_input)?;
                let newline: &[_] = &['\r', '\n'];
                user_input.truncate(user_input.trim_end_matches(newline).len());
                user_input
            },
        };
        
        // Prompt user for password if not already known.
        let pass = match pass_copy {
            Some(pass) => pass,
            None => {
                cmd::Password::new(rpassword::prompt_password_stdout("Enter your NDA password: ")?)
            },
        };
        // Done
        Ok((user, pass))
    }).await??;
    
    *user = Some(user_new);
    *pass = Some(pass_new);
    
    Ok((user.as_ref().unwrap(), pass.as_ref().unwrap()))
}

// Get a list of uniform resource locators (URIs) associated with a package.
// See https://github.com/NDAR/nda-tools
async fn get_package_uris(package_id: u64) -> Result<Uris, BoxError> {
    // URL to query for AWS S3 links
    let url = "https://nda.nih.gov/DataManager/dataManager";
    
    let body = format!(
"<?xml version=\"1.0\" ?>
<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">
    <S:Body>
        <ns3:QueryPackageFileElement
            xmlns:ns4=\"http://dataManagerService\"
            xmlns:ns3=\"http://gov/nih/ndar/ws/datamanager/server/bean/jaxb\"
            xmlns:ns2=\"http://dataManager/transfer/model\">
            <packageId>{}</packageId>
            <associated>true</associated>
        </ns3:QueryPackageFileElement>
    </S:Body>
</S:Envelope>", package_id);

    // Create a client to communicate with the server.
    let client = reqwest::Client::new();
    
    // Formulate a post request.
    let request = client
        .post(url)
        .header(reqwest::header::CONTENT_TYPE, "text/xml")
        .header("SOAPAction", "associatedFiles")
        .body(body);
    
    // Send the request and block until we get a response.
    let response = request.send().await?;
    
    // Check the status of the response.  It should be 200 OK.
    if response.status() != reqwest::StatusCode::OK {
        return Err(cmd::Error{
            what: format!("Error getting package uris.\nGot status {} from {}.", response.status(), url),
            source: None,
        }.into());
    }
    
    // Receive the body/text of the response.
    let response = response.text().await?;
    
    // Typical response looks like this.  Note that if the package ID doesn't exist the server will
    // not respond with an error.  It will just give us an empty manifest (i.e. no queryPackageFiles
    // elements) instead.
    // <?xml version='1.0' encoding='UTF-8'?>
    // <S:Envelope xmlns:S="http://schemas.xmlsoap.org/soap/envelope/">
    //   <S:Body>
    //     <ns3:associatedFiles xmlns:ns2="http://dataManager/transfer/model" xmlns:ns3="http://dataManagerService">
    //       <queryPackageFiles>
    //         <id>703854882</id>
    //         <packageId>122816</packageId>
    //         <path>/gpop/ndar_data/QueryPackages/PRODDB/sharedpackages/Package_122816/abcd_ant01.txt</path>
    //         <alias>abcd_ant01.txt</alias>
    //         <sftpSiteId>50</sftpSiteId>
    //         <fileSize>4436194</fileSize>
    //         <isAssociated>false</isAssociated>
    //       </queryPackageFiles>
    //       <queryPackageFiles>
    //         <id>703854883</id>
    //         <packageId>122816</packageId>
    //         <path>/gpop/ndar_data/QueryPackages/PRODDB/sharedpackages/Package_122816/abcd_asrs01.txt</path>
    //         <alias>abcd_asrs01.txt</alias>
    //         <sftpSiteId>50</sftpSiteId>
    //         <fileSize>6438664</fileSize>
    //         <isAssociated>false</isAssociated>
    //       </queryPackageFiles>
    //       <!-- ...additional queryPackageFiles elements... -->
    //     </ns3:associatedFiles>
    //   </S:Body>
    // </S:Envelope>
    
    // Parse the XML response to extract a list of package files.
    let envelope: xml::manifest::ManifestEnvelope = quick_xml::de::from_str(&response)?;
    let manifest = envelope.body.associated_files.query_package_files;
    
    // There should be at least one file to download in the manifest.
    if manifest.is_empty() {
        return Err(
            cmd::Error {
                what: format!("Manifest for package {} is empty.", package_id),
                source: None,
            }.into()
        );
    }
    
    // Initialize an empty `Uris` structure.
    let mut uris = Uris {
        s3_uris: Vec::new(),
        nda_data_struct_names: Vec::new(),
    };
    
    // Iterate over manifest and insert each file into the uri list.
    let re = regex::Regex::new(r"\d+\.txt$").unwrap(); // pre-compile regular expression, see below
    for file in manifest {
        // Extract desired information from xml.
        let package_file = get_package_file_from_xml(file)?;
        
        // Inspect the destination name and deduce whether the package file
        // is a data object that has an associated data dictionary.  For the
        // most part, all data objects end with a number and .txt, e.g.
        // data_file01.txt
        if
        re.is_match(&package_file.dest_name) && // files ending in # and .txt
        !package_file.dest_name.contains('/') { // exclude any files in subdirectories
            // Strip .txt and push onto vector of NDA dictionary URIs.
            // Need to add trailing / due to odd semantics of
            // `reqwest::Url::push`.
            let mut data_struct_name = package_file.dest_name.trim_end_matches(".txt").to_string();
            data_struct_name.push('/');
            uris.nda_data_struct_names.push(data_struct_name);
        }
        
        // Push onto vector of S3 URIs.
        uris.s3_uris.push(S3Uri{
            bucket: package_file.bucket,
            key: package_file.key,
            dest: std::path::PathBuf::from(package_file.dest_name), // TODO make sure destination path is a subdirectory of the data directory
        });
    }
    
    Ok(uris)
}

// Parse a QueryPackageFiles structure.
fn get_package_file_from_xml(file: xml::manifest::QueryPackageFiles) -> Result<PackageFile, BoxError> {
    // Split path into Amazon Web Services (AWS) S3 bucket and key.
    // If path starts with / then skip that first /.
    let path = file.path.trim_start_matches("/");
    // Split on the remaining /.
    let mut path_parts = path.split('/');
    // Take the first part as the bucket.
    let bucket = match path_parts.next() {
        Some(bucket) => bucket.to_string(),
        None => return Err(cmd::Error{
            what: "Error parsing QueryPackageFiles element: path is too short.".into(),
            source: None,
        }.into()),
    };
    let key = path_parts.collect::<Vec<&str>>().join("/");
    
    Ok(PackageFile{
        bucket,
        key,
        dest_name: file.alias,
    })
}

/// Structure that describes the relevant information in a QueryPackageFiles
/// tag, see:
/// https://github.com/NDAR/nda-tools
#[derive(Debug)]
struct PackageFile {
    /// Amazon Web Services (AWS) S3 bucket (first part of path)
    bucket: String,
    
    /// AWS S3 key (second part of path)
    key: String,
    
    /// Name of destination file.  Not a full path.
    dest_name: String,
}

// Get aws token
// See https://github.com/NDAR/nda_aws_token_generator
async fn get_aws_credentials(user: &str, pass: &cmd::Password) -> Result<rusoto_credential::AwsCredentials, BoxError> {
    // Hash the password with SHA1.
    let pass = Sha1::new().chain_update(pass.get()).finalize();
    
    // URL where we will send the request.
    let url = "https://nda.nih.gov/DataManager/dataManager";
    
    // Formulate body of the request.
    let body = format!(
"<?xml version=\"1.0\" ?>
<S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\">
    <S:Body>
        <ns3:UserElement xmlns:ns4=\"http://dataManagerService\"
                         xmlns:ns3=\"http://gov/nih/ndar/ws/datamanager/server/bean/jaxb\"
                         xmlns:ns2=\"http://dataManager/transfer/model\">
            <user>
                <id>0</id>
                <name>{}</name>
                <password>{:x}</password>
                <threshold>0</threshold>
            </user>
        </ns3:UserElement>
    </S:Body>
</S:Envelope>", user, pass);
    
    // Create a client to communicate with the server.
    let client = reqwest::Client::new();
    
    // Formulate a post request.
    let request = client
        .post(url)
        .header(reqwest::header::CONTENT_TYPE, "text/xml")
        .header("SOAPAction", "generateToken")
        .body(body);
    
    // Send the request and block until we get a response.
    let response = request.send().await?;
    
    // Check the status of the response.  It should be 200 OK.
    if response.status() != reqwest::StatusCode::OK {
        return Err(cmd::Error{
            what: format!("Error getting AWS token.\nGot status {} from {} with user {}.", response.status(), url, user),
            source: None,
        }.into());
    }
    
    // Download the body of the response and convert it to a String.
    let response = response.text().await?;
    
    // Examples of possible output:
    
    // Successful authentication
    // <?xml version=\'1.0\' encoding=\'UTF-8\'?><S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\"><S:Body><ns3:generateToken xmlns:ns2=\"http://dataManager/transfer/model\" xmlns:ns3=\"http://dataManagerService\"><accessKey>ASIAZAAXFM2FLR7NHDLV</accessKey><secretKey>4t1U/ZvsQfu9ZQ06BfrHQ7PQ9l1c6dbvR/BkDRX+</secretKey><sessionToken>FwoGZXIvYXdzEIz//////////wEaDMS1TznnrGFzMdChiCKRAdqoPrthQZJymg/btqNRS+xz9Wqk0RCV7mrInAUhoklBZfxLH9ZAngepD63thKI+1rqZWJTzXFlaDiu5ITrxF9EyD1OByvNKLfBak5jLmT7NG2Fmq+pbwKNe3XvYoA+hITUsalgOcjVJgZ+3SRwlTlvroY5e1x7ri6EsLptpKdthv9R9+RkmVlzznMENmL51ubEo5tLT8QUyKVdNispmVNjfDOGfeyYCBae2zbs5N/jQAiMUTU5rUsI2GofPFAIRjG1d</sessionToken><expirationDate>2020-02-01T14:58:46Z</expirationDate></ns3:generateToken></S:Body></S:Envelope>
    
    // Authentication failure
    // <?xml version=\'1.0\' encoding=\'UTF-8\'?><S:Envelope xmlns:S=\"http://schemas.xmlsoap.org/soap/envelope/\"><S:Body><ns3:generateToken xmlns:ns2=\"http://dataManager/transfer/model\" xmlns:ns3=\"http://dataManagerService\"><errorMessage>Invalid username and/or password</errorMessage></ns3:generateToken></S:Body></S:Envelope>
    
    // Parse response as xml using serde and quick-xml.
    let envelope: xml::aws::AwsTokenEnvelope = quick_xml::de::from_str(&response)?;
    match envelope.body.response {
        xml::aws::AwsTokenResponse::Token{key, secret, token, expires_at} => Ok(
            rusoto_credential::AwsCredentials::new(
                key,
                secret,
                token,
                expires_at,
            )
        ),
        xml::aws::AwsTokenResponse::Err(msg) => Err(
            cmd::Error {
                what: format!("Couldn't obtain AWS credentials.  Server responded: {}", msg),
                source: None,
            }.into()
        ),
    }
}

/// Amazon Web Services (AWS) S3 uniform resource locator (URI)
#[derive(Debug)]
struct S3Uri {
    /// S3 bucket (the first part of the path)
    bucket: String,
    
    /// S3 key (the second part of the path)
    key: String,
    
    /// Local destination file path
    dest: std::path::PathBuf,
}

/// Simple structure to hold vectors of different types of URIs.
#[derive(Debug)]
struct Uris {
    s3_uris: Vec<S3Uri>,
    nda_data_struct_names: Vec<String>,
}

mod xml {
    //! Structures for deserializing xml with serde.
    
    // Use serde for deserializing xml.
    use serde::Deserialize;
    
    pub(super) mod aws {
        //! Structures for deserializing AWS Token Response.
        use super::Deserialize;
        
        // XML SOAP envelope containing an AWS authentication token or an error
        // message.  Deserialize server response to this structure. 
        #[derive(Debug, Deserialize)]
        pub struct AwsTokenEnvelope {
            #[serde(rename="Body")]
            pub body: AwsTokenBody,
        }
        
        // XML SOAP body containing an AWS authentication token or an error
        // message.  Do not desirialize from this structure directly.  Deserialize
        // from AwsTokenEnvelope instead.
        #[derive(Debug, Deserialize)]
        pub struct AwsTokenBody {
            #[serde(rename="generateToken")]
            pub response: AwsTokenResponse,
        }
        
        // XML generateToken element containing server's authentication response.
        // Can contain an AWS token or an error message.  This raw structure is
        // needed for the workaround described here:
        // https://github.com/tafia/quick-xml/issues/203
        // Use AwsTokenResponse instead.
        #[derive(Debug, Deserialize)] 
        struct AwsTokenResponseRaw {
            #[serde(rename="accessKey")]
            access_key: Option<String>,
            #[serde(rename="secretKey")]
            secret_key: Option<String>,
            #[serde(rename="sessionToken")]
            session_token: Option<String>,
            #[serde(rename="expirationDate")]
            expiration_date: Option<chrono::DateTime<chrono::offset::Utc>>,
            #[serde(rename="errorMessage")]
            error_message: Option<String>,
        }
            
        // XML generateToken element containing server's authentication response.
        // Can contain an AWS token or an error message.  Do not deserialize from
        // this structure directly.  Deserialize from AwsTokenEnvelope instead.
        #[derive(Debug, Deserialize)]
        #[serde(try_from = "AwsTokenResponseRaw")]
        pub enum AwsTokenResponse {
            #[allow(unused)]
            Token {
                key: String,
                secret: String,
                token: Option<String>,
                expires_at: Option<chrono::DateTime<chrono::offset::Utc>>,
            },
            #[allow(unused)]
            Err(String)
        }
        impl std::convert::TryFrom<AwsTokenResponseRaw> for AwsTokenResponse {
            type Error = &'static str;
            fn try_from(raw: AwsTokenResponseRaw) -> Result<Self, Self::Error> {
                // If the server responded with any kind of error message then
                // default to Err.
                if let Some(msg) = raw.error_message {
                    return Ok(Self::Err(msg));
                }
                
                // Try to extract AWS token elements.
                if let (Some(key), Some(secret)) = (raw.access_key, raw.secret_key) {
                    return Ok(Self::Token{
                        key,
                        secret,
                        token: raw.session_token,
                        expires_at: raw.expiration_date
                    });
                }
                
                // If we get to this point then something went wrong.
                Err("Malformed XML server response.  Could not find complete AWS token nor an error message in generateToken element.")
            }
        }
    }
    
    pub(super) mod manifest {
        //! Package manifest containing S3 URIs for NDA package.
        use super::Deserialize;
        
        // XML SOAP envelope containing a package manifest or error
        // message.  Deserialize server response to this structure. 
        #[derive(Debug, Deserialize)]
        pub struct ManifestEnvelope {
            #[serde(rename="Body")]
            pub body: ManifestBody,
        }
        
        // XML SOAP body containing list of associated files.
        #[derive(Debug, Deserialize)]
        pub struct ManifestBody {
            #[serde(rename="associatedFiles")]
            pub associated_files: AssociatedFiles,
        }
        
        // Element containing vector of zero or more package files.
        #[derive(Debug, Deserialize)]
        pub struct AssociatedFiles {
            #[serde(rename="queryPackageFiles")]
            pub query_package_files: Vec<QueryPackageFiles>,
        }
        
        // Element containing properties of one package file.
        #[derive(Debug, Deserialize)]
        pub struct QueryPackageFiles {
            pub path: String,
            pub alias: String,
        }
    }
}
