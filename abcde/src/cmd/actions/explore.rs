//! Functions that perform actions for the explore subcommand.

// Import top-level objects from crate.
use crate::*;

/// Perform the explore action/command described by `opts`.
pub fn explore(opts: cmd::opts::ExploreOpts) -> Result<(), BoxError> {
    // List queries in a table.
    if opts.explore.list {
        match opts.explore.table {
            Some(ref table) => {
                let table = explorer::QueryFile::new(&opts.common.data_dir, table)?;
                for query in table.queries.into_iter() {
                    println!("{}", query);
                }
            }
            None => {
                return Err(cmd::Error{
                    what: "Missing argument for table to list queries in.".into(),
                    source: None,
                }.into());
            },
        }
        return Ok(())
    }
    
    // Get information about a query.
    if let Some(query) = &opts.explore.query {
        // Parse the file and field portion of the query.
        let mut split = query.splitn(2, '.');
        let file: &str;
        let field: &str;
        match split.next() {
            Some(f) => { file = f; }, 
            None => {
                return Err(cmd::Error{
                    what: format!("Malformed query missing table component: {}", query),
                    source: None,
                }.into());
            },
        }
        match split.next() {
            Some(f) => {
                // If field name contains a . and is escaped by quotes then
                // strip the quotes.
                field = (|| {
                    if f.contains('.') {
                        if let Some(c) = f.chars().next() {
                            if c == '"' {
                                if let Some(c) = f.chars().last() {
                                    if c == '"' {
                                        return &f[0..f.len()-1] // strip
                                    }
                                }
                            }
                        }
                    }
                    f // don't strip
                })();
            },
            None => {
                return Err(cmd::Error{
                    what: format!("Malformed query missing field component: {}", query),
                    source: None,
                }.into());
            },
        }
        
        // Read in the header of the file this query is supposedly in.
        let mut f = explorer::QueryFile::new(&opts.common.data_dir, file)?;
        
        // Make sure the query is in the file.
        let index = match f.queries.iter().position(|q| q.field == field) {
            Some(index) => index,
            None => {
                return Err(cmd::Error{
                    what: format!("Query with field {} not found in file {}.", field, file),
                    source: None,
                }.into());
            }
        };
        
        // Look up dictionary information for the file.
        f.dict_lookup()?;
        
        // Get 10 data samples from the file.
        f.try_read_samples(10)?;
        
        // Display information about the query.
        print!("{}", f.queries[index]);
        
        return Ok(());
    }
    
    // Search for queries based on a regex.
    if let Some(regex_str) = &opts.explore.regex {
        // Compile the regular expression.
        let regex = regex::Regex::new(&regex_str)?;
        
        // Initialize an empty vector into which we will push search results.
        let mut queries: Vec<explorer::QueryInfo> = Vec::new();
        
        // Search just one table or all tables?
        match &opts.explore.table {
            Some(table) => {
                let table = explorer::QueryFile::new(&opts.common.data_dir, table)?;
                table.take_where_regex(&mut queries, &regex);
            }
            None => {
                // Iterate over all tables in the data directory.
                let entries = std::fs::read_dir(&opts.common.data_dir)?;
                for entry in entries {
                    let entry = entry?.path();
                    if entry.is_file() {
                        if let Some(ext) = entry.extension() {
                            if ext == "txt" {
                                if let Some(table) = entry.file_stem() {
                                    if let Some(table) = table.to_str() {
                                        let table = explorer::QueryFile::new(&opts.common.data_dir, table)?;
                                        table.take_where_regex(&mut queries, &regex);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        
        // Display search results.
        for query in queries {
            println!("{}", query);
        }
        
        return Ok(());
    } 
    
    Err(cmd::Error{
        what: "Not enough arguments for explore subcommand.".into(),
        source: None,
    }.into())
}
