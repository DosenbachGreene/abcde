//! This module contains the `Password` type, which is used for securely storing
//! passwords.

/// Newtype pattern to store passwords.
/// Differs from String in that its Debug/Display traits do not print out the
/// password, but instead print out "****".
#[derive(Clone)]
pub struct Password(String);
impl Password {
    // Create a new password that stores the argument `password`.
    #[allow(unused)]
    pub fn new<S: Into<String>>(password: S) -> Self {
        Self(password.into())
    }
    
    // Set the stored password to a new value.
    #[allow(unused)]
    pub fn set<S: Into<String>>(&mut self, new_password: S) {
        self.0 = new_password.into();
    }
    
    // Get a reference to the stored password.
    #[allow(unused)]
    pub fn get(&self) -> &str {
        &self.0
    }
    
    // Consume the password object, returning a String to the stored password.
    #[allow(unused)]
    pub fn into_string(self) -> String {
        self.0
    }
}
impl std::fmt::Display for Password {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	    write!(f, "****")
	}
}
impl std::fmt::Debug for Password {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	    write!(f, "****")
	}
}
impl std::str::FromStr for Password {
    type Err = ParsePasswordError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Ok(Password::new(s))
    }
}

/// Parsing a password from an str cannot fail.  Define a simple error type that
/// does nothing.
#[derive(Debug)]
pub struct ParsePasswordError;
impl std::fmt::Display for ParsePasswordError {
	fn fmt(&self, _: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
	    Ok(())
	}
}
impl std::error::Error for ParsePasswordError { }

