//! Generic error type implementing `std::error::Error`.  Used by the `main.rs`
//! et al for error propagation.

use crate::BoxError;

/// Generic error type implementing `std::error::Error`.
#[derive(Debug)]
pub struct Error {
    /// What went wrong?
	pub what: String,
	/// What was the source/cause of this error, if any?
	pub source: Option<BoxError>
}

// implement Display trait
impl std::fmt::Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.what)?;
		if let Some(error) = &self.source {
			write!(f, "\nCaused by: {}", error)?;
		}
		Ok(())
	}
}

// implement Error trait
impl std::error::Error for Error {
	fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
		match &self.source {
			Some(error) => Some(error.as_ref()),
			None => None
		}
	}
}
