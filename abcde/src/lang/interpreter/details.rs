use super::*;
use std::io::BufRead;
use std::str::FromStr;
use crate::explorer::make_filename;

impl Interpreter {
    pub(super) fn as_proj(&mut self) -> InterpreterProj {
        InterpreterProj {
            code: &self.code,
            code_index: &mut self.code_index,
            // select_count: &self.select_count, // TODO appears not to be needed, eventually remove from projection
            // data_dir: &self.data_dir, // TODO appears not to be needed, eventually remove from projection
            queries: &self.queries,
            vars: &self.vars,
            types: &mut self.types,
            data: &mut self.data,
        }
    }

    // Make sure a file in the data directory exists for each query.
    pub(super) fn check_files_exist(&self) -> Result<(), std::io::Error> {
        // Iterate over unique query files.
        // Queries are ordered by file, so queries with duplicate files will be
        // consecutive.
        // Compare the file of each subsequent query to the last file checked,
        // and only check if the new file is different from the old.
        // Returns an error if any files could not be found.
        let mut prev_file = "";
        for (query, _) in &self.queries {
            if let Some(file) = &query.file {
                if file != prev_file {
                    prev_file = file;
                    let file = make_filename(&self.data_dir, file);
                    if !file.exists() {
                        return Err(std::io::Error::new(std::io::ErrorKind::NotFound, file.display().to_string()));
                    }
                }
            }
        }
        Ok(())
    }

    pub(super) fn perform_queries(&mut self) -> Result<(), InterpreterError> {
        // Queries are ordered by file, so queries with duplicate files will be
        // consecutive.
        // Batch groups of queries from the same file and perform them at the
        // same time.

        // Vector of fields within a file.
        // Field name, query/field type, and index within the data vector.
        let mut fields: Vec<(&str, ast::Type, usize)> = Vec::new();

        // Skip first however many queries do not have a file.
        let mut queries = self.queries.iter().skip_while(|(query, _)| query.file.is_none() ).peekable();

        // Store the first file encountered as the "previous" file.
        // OK to panic because ValidCode guarantees we will query at least one file.
        let mut prev_file = match queries.peek() {
            Some((query, _)) => match &query.file {
                Some(file) => file,
                None => panic!("No files were queried.")
            },
            None => panic!("No files were queried.")
        };

        for (query, index) in queries {
            if let Some(file) = &query.file {
                // Found new file, perform all queries on previous file at the same time.
                if file != prev_file {
                    perform_queries(
                        // Data table and number of columns to pre-allocate in each row.
                        (&mut self.data, self.types.len()),
                        // File in which to query fields.
                        (&self.data_dir, &prev_file),
                        // Fields (name, type, column index in data table).
                        &fields
                    )?;

                    // Reset loop state to start collecting queries from the
                    // next file on the next iteration.
                    prev_file = file;
                    fields.clear();
                }

                // Collect fields within the same file.
                let index = *index;
                fields.push((&query.field, self.types[index].unwrap(), index));
            }
        }
        // Perform collected queries from last file.
        perform_queries(
            // Data table and number of columns to pre-allocate in each row.
            (&mut self.data, self.types.len()),
            // File in which to query fields.
            (&self.data_dir, prev_file),
            // Fields (name, type, column index in data table).
            &fields
        )?;

        Ok(())
    }
}

// Reference projection.
pub(super) struct InterpreterProj<'a> {
    pub(super) code: &'a ast::Code,
    pub(super) code_index: &'a mut usize,
    // pub(super) select_count: &'a usize, // TODO appears not to be needed, eventually remove from projection
    // pub(super) data_dir: &'a std::path::Path, // TODO appears not to be needed, eventually remove from projection
    pub(super) queries: &'a BTreeMap<ast::Query, usize>,
    pub(super) vars: &'a HashMap<ast::Variable, usize>,
    pub(super) types: &'a mut Vec<Option<ast::Type>>,
    pub(super) data: &'a mut BTreeMap<Key, Vec<Option<ast::Value>>>,
}

impl<'a> InterpreterProj<'a> {
    // Evaluate a let statement, updating the data and types table.
    pub(super) fn evaluate_let_stmt(&mut self, stmt: &ast::Let) -> Result<(), TypeError> {
        // Get index of the variable on the lhs of the let statement.
        let index = self.vars[&stmt.lhs];

        // Update the types vector for this variable.
        let types = types::Types{queries: &self.queries, vars: &self.vars, types: &self.types};
        self.types[index] = Some(types.evaluate_expr_type(&stmt.rhs)?);

        // Iterate over each row in the data table, evaluate the current let
        // statement with data from that row, and then update the affected
        // variable in that row.
        for (key, row) in self.data.iter_mut() {
            // Evaluate the statement for this row.
            let eval_row = EvaluateRow{queries: &self.queries, vars: &self.vars, key: &key, row: &row};
            row[index] = eval_row.evaluate_expr(&stmt.rhs)?;
        }
        Ok(())
    }

    // Generate a vector of captures from a vector of symbols on the lhs of a
    // select statement.  Returns a tuple of symbols to be captured and which
    // special fields are going to be captured.
    pub(super) fn gen_captures(&self, symbols: &Vec<ast::Symbol>) -> Result<(Vec<ast::Symbol>, ContainsSpecialFields), InterpreterError> {
        // Figure out if subjectkey or interview_date were requested.
        let mut special_fields = ContainsSpecialFields::from_symbols(symbols);

        // Initialize empty vector of captures symbols.
        let mut captures: Vec<ast::Symbol>;

        // The rules for specifying special queries subjectkey and
        // interview_date in the captures are as follows:
        // 1. You can specify both.
        // 2. You can specify just subjectkey.
        // 2. You can specify neither, in which case both will
        //    automatically be prepended to the capture.
        //
        // If the user specifies subjectkey but not interview_date then the user
        // wants for each output row to be unique on subjectkey alone.

        // OK to ask for subjectkey without interview_date, but
        // mustn't ask for interview_date without subjectkey.
        if !special_fields.is_allowed() {
            return Err(SelectError{what: format!("Invalid combination of special keys in select statement: {}", symbols.iter().map(|s| s.to_string()).collect::<Vec<String>>().join(", "))}.into());
        }

        // Initialize captures.
        captures = Vec::with_capacity(symbols.len());

        // If no special fields were specified then add them all automatically.
        if special_fields.none() {
            // Add symbol to captures.
            captures.push(ast::Symbol{
                name: ast::SymbolName::Query(ast::Query{
                    file: None,
                    field: "subjectkey".into(),
                }),
                kind: Some(ast::Type::String),
            });
            // Keep track of which special fields are now contained in the
            // captures.
            special_fields.subjectkey = true;
            captures.push(ast::Symbol{
                name: ast::SymbolName::Query(ast::Query{
                    file: None,
                    field: "interview_date".into(),
                }),
                kind: Some(ast::Type::Date),
            });
            special_fields.interview_date = true;
            captures.push(ast::Symbol{
                name: ast::SymbolName::Query(ast::Query{
                    file: None,
                    field: "eventname".into(),
                }),
                kind: Some(ast::Type::String),
            });
            special_fields.eventname = true;
        }

        // If the select symbols vector is empty then automatically add
        // everything else too.
        if symbols.is_empty() {
            // Add the rest of the queries first.
            for (query, index) in self.queries {
                if query.file.is_none() {
                    // Don't double-add the special queries.
                    if query.field == "subjectkey" || query.field == "interview_date" {
                        continue;
                    }
                }
                else {
                    // Add the query to the captures.
                    captures.push(ast::Symbol{
                        name: ast::SymbolName::Query(ast::Query{
                            file: query.file.clone(),
                            field: query.field.clone(),
                        }),
                        kind: self.types[*index],
                    });
                }
            }

            // Now add all the variables.
            for (var, index) in self.vars {
                captures.push(ast::Symbol{
                    name: ast::SymbolName::Variable(var.clone()),
                    kind: self.types[*index],
                });
            }
        }

        let types = types::Types{queries: &self.queries, vars: &self.vars, types: &self.types};
        // Push the requested symbols and var types onto the captures vector.
        for symbol in symbols {
            match symbol.name {
                ast::SymbolName::Query(ref query) => captures.push(ast::Symbol{
                    name: symbol.name.clone(),
                    kind: types.query_type(query),
                }),
                ast::SymbolName::Variable(ref var) => captures.push(ast::Symbol{
                    name: symbol.name.clone(),
                    kind: self.types[self.vars[var]],
                }),
            }
        }

        // Done!
        Ok((captures, special_fields))
    }
}

pub(super) struct EvaluateRow<'a> {
    pub(super) queries: &'a BTreeMap<ast::Query, usize>,
    pub(super) vars: &'a HashMap<ast::Variable, usize>,
    pub(super) key: &'a Key,
    pub(super) row: &'a Vec<Option<ast::Value>>,
}
impl<'a> EvaluateRow<'a> {
    pub(super) fn evaluate_expr(&self, expr: &ast::Expr) -> Result<Option<ast::Value>, TypeError> {
        match expr {
            ast::Expr::BinaryOp(lhs, opcode, rhs) => evaluate_binary_operation(self.evaluate_expr(lhs)?, *opcode, self.evaluate_expr(rhs)?),
            ast::Expr::UnaryOp(opcode, operand) =>  evaluate_unary_operation(*opcode, self.evaluate_expr(operand)?),
            ast::Expr::If(i) => self.evaluate_if(i),
            ast::Expr::Function(_) => unimplemented!(),
            ast::Expr::Value(value) => Ok(Some(value.clone())),
            ast::Expr::Symbol(symbol) => match symbol.name {
                ast::SymbolName::Variable(ref var) => Ok(self.row[self.vars[var]].clone()),
                ast::SymbolName::Query(ref query) => {
                    if query.is_special() {
                        match query.field.as_str() {
                            "subjectkey" => Ok(Some(ast::Value::String(self.key.get_subjectkey()))),
                            "interview_date" => Ok(Some(ast::Value::Date(self.key.get_interview_date().clone()))),
                            "eventname" => Ok(Some(ast::Value::String(self.key.get_eventname().into()))),
                            _ => Ok(self.row[self.queries[query]].clone()),
                        }
                    }
                    else {
                        Ok(self.row[self.queries[query]].clone())
                    }
                }
            },
        }
    }

    fn evaluate_if(&self, i: &ast::If) -> Result<Option<ast::Value>, TypeError> {
        // Make sure predicate expression is boolean.
        if let Some(ref predicate) = i.predicate {
            let predicate = self.evaluate_expr(predicate)?;
            match predicate {
                Some(predicate) => match predicate {
                    // Make sure the predicate is boolean.
                    ast::Value::Bool(predicate) => {
                        // If the predicate is true, evaluate the statement's expression.
                        if predicate == true {
                            let val = self.evaluate_expr(&i.expression)?;
                            // Convert a NoneLiteral to None.
                            if let Some(val) = val {
                                if val.kind() == ast::Type::NoneLiteral {
                                    Ok(None)
                                }
                                else { Ok(Some(val)) }
                            }
                            else { Ok(val) }
                        }
                        // If the predicate is false, evaluate the alternative branch.
                        else {
                            if let Some(ref alternative) = i.alternative {
                                self.evaluate_if(alternative)
                            }
                            // If there is no alternative branch then evaluate to None.
                            else { Ok(None) }
                        }
                    },
                    // If the predicate is not boolean, generate an error.
                    ast::Value::U32(_) | ast::Value::I32(_) | ast::Value::F32(_) |
                    ast::Value::String(_) | ast::Value::Date(_) | ast::Value::NoneLiteral =>
                        Err(TypeError{
                            what: "If expression requires a boolean predicate.".into(),
                            expr: ast::ExprKind::If,
                            kinds: vec![],
                            hint: None,
                        }),
                },
                // If the predicate is None then evaluate to None.
                None => Ok(None),
            }
        }
        else {
            // No predicate -- this is the else branch of an if expression.
            // There is no alternative branch after this one, so we can evaluated the expression
            // unconditionally.
            let val = self.evaluate_expr(&i.expression)?;

            // Convert a NoneLiteral to None.
            if let Some(val) = val {
                if val.kind() == ast::Type::NoneLiteral {
                    Ok(None)
                }
                else { Ok(Some(val)) }
            }
            else { Ok(val) }
        }
    }
}

// Query a list of fields from a single file and update columns in a data table.
fn perform_queries(
    // Data table and number of columns to pre-allocate in each row.
    (data, cols): (&mut BTreeMap<Key, Vec<Option<ast::Value>>>, usize),
    // File in which to query fields.
    (data_dir, file): (&std::path::Path, &str),
    // Field names, field types, and corresponding columns in the data table.
    fields: &Vec<(&str, ast::Type, usize)>)
-> Result<(), InterpreterError> {
    // Get the filename of the file we are going to query.
    let filename = make_filename(data_dir, file);

    // Create a vector of fields with colum index in destination data table,
    // column index in source file, and type.
    let mut fields_vec: Vec<Field> = Vec::with_capacity(fields.len() + 2);

    // Take fields vector from function argument and convert it into a map of
    // field name to index in `fields_vec`.  Also keep track of whether the
    // field is encoutered more than once so that we can detect duplicates.
    let mut fields_map: HashMap<&str, (usize, bool)> = HashMap::with_capacity(fields.len() + 2);
    for field in fields {
        let index = fields_vec.len();
        fields_vec.push(Field{
            kind: field.1,
            dst_col: Some(field.2),
            src_col: None,
            name: field.0,
        });
        if fields_map.insert(field.0, (index, false)).is_some() {
            // Should never encounter duplicates, panic if we do.
            panic!("Duplicate field in argument to perform_queries().")
        }
    }

    // Add in special subjectkey, interview_date, and eventname fields.
    {
        let index = fields_vec.len();
        fields_vec.push(Field{kind: ast::Type::String, dst_col: None, src_col: None, name: "subjectkey"});
        if fields_map.insert("subjectkey", (index, false)).is_some() {
            // Should never encounter duplicates, panic if we do.
            panic!("Duplicate field in argument to perform_queries().")
        }
    }
    {
        let index = fields_vec.len();
        fields_vec.push(Field{kind: ast::Type::Date, dst_col: None, src_col: None, name: "interview_date"});
        if fields_map.insert("interview_date", (index, false)).is_some() {
            // Should never encounter duplicates, panic if we do.
            panic!("Duplicate field in argument to perform_queries().")
        }
    }
    {
        let index = fields_vec.len();
        fields_vec.push(Field{kind: ast::Type::String, dst_col: None, src_col: None, name: "eventname"});
        if fields_map.insert("eventname", (index, false)).is_some() {
            // Should never encounter duplicates, panic if we do.
            panic!("Duplicate field in argument to perform_queries().")
        }
    }

    // Read the file line by line.
    let mut lines = std::io::BufReader::new(std::fs::File::open(&filename)?).lines();
    // Examine the first line to figure out what columns our queries are in
    // within the source file.  Note that queries will later be stored in a
    // *different* column within the destination data table.
    let col_names = lines.nth(0);
    if col_names.is_none() {
        return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, filename.display().to_string()).into())
    }
    let col_names = col_names.unwrap()?;
    let col_names = col_names.split('\t');
    for (index, name) in col_names.enumerate() {
        // Strip first and last charceter (quotes) from column name.
        let name = ast::strip(name);

        // Figure out source column for fields of interest.
        if let std::collections::hash_map::Entry::Occupied(mut entry) = fields_map.entry(name) {
            // Get a mutable reference to the value of the entry.
            let mut value = &mut entry.get_mut();

            // Detect duplicates.
            if value.1 {
                return Err(QueryError{
                    what: format!("File contains duplicate columns: {}", filename.display()),
                    file: Some(file.into()),
                    field: Some(name.into()),
                    hint: None,
                }.into());
            }
            value.1 = true;

            // Update the source column.
            let mut field = &mut fields_vec[value.0];
            field.src_col = Some(index);
        }
    }

    // At this point we no longer need the fields map.
    std::mem::drop(fields_map);

    // Sort by source column.  Order any missing column indices first so that
    // we can detect them easily.
    fields_vec.sort_unstable_by(|a, b| {
        match a.src_col {
            Some(i) => match b.src_col {
                Some(j) => i.cmp(&j),
                None => std::cmp::Ordering::Greater,
            }
            None => match b.src_col {
                Some(_) => std::cmp::Ordering::Less,
                None => std::cmp::Ordering::Equal,
            }
        }
    });

    // Check the first field to make sure we are not missing any source columns.
    if fields_vec[0].src_col.is_none() {
        return Err(QueryError{
            what: format!("Missing field from table: {}", filename.display()),
            file: Some(file.into()),
            field: Some(fields_vec[0].name.into()),
            hint: None,
        }.into());
    }

    // Figure out again which special fields contain the subjectkey,
    // interview_date, and eventname.
    let subjectkey_idx = match fields_vec.iter().position(|field| field.dst_col.is_none() && field.kind == ast::Type::String && field.name == "subjectkey") {
        Some(idx) => idx,
        None => return Err(QueryError{
            what: format!("Missing field subjectkey from table: {}", filename.display()),
            file: Some(file.into()),
            field: Some("subjectkey".into()),
            hint: None,
        }.into()),
    };
    let interview_date_idx = match fields_vec.iter().position(|field| field.dst_col.is_none() && field.kind == ast::Type::Date) {
        Some(idx) => idx,
        None => return Err(QueryError{
            what: format!("Missing field interview_date from table: {}", filename.display()),
            file: Some(file.into()),
            field: Some("interview_date".into()),
            hint: None,
        }.into()),
    };
    let eventname_idx = match fields_vec.iter().position(|field| field.dst_col.is_none() && field.kind == ast::Type::String && field.name == "eventname") {
        Some(idx) => idx,
        None => return Err(QueryError{
            what: format!("Missing field eventname from table: {}", filename.display()),
            file: Some(file.into()),
            field: Some("eventname".into()),
            hint: None,
        }.into()),
    };

    // Skip the next line in the file, which contains the column descriptions
    // but does not contain any data.
    let lines = lines.skip(1);

    // Iterate over remaining lines and parse the fields of interest out of
    // their respective columns and into our data table.
    for line in lines {
        // Split on tabs.
        let line = line?;
        let mut line = line.split('\t');

        // Accumulate values into a temporary vector.
        let mut values: Vec<Option<ast::Value>> = Vec::with_capacity(fields_vec.len());

        // Iterate over fields/columns.
        let mut skip_col: usize = 0;
        for field in &fields_vec {
            // Extract the next field/column of interest.
            let src_col = field.src_col.unwrap();
            let src = line.nth(src_col - skip_col);
            skip_col = src_col + 1;
            if src.is_none() {
                return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, filename.display().to_string()).into())
            }

            // Strip first and last characeter (quotes) and parse.
            let src = ast::strip(src.unwrap());
            let value = match parse(src, field.kind) {
                Ok(value) => Ok(value),
                Err(_) => Err(QueryError{
                    what: format!("Couldn't parse \"{}\" as {} in file: {}", src, field.kind, filename.display().to_string()),
                    file: Some(file.into()),
                    field: Some(field.name.into()),
                    hint: None,
                }),
            }?;
            values.push(value);
        }

        // Generate row key for data table.
        let subjectkey = match values[subjectkey_idx] {
            Some(ast::Value::String(ref subjectkey)) => subjectkey.clone(),
            _ => return Err(QueryError{
                what: format!("Row is missing subjectkey in file: {}", filename.display()),
                file: Some(file.into()),
                field: Some("subjectkey".into()),
                hint: None,
            }.into()),
        };
        let interview_date = match values[interview_date_idx] {
            Some(ast::Value::Date(interview_date)) => interview_date,
            _ => return Err(QueryError{
                what: format!("Row is missing interview_date in file: {}", filename.display()),
                file: Some(file.into()),
                field: Some("interview_date".into()),
                hint: None,
            }.into()),
        };
        let eventname = match &values[eventname_idx] {
            Some(ast::Value::String(eventname)) => eventname.into(),
            _ => return Err(QueryError{
                what: format!("Row is missing eventname in file: {}", filename.display()),
                file: Some(file.into()),
                field: Some("eventname".into()),
                hint: None,
            }.into()),
        };
        let key = Key::new(subjectkey, interview_date, eventname);

        // Update this row in the data table.
        match data.entry(key) {
            std::collections::btree_map::Entry::Vacant(entry) => {
                // Insert a row with the correct number of columns
                // and initialize columns to None.
                let row = entry.insert(Vec::with_capacity(cols));
                row.resize(cols, None);

                // Update columns from this query.
                // Since we just created the row, we don't need to worry about
                // the possibility of duplicates.
                for (index, value) in values.into_iter().enumerate() {
                    if let Some(col) = fields_vec[index].dst_col {
                        row[col] = value;
                    }
                }
            }
            std::collections::btree_map::Entry::Occupied(mut entry) => {
                // If the row already exists then check to make sure it is not
                // a duplicate by making sure each column from this query
                // contains None before updating it with the actual data.
                let row = entry.get_mut();
                for (index, value) in values.into_iter().enumerate() {
                    if let Some(col) = fields_vec[index].dst_col {
                        if row[col].is_none() {
                            row[col] = value;
                        }
                        else {
                            let key = entry.key();
                            return Err(QueryError{
                                what: format!("Duplicate row detected with {} in file: {}", key, filename.display()),
                                file: Some(file.into()),
                                field: None,
                                hint: None,
                            }.into());
                        }
                    }
                }
            },
        }
    }
    Ok(())
}

// Structure to keep track of fields in `perform_queries()`.
struct Field<'a> {
    // Type.
    kind: ast::Type,
    // Column index in data table.
    dst_col: Option<usize>,
    // Column index in source file.
    src_col: Option<usize>,
    // Field name.
    name: &'a str,
}

// Parse the string representation of a field from the source file into a value.
fn parse(src: &str, kind: ast::Type) -> Result<Option<ast::Value>, ast::ParseError> {
    match kind {
        ast::Type::Bool => {
            // Since the ABCD data got a little creative with their boolean
            // responses, we will allow more than just "true" and "false" here.
            if src.is_empty() { Ok(None) }
            else if
                src.eq_ignore_ascii_case("true") ||
                src.eq_ignore_ascii_case("T") ||
                src.eq_ignore_ascii_case("Y") ||
                src == "1"
            {
                Ok(Some(ast::Value::Bool(true)))
            }
            else if
                src.eq_ignore_ascii_case("false") ||
                src.eq_ignore_ascii_case("F") ||
                src.eq_ignore_ascii_case("N") ||
                src == "0"
            {
                Ok(Some(ast::Value::Bool(false)))
            }
            else
            {
                Err(ast::ParseError::ParseBoolError(src.into()))
            }
        },
        ast::Type::U32 => {
            if src.is_empty() { Ok(None) }
            else if src == "nan" { Ok(None) }
            else { Ok(Some(ast::Value::U32(u32::from_str(src)?))) }
        },
        ast::Type::I32 => {
            if src.is_empty() { Ok(None) }
            else if src == "nan" { Ok(None) }
            else { Ok(Some(ast::Value::I32(i32::from_str(src)?))) }
        },
        ast::Type::F32 => {
            if src.is_empty() { Ok(None) }
            else if src == "nan" { Ok(None) }
            else { Ok(Some(ast::Value::F32(f32::from_str(src)?))) }
        },
        ast::Type::String => {
            if src.is_empty() { Ok(None) }
            else { Ok(Some(ast::Value::String(src.into()))) }
        },
        ast::Type::Date => {
            if src.is_empty() { Ok(None) }
            else { Ok(Some(ast::Value::Date(chrono::NaiveDate::parse_from_str(src, "%m/%d/%Y")?))) }
        },
        // Can't really parse NoneLiteral
        ast::Type::NoneLiteral => Ok(None),
    }
}

// Evaluate a binary operation.
fn evaluate_binary_operation(lhs: Option<ast::Value>, opcode: ast::Opcode, rhs: Option<ast::Value>) -> Result<Option<ast::Value>, TypeError> {
    match opcode {
        ast::Opcode::AND => match (&lhs, &rhs) {
            (None, None) =>
                Ok(None),
            // If one operand is missing but the other is false then false.
            // If one operand is missing but the other is true then can't
            // determine, so None.
            (None, Some(ast::Value::Bool(rhs))) => match rhs {
                true => Ok(None),
                false => Ok(Some(ast::Value::Bool(false))),
            },
            (Some(ast::Value::Bool(lhs)), None) => match lhs {
                true => Ok(None),
                false => Ok(Some(ast::Value::Bool(false))),
            },
            (Some(ast::Value::Bool(lhs)), Some(ast::Value::Bool(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs && *rhs))),
            (Some(ast::Value::U32(_)), None) | (None, Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), None) | (None, Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), None) | (None, Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), None) | (None, Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), None) | (None, Some(ast::Value::Date(_))) |
            (Some(ast::Value::NoneLiteral), None) | (None, Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::U32(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::Date(_))) |
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::U32(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "types are not boolean".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        // identical to AND but different operation
        ast::Opcode::OR => match (&lhs, &rhs) {
            (None, None) =>
                Ok(None),
            // If one operand is missing but the other is true then true.
            // If one operand is missing but the other is false then can't
            // determine, so None.
            (None, Some(ast::Value::Bool(rhs))) => match rhs {
                true => Ok(Some(ast::Value::Bool(true))),
                false => Ok(None),
            },
            (Some(ast::Value::Bool(lhs)), None) => match lhs {
                true => Ok(Some(ast::Value::Bool(true))),
                false => Ok(None),
            },
            (Some(ast::Value::Bool(lhs)), Some(ast::Value::Bool(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs || *rhs))),
            (Some(ast::Value::U32(_)), None) | (None, Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), None) | (None, Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), None) | (None, Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), None) | (None, Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), None) | (None, Some(ast::Value::Date(_))) |
            (Some(ast::Value::NoneLiteral), None) | (None, Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::U32(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::Date(_))) |
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::U32(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "types are not boolean".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        ast::Opcode::EQ => match (&lhs, &rhs) {
            (None, None) |
            (None, Some(ast::Value::Bool(_))) | (Some(ast::Value::Bool(_)), None) |
            (None, Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), None) |
            (None, Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), None) |
            (None, Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), None) |
            (None, Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), None) |
            (None, Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), None) =>
                Ok(None),
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (None, Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), None) =>
                Ok(Some(ast::Value::Bool(true))),
            (Some(ast::Value::Bool(lhs)), Some(ast::Value::Bool(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs == rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs == rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs == rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs == rhs))),
            (Some(ast::Value::String(lhs)), Some(ast::Value::String(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs == rhs))),
            (Some(ast::Value::Date(lhs)), Some(ast::Value::Date(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs == rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as i32 == *rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs == *rhs as i32))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as f32 == *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs == *rhs as f32))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as f32 == *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs == *rhs as f32))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) =>
                Ok(Some(ast::Value::Bool(false))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "incompatible types for comparison".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        // identical to EQ but different operator
        ast::Opcode::NE => match (&lhs, &rhs) {
            (None, None) |
            (None, Some(ast::Value::Bool(_))) | (Some(ast::Value::Bool(_)), None) |
            (None, Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), None) |
            (None, Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), None) |
            (None, Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), None) |
            (None, Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), None) |
            (None, Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), None) =>
                Ok(None),
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (None, Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), None) =>
                Ok(Some(ast::Value::Bool(false))),
            (Some(ast::Value::Bool(lhs)), Some(ast::Value::Bool(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs != rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs != rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs != rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs != rhs))),
            (Some(ast::Value::String(lhs)), Some(ast::Value::String(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs != rhs))),
            (Some(ast::Value::Date(lhs)), Some(ast::Value::Date(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs != rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as i32 != *rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs != *rhs as i32))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as f32 != *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs != *rhs as f32))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as f32 != *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs != *rhs as f32))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) =>
                Ok(Some(ast::Value::Bool(true))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "incompatible types for comparison".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        ast::Opcode::GT => match (&lhs, &rhs) {
            (None, None) |
            (None, Some(_)) | (Some(_), None) =>
                Ok(None),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs > rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs > rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs > rhs))),
            (Some(ast::Value::String(lhs)), Some(ast::Value::String(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs > rhs))),
            (Some(ast::Value::Date(lhs)), Some(ast::Value::Date(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs > rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as i32 > *rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs > *rhs as i32))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as f32 > *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs > *rhs as f32))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as f32 > *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs > *rhs as f32))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "incompatible types for ordering".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        // identical to GT but different operator
        ast::Opcode::LT => match (&lhs, &rhs) {
            (None, None) |
            (None, Some(_)) | (Some(_), None) =>
                Ok(None),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs < rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs < rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs < rhs))),
            (Some(ast::Value::String(lhs)), Some(ast::Value::String(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs < rhs))),
            (Some(ast::Value::Date(lhs)), Some(ast::Value::Date(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs < rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool((*lhs as i32) < *rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs < *rhs as i32))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool((*lhs as f32) < *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs < *rhs as f32))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool((*lhs as f32) < *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs < *rhs as f32))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "incompatible types for ordering".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        // identical to GT but different operator
        ast::Opcode::GE => match (&lhs, &rhs) {
            (None, None) |
            (None, Some(_)) | (Some(_), None) =>
                Ok(None),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs >= rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs >= rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs >= rhs))),
            (Some(ast::Value::String(lhs)), Some(ast::Value::String(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs >= rhs))),
            (Some(ast::Value::Date(lhs)), Some(ast::Value::Date(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs >= rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as i32 >= *rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs >= *rhs as i32))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as f32 >= *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs >= *rhs as f32))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs as f32 >= *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs >= *rhs as f32))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "incompatible types for ordering".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        // identical to GT but different operator
        ast::Opcode::LE => match (&lhs, &rhs) {
            (None, None) |
            (None, Some(_)) | (Some(_), None) =>
                Ok(None),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs <= rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs <= rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs <= rhs))),
            (Some(ast::Value::String(lhs)), Some(ast::Value::String(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs <= rhs))),
            (Some(ast::Value::Date(lhs)), Some(ast::Value::Date(rhs))) =>
                Ok(Some(ast::Value::Bool(lhs <= rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool((*lhs as i32) <= *rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs <= *rhs as i32))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool((*lhs as f32) <= *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs <= *rhs as f32))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::Bool((*lhs as f32) <= *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::Bool(*lhs <= *rhs as f32))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "incompatible types for ordering".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        ast::Opcode::PLUS => match (&lhs, &rhs) {
            (None, None) |
            (None, Some(_)) | (Some(_), None) =>
                Ok(None),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::U32(lhs + rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::I32(lhs + rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(lhs + rhs))),
            (Some(ast::Value::String(lhs)), Some(ast::Value::String(rhs))) =>
                Ok(Some(ast::Value::String(lhs.to_owned() + rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::I32(*lhs as i32 + *rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::I32(*lhs + *rhs as i32))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs as f32 + *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs + *rhs as f32))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs as f32 + *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs + *rhs as f32))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::Date(_))) |
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "incompatible types for addition".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        ast::Opcode::MINUS => match (&lhs, &rhs) {
            (None, None) |
            (None, Some(_)) | (Some(_), None) =>
                Ok(None),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::U32(lhs - rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::I32(lhs - rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(lhs - rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::I32(*lhs as i32 - *rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::I32(*lhs - *rhs as i32))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs as f32 - *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs - *rhs as f32))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs as f32 - *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs - *rhs as f32))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::Date(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::String(_))) |
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "incompatible types for subtraction".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        // same as MINUS but different operator
        ast::Opcode::MUL => match (&lhs, &rhs) {
            (None, None) |
            (None, Some(_)) | (Some(_), None) =>
                Ok(None),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::U32(lhs * rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::I32(lhs * rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(lhs * rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::I32(*lhs as i32 * *rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::I32(*lhs * (*rhs as i32)))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs as f32 * (*rhs)))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs * (*rhs as f32)))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs as f32 * (*rhs)))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs * (*rhs as f32)))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::Date(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::String(_))) |
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "incompatible types for multiplication".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        // same as MINUS but different operator
        ast::Opcode::DIV => match (&lhs, &rhs) {
            (None, None) |
            (None, Some(_)) | (Some(_), None) =>
                Ok(None),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::U32(lhs / rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::I32(lhs / rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(lhs / rhs))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::I32(*lhs as i32 / *rhs))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::I32(*lhs / *rhs as i32))),
            (Some(ast::Value::U32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs as f32 / *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::U32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs / *rhs as f32))),
            (Some(ast::Value::I32(lhs)), Some(ast::Value::F32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs as f32 / *rhs))),
            (Some(ast::Value::F32(lhs)), Some(ast::Value::I32(rhs))) =>
                Ok(Some(ast::Value::F32(*lhs / *rhs as f32))),
            (Some(ast::Value::Bool(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::Date(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::String(_))) |
            (Some(ast::Value::NoneLiteral), Some(ast::Value::NoneLiteral)) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::String(_))) |
            (Some(ast::Value::Date(_)), Some(ast::Value::NoneLiteral)) | (Some(ast::Value::NoneLiteral), Some(ast::Value::Date(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::U32(_))) | (Some(ast::Value::U32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::I32(_))) | (Some(ast::Value::I32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::F32(_))) | (Some(ast::Value::F32(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::Bool(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::Bool(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::U32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::U32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::I32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::I32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::String(_))) | (Some(ast::Value::String(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::F32(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::F32(_))) |
            (Some(ast::Value::String(_)), Some(ast::Value::Date(_))) | (Some(ast::Value::Date(_)), Some(ast::Value::String(_))) =>
                Err(TypeError{what: "incompatible types for division".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs.unwrap().kind(), rhs.unwrap().kind()], hint: None}),
        },
        ast::Opcode::NOT => panic!("Called evaluate_binary_operation() with unary operator NOT \"!\"."),
    }
}

// Evaluate a unary operation.
fn evaluate_unary_operation(opcode: ast::Opcode, operand: Option<ast::Value>) -> Result<Option<ast::Value>, TypeError> {
    match opcode {
        ast::Opcode::NOT => match operand {
            Some(operand) => match operand {
                ast::Value::Bool(b) =>
                    Ok(Some(ast::Value::Bool(!b))),
                ast::Value::U32(_) |
                ast::Value::I32(_) |
                ast::Value::F32(_) |
                ast::Value::String(_) |
                ast::Value::Date(_) |
                ast::Value::NoneLiteral =>
                    Err(TypeError{what: "type is not boolean".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![operand.kind()], hint: None}),
            },
            None => Ok(None),
        },
        ast::Opcode::AND |ast::Opcode::OR | ast::Opcode::EQ | ast::Opcode::NE |
        ast::Opcode::LT | ast::Opcode::GT | ast::Opcode::LE | ast::Opcode::GE |
        ast::Opcode::PLUS | ast::Opcode::MINUS |
        ast::Opcode::MUL | ast::Opcode::DIV
            => panic!("Called evaluate_unary_operation with binary operator {}.", opcode),
    }
}
