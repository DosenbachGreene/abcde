//! Generic error types.  Used by the `main.rs`
//! et al for error propagation.

/// Type-erased BoxError type, see https://github.com/rust-lang/rfcs/pull/2820
pub type BoxError = std::boxed::Box<dyn
	std::error::Error   // must implement Error to satisfy ?
	+ std::marker::Send // needed for threads
	+ std::marker::Sync // needed for threads
>;

/// Generic error type implementing `std::error::Error`.
#[derive(Debug)]
pub struct Error {
    /// What went wrong?
	pub what: String,
	/// What was the source/cause of this error, if any?
	pub source: Option<BoxError>
}

// implement Display trait
impl std::fmt::Display for Error {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.what)?;
		if let Some(error) = &self.source {
			write!(f, "\nCaused by: {}", error)?;
		}
		Ok(())
	}
}

// implement Error trait
impl std::error::Error for Error {
	fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
		match &self.source {
			Some(error) => Some(error.as_ref()),
			None => None
		}
	}
}
