//! Module for domain-specific language.

// Export abstract ayntax tree as sub-module.
pub mod ast;

// Parser module
use lalrpop_util::lalrpop_mod;
lalrpop_mod!(pub parser);
pub use parser::CodeParser;

// Re-export interpreter object.
mod interpreter;
pub use interpreter::ExpandedCode;
pub use interpreter::ValidCode;
pub use interpreter::Interpreter;
pub use interpreter::OutputTable;
