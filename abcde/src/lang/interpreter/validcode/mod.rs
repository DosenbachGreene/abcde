use super::*;

/// Structure storing code consisting of an abstract syntax tree (AST) and type
/// information.  Typically used as an intermediate step between parsing the
/// the code into an AST and using the code in an `Interpreter` object.  If a
/// `ValidCode` object is created successfully with `new()` it implies that the
/// code is valid and "compiles" without errors. 
pub struct ValidCode {
    /// The user's code, represented as an abstract syntax tree (AST).
    pub(super) code: ast::Code,
    
    /// The number of select statements in the code.
    pub(super) select_count: usize,
    
    /// An ordered map from query name to a vector index in the types vector
    /// and values.  Ordered so that we can easily group queries by file.
    pub(super) queries: BTreeMap<ast::Query, usize>,
    
    /// An unordered map from variable name to a vector index in the types
    /// vector and values.  The index is the same as for `queries`, i.e. an
    /// index number that appears in `queries` cannot appear here and vice
    /// versa.
    pub(super) vars: HashMap<ast::Variable, usize>,
    
    /// A vector corresponding to the types of the symbols.
    pub(super) types: Vec<Option<ast::Type>>,
}

// Implementation of public methods.
impl ValidCode {
    /// Take an abstract syntax tree (AST) and validate it, initializing
    /// internal type information in the process.  If called with an `str` will
    /// automatically parse the code into an AST before validating it.
    pub fn new<T>(code: T) -> Result<ValidCode, InterpreterError>
    where T: IntoExpandedCode {
        // Initialize state.
        let mut valid_code = ValidCode {
            code: code.into_expanded_code()?.code,
            select_count: 0,
            queries: BTreeMap::new(),
            vars: HashMap::new(),
            types: Vec::new(),
        };
        
        // Perform validation.
        valid_code.validate()?;
        
        // Return validated code if successful.
        Ok(valid_code)
    }
    
    /// Inspect the abstract syntax tree (AST).
    pub fn code(&self) -> &ast::Code {
        &self.code
    }
    
    /// Get the number of select statements.
    pub fn select_count(&self) -> usize {
        self.select_count
    }
    
    /// Get an iterator over all queries in the AST.
    /// Useful for inspecting their types.
    pub fn queries(&self) -> QueryIterator {
        QueryIterator {
            inner: self.queries.iter(),
            types: &self.types,
        }
    }
}

/// Trait implemented by objects that we can
/// attempt to converted into valid code.
pub trait IntoValidCode {
    /// Convert argument into ValidCode struct.
    fn into_valid_code(self) -> Result<ValidCode, InterpreterError>;
}
impl IntoValidCode for ValidCode {
    fn into_valid_code(self) -> Result<ValidCode, InterpreterError> {
        Ok(self)
    }
}
impl<T: ast::IntoCode> IntoValidCode for T {
    fn into_valid_code(self) -> Result<ValidCode, InterpreterError> {
        ValidCode::new(self)
    }
}

// Split off private implementation details into a separate file.
mod details;
