# ABCDE Tutorial

This tutorial provides a gentle introduction to exploring and capturing data with the `abcde` tool.  We will begin with some simple examples on a small tutorial data set.  These examples will teach you how to use abcde's simple query language.  We will then work some examples exploring common use cases and idiosyncrasies of the actual ABCD data.

Note that abcde itself does not perform any statistics on the data.  Its purpose is to help you find data with the explore subcommand and then generate custom tables with the capture subcommand.  You may then import these tables into R, Matlab, Excel, or some other statistical program.

Also please note that the abcde tool is minimally maintained.  If you think you have found an error, please direct comments, suggestions, and complaints to the primary author [Benjamin Kay](mailto:benjamin@benkay.net).

## Prerequisites

Please see the [main README.md](../README.md) for instructions on how to compile the abcde binary and download the data.  The first part of this tutorial uses example data located in this [tutorial](./) folder, but the latter half requires the actual ABCD data.

## Motivating Example

Let's hypothesize that consumption of high-calorie foods like ice cream is correlated with obesity whereas consumption of low-calorie foods like salad is not.  Inside this [tutorial](./) folder is a small example dataset formatted in the same way as the ABCD data.  The data set contains information about the weight and food preferences of three fictional study subjects.

## Explore

### <a name="structure-of-the-abcd-data"></a>Structure of the ABCD Data

The ABCD database is comprised of a single directory containing multiple text files.  Each text file is a table with the same name as the file, minus the `.txt` extension.  A table contains columns, or fields, and rows, or observations.  Typically each table has a theme.  For example, the `abcd_ant01` table contains anatomical data about each subject.  Its fields include height, weight, etc.

![Structure of Database](./structure.svg)

The combination of a table name, followed by a dot `.`, followed by the name of a field is a _query_.  For example, `abcd_ant01.anthroheightcalc` is a query referring to a subject's height in inches.

The separate tables in the database are linked together by _key_ fields.  In ABCD, the key field `subjectkey` is a subject's unique identifier, e.g. `NDAR_INVABCD1234`.  So, for example, a row in the `abcd_ant01` table for with the `subjectkey` `NDAR_INVABCD1234` refers to the same subject as a row in the `abcd_cbcl01` table (Child Behavioral Checklist questionnaire) with the same `subjectkey`.

### Format of a Table

Let's explore the tutorial database.  Begin by opening the file [vitals.txt](./vitals.txt) in a text editor like gedit, kwrite, or Windows notepad.  This is the `vitals` table.  The first few lines look something like this:

```
"subjectkey" "interview_date" "eventname" "gender" "height" "weight" "eye.color"
"unique id" "date of interview" "longitudinal event" "sex of the subject" "height" "weight" "eye color"
"NDAR_INVABCD1234" "12/25/2018" "baseline" "M" "75" "190" "brown"
```

The table is divided into rows and columns.  Each column is a _field_ and is delimited by quotes.

1. The first line is all the _fields_ in the file.
2. The second line is brief descriptions of the fields.
3. The third line is the first row in the table, an observation of the value of each field.
4. Each subsequent line is a new row.

Recall from the previous section that we can address the combination of table + field using a _query_.  Just take the table name (file name name sans `.txt`), add a dot `.`, and then add the field name.  For example, `vitals.weight`.

### Listing Fields in a Table

The tutorial data set is small; each table contains just a few fields and observations.  In the actual ABCD data each table can be megabytes in size, containing hundreds of fields and tens of thousands of observations.  Working with such large data files in a text editor can become unwieldy.  The abcde tool is here to help!

Instead of using a text editor, let's try exploring [vitals.txt](./vitals.txt) using the `explore` subcommand.  We'll learn more about subcommands in the next section.  Here is an example of what the output should look like (abbreviated for length):

```
abcde --data-dir tutorial explore --list --table vitals
```
```
Query: vitals.subjectkey
Description:
unique id

Query: vitals.interview_date
Description:
date of interview

Query: vitals.eventname
Description:
longitudinal event

Query: vitals.gender
Description:
Sex of the subject

...
```

> Tip: The output of `abcde` can be scrolled using `abcde ... | less`, or saved to a text file for later viewing with `abcde ... > file.txt`.

### Pointing to the Data

In the example above we used the option `--data-dir tutorial` to tell abcde to look for the ABCD database in the `tutorial` directory.  We could also have used `-d tutorial` for brevity.  We will learn more about the command line options in the next section.

To save typing, you can also set the `ABCDE_DATA_DIR` environment variable to point to your data directory (`/data/Daenerys/ABCD/data/behavioral_data/3.0` for WUSTL NIL users).  Then you can skip the `--data-dir` option entirely.

* Bash shell (most Linux users):
  
  ```
  echo 'export ABCDE_DATA_DIR=/data/Daenerys/ABCD/data/behavioral_data/3.0' >> ~/.profile
  source ~/.profile
  ```
  
* C shell (WUSTL NIL users):
  
  ```
  echo 'setenv ABCDE_DATA_DIR /data/Daenerys/ABCD/data/behavioral_data/3.0' >> ~/.lin.cshrc
  source ~/.lin.cshrc
  ```

### Using Help

In the previous section we called abcde and passed a series of arguments to it. ABCDE accepts two types of arguments: options (or flags) and subcommands.  You can find out more about them by running abcde without any arguments or using the help subcommand (output abbreviated for length):

```
abcde
```
```
...

USAGE:
    abcde [FLAGS] --data-dir <data-dir> <SUBCOMMAND>

...

OPTIONS:
    -d, --data-dir <data-dir>    Where the ABCD data is located

SUBCOMMANDS:
    capture     Generate a custom table from a subset of the data
    download    Download the data from NDA, the NIMH Data Archive
                Size is ~ 7 GB
    explore     Search for available fields/columns in the data
    help        Prints this message or the help of the given subcommand(s)
    
...
```

Each subcommand performs a different function.  As hinted at in the help message above, the explore subcommand used in the previous section allows us to search through and explore available queries.  In the following sections we will also learn about the capture subcommand.

Use the help subcommand to get more help about a subcommand.  For example:

```
abcde help explore
```
```
abcde-explore 0.1.0
Search for available fields/columns in the data

USAGE:
    abcde explore [OPTIONS] <--list|--search <regex>|--info <query>>

FLAGS:
    -h, --help       Prints help information
        --list       List all the fields of a file/table
    -V, --version    Prints version information

OPTIONS:
        --info <query>      Display details about a query
                            Example: abcd_mri01.mri_info_studydate
        --search <regex>    Search for fields matching a regular expression
        --table <table>     Limit search to a single table
                            Example: abcd_mri01
```

In the previous section we combined the `--list` and `--table` arguments to list the fields inside the `vitals` table.  Note that although the actual data is contained in a file `vitals.txt`, when forming queries like `vitals.weight` or passing an option like `--table vitals` we omit the `.txt` file extension.  Let's try out some of the other options in the explore subcommand!

### Getting Information About a Query

Things are going well.  It's almost time to submit our manuscript on how ice cream causes obesity to _Nature_.  Let's get a little more information about a query central to our hypothesis: weight. 

```
abcde -d tutorial explore --info vitals.weight
```
```
Query: vitals.weight
Description:
weight
Dictionary:
weight
Notes:
measured in pounds
Sample values:
190
130
170
131
180
```

Here we used the `--info` argument to get information about a query.  Since the name of the query `vitals.weight` includes both its table `vitals` and field `gender`, it is not necessary to specify the `--table` argument.

Looking at the output of `--info` we have more details than when we `--list`ed the whole table.  We see the query name and description as before.  We also get additional information from a data dictionary, which is located in [tutorial/dictionary/vitals.txt](./dictionary/vitals.txt) for the curious.  Sometimes the dictionary contains helpful information not included in the description -- in this case, the units in which weight was measured.

Some of the extra dictionary fields you can retrieve with info are as follows.  If a field is not present in the dictionary then it will not be displayed. 

* _Description_ is derived from the table and is the same as when using `--list`.
* _Dictionary_ is the description derived from the dictionary and is usually the same as the description from the table.
* _Notes_ contains additional notes about the query, often the meanings of the different possible values.
* _Value range_ indicates the possible values a query can have.
* _Aliases_ is a list of other fields that contain the same data.

Finally, we also get sample values from the first 10 observations in the table, each on a new line.  (There are only 5 observations in the tutorial data.)  As expected, se we see some numbers consistent with the weight of an adult human in pounds.

### Searching for a Query

With thousands of queries available in hundreds of tables, we need a way to search for them.  Fortunately this is easy with the `--search` argument.  Let's search for queries on ice cream:

```
abcde -d tutorial explore --search '(?i)ice.?cream'
```
```
Query: prefs.ice_cream
Description:
likes ice cream
```

The `--search` argument takes a regular expression, or _regex_, a special kind of search parameter.  You can read more about how to compose a regex in the [regex crate documentation](https://docs.rs/regex), in the [RexEgg tutorial](https://www.rexegg.com/regex-quickstart.html), or by searching the Internet for help on how to use this standard tool.  It generates a list of queries whose names or descriptions match the regex.  In the example above the regex is enclosed in single quotes `'regex'` so that it is not interpreted by the Linux shell.

The `(?i)` at the beginning makes the regex case **i**nsensitive, which is to say that it would match `ice`, `ICE`, `iCe`, etc. without regard to capitalization.  Try running with `--search 'ICE'` to see what happens -- you won't get any results because the `ice` in the data is lowercase.  Then try with `--search '(?i)ICE'` -- you will get results again because the `(?i)` makes the regex ignore capitaliation.

The `.?` means match zero or one of any character.  The `.` means any character and the `?` means match zero or one of.  This allows us to match `icecream`, `ice_cream`, `ice cream`, etc.

> Tip: If a search gives you too much output try revising the regular explression.  You can also limit the search to a single table by combining it with the `--table` argument.

Applying our knowledge from the previous sections, we can now get more information about this newly-discovered query using `--info prefs.ice_cream` or list other fields in the same table with `--list --table prefs`.

## Capture

The data can contain thousands of queries in hundreds of tables, but typically we are interested in just a few queries.  In the section above we learned how to use the `abcde explore` subcommand to search for queries of interest and get more information about them.  In this section we will learn to use the `capture` subcommand to generate custom tables containing just the queries we are interested in, in a format suitable for importing into our favorite statistical software.  To see all of the available options  use the help:

```
abcde help capture
```

### Key Fields

Before we write our first capture, let's revisit the concept of [key fields](#structure-of-the-abcd-data).  Key fields are present in all tables.  A combination of key fields ***uniquely*** identifies an observation, allowing us to link observations across tables.  Since these key fields are present in every table, we query them by omitting the table name and just using the preceding dot, e.g. `.key_field`.  The two key fields in the ABCD data are `.subjectkey` and `.interview_date`.  Due to a mistake in the ABCD 3.0 release, you may also need to use a third key field, `.eventname`, to disambiguate longitudinal data.

### My First Capture

Recall we are studying a hypothesized relationship between high-calorie foods like ice cream and obesity.  Let's start out by generating a table of weight and preference for ice cream.  Recall that `-d tutorial` tells `abcde` to look for the data in the `tutorial` directory.

```
abcde -d tutorial capture --code \
'select vitals.weight as f32, prefs.ice_cream as bool where true;'
```
```
".subjectkey" ".interview_date" "vitals.weight" "prefs.ice_cream"
"NDAR_INVABCD1234" "12/25/2018" 190 true
"NDAR_INVABCD1234" "11/2/2019" 195 true
"NDAR_INVEFGH5678" "7/4/2018" 130 false
...
```

> Tip: See `abcde help capture` for arguments that control how the output is formatted.  You can use `--fmt-matab` to generate output formatted in a way that is comprehensible to Matlab's `readtable` function.

Captures are written in a domain-specific capture language loosely resembling [SQL](https://en.wikipedia.org/wiki/SQL).  A collection of valid statements written in this language is referred to as _code_.  In the example above, the code is passed in using the `--code` argument and enclosed in single quotes `'` so that it will not be interpreted by the shell.

A select statement starts with the keyword `select` and ends with a semicolon `;`.  It has the syntax `select symbols where predicate;`.  The symbols are a comma-separated list of _symbols_, which are _queries_ (familiar from the previous sections) or _variables_ (which will be discussed in the coming sections).  The predicate must be a _boolean_ (i.e. evaluates to true or false) expression.  The select statement will generate a table of observations containing the symbols we request for observations for which the predicate is true.

In this example we select two queries, one for weight and one for whether or not the subject likes ice cream.  Our predicate expression is simply the literal value `true`, which will include all observations in the output table.

The output table contains a column for each symbol in the select statement.  The special fields `.subjectkey` and `.interview_date` are also included automatically.  The first line of the table is a header with the name of each column.  Each subsequent line, or row, is an observation that satisfies the predicate.  Again, in this example the predicate is literally `true` for all observations, therefore all observations are included in the output table.  We will see how to design a more exclusive predicate in the coming sections.

### Specifying Type

In `abcde` values and the symbols that represent them have _type_.  Currently supported types include:

* `bool` a boolean, i.e. true or false
* `i32` signed 32-bit integer, i.e. number with nothing after the decimal point, e.g. -3, 0, 10
* `u32` unsigned 32-bit integer, i.e. zero or a positive integer, e.g. 0, 1, 2
* `f32` single-precision float, i.e. number that can have decimals, e.g. -1.0, 3.14
* `String` a sequence of unicode characters, e.g. "hello world"
* `Date` a M/D/YYYY formatted date, e.g. "12/25/2018"

You can, and often must, specify the type of a query or value explicitly using the syntax `as type`.  While this may seem cumbersome, it helps you avoid bad assumptions where you think the data is one type when, in fact, it is another.  If the data do not satisfy your assumptions then abcde will generate an error, giving you an opportunity to rectify the problem.

In the example above we explicitly take `vitals.weight as f32` as a floating point number and `prefs.ice_cream` as a boolean.  This tells abcde what to expect when it is parsing data from the data files.  For example, it tells `abcde` to interepret an ice cream preference of "Y" as `true` and "N" as false.  If the program encounters unexpected data for ice cream preference such as "yummy" it will generate an error to tell you that you have made an incorrect assumption about the type of data the `prefs.ice_cream` query contains.

As we will see later on, sometimes abcde can infer the type of the data for you.  In the example above it is able to infer that the `true` in `... where true;` is a boolean value.  However, we could have specified this explicitly if we wanted to with `... where true as bool;`.

### Escaping the dot `.`

By convention queries are named `the_table.some_field`.  However, some of the data deviate from this convention by allowing a dot, period, or `.` inside the field name, for example: `the_table.some.field`.  Within the tutorial data we have the query for eye color:

```
abcde -d tutorial capture --code \
'select vitals.eye.color as String where true;'
```
```
ABCDE encountered an error and had to exit.
Lexer/Parser error: Unrecognized token `.` found at 17:18
Expected one of "!=", "&&", ")", "*", "+", ",", "-", "/", ";", "<", "<=", "==", ">", ">=", "as", "where", "{", "||" or "}"
```

What happened here?  ABCDE was confused by the query `vitals.eye.color`.  It isn't sure how to interpret the `.` within the field name `eye.color`.  In order to capture such queries we must escape the field containing the dot with quotes:

```
abcde -d tutorial capture --code \
'select vitals."eye.color" as String where true;'
```
```
".subjectkey" ".interview_date" "vitals.eye.color"
"NDAR_INVABCD1234" "12/25/2018" "brown"
"NDAR_INVEFGH5678" "7/4/2018" "blue"
"NDAR_INVEFGH5678" "7/4/2019" "blue"
...
```

### Putting Code in a File

The `--code` argument is for supplying code written inside the command line arguments themselves.  This works fine for small snippets of code, but when writing larger blocks of code it is more ergonomic to put the code inside of a file.  Try creating the following file, saving it as `code.txt`, and passing it to `abcde` using the `--code-file` argument:

```
// code.txt
// This line is a comment.  Comments are ignored by abcde...
// ...but appreciated by your fellow humans!
// Generate a table of weights and ice cream preference.
select vitals.weight as f32, prefs.ice_cream as bool where true;
```
```
abcde -d tutorial capture --code-file code.txt
```

You should get the same output as above.  From now on instead of showing example captures as full abcde commands we will just show the contents of the example code file.

This example introduces another kind of statement, the comment.  A comment statement starts with `//` and runs to the end of the line in a file.  ABCDE ignores comments when evaluating your code, but your fellow humans will appreciate these clues about what your code is doing!

### Dealing with Longitudinal Data

ABCD contains longitudinal data, i.e. duplicate observations of the same field for the same subject at different points in time.  Consider the following example:

```
select vitals.weight as f32 where true;
```
```
".subjectkey" ".interview_date" ".eventname" "vitals.weight"
"NDAR_INVABCD1234" "12/25/2018" "baseline" 190
"NDAR_INVABCD1234" "11/2/2019" "followup" 195
...
```

See how subject NDAD_INVABCD1234 was interviewed twice, once on 12/25/2018 and once on 11/2/2019?  In ABCD, these longitudinal interviews are identified by an `eventname` field.  Here the first interview was from the `baseline` event and the second interview was from the `followup` event.

Often we would like data from just one event.  We can select this event using the equal to `==` comparison operator in the predicate like this:

```
select vitals.weight as f32 where eventname == "baseline";
```
```
".subjectkey" ".interview_date" ".eventname" vitals.weight"
"NDAR_INVABCD1234" "12/25/2018" "baseline" 190
"NDAR_INVEFGH5678" "7/4/2018" "baseline" 130
"NDAR_INVIJKL9012" "1/1/2018" "baseline" 170
```

Now we get just the data from the baseline event.

### Guaranteeing Uniqueness

As mentioned in the previous section on special queries and keys, each observation in the data is uniquely identified by a key.  In the tutorial data set, as in the ABCD data itself, the key is a combination of `.subjectkey` and `.interview_date`.  That means that no two observations will have the same NDAR_INVXXXXXXXX `.subject_key` and M/D/YYYY `.interview_date`.  However, **two observations can have the same `.subjectkey`** so long as each has a different `.interview_date`.  As we saw in the section above, this scenario is actually quite common when dealing with longitudinal data!

> Note: The ABCD 3.0 data release erroneously contains some observations/rows with the same subjectkey and interview_date.  Thus, when working with the 3.0 release, you may also have to use eventname as part of the unique key.

Often we will want to generate exactly row in out output table for each subject.  We can communicate this intent to abcde by explicitly specifying the `.subjectkey` in our list of symbols.  In the previous section we ran this code and `abcde` automatically added the `.subjectkey`, `.interview_date`, and `.eventname` to the output:

```
select vitals.weight as f32, prefs.ice_cream as bool where true;
```

We could have explicitly requested these fields in the output with the following.  (Notice how the types of special queries is automatically inferred.)  The output is the same as above.

```
select .subjectkey, .interview_date, .subjectkey
vitals.weight as f32, prefs.ice_cream as bool
where true;
```

We can explicitly ask for the `.subjectkey` only and omit the `.interview_date` and `.eventname` to force the output to be unique on `.subjectkey` alone:

```
select .subjectkey,
vitals.weight as f32, prefs.ice_cream as bool
where true;
```
```
".subjectkey" "vitals.weight" "prefs.ice_cream"
"NDAR_INVABCD1234" 190 true
ABCDE encountered an error and had to exit.
Invalid select statement: Duplicate row for subjectkey NDAR_INVABCD1234.  Hint: Write "select .subjectkey .interview_date" to allow rows with the same subjectkey but a different interview_date.
```

Uh-oh, what happened here?  We tried to generate an output table with unique rows based on `.subjectkey` alone, but there are two matching rows for subject NDAR_INVABCD1234.  ABCDE detects this problem and generates an error.

If we really want to generate no more than one row for each subject then we need to tell abcde how we would like to disambiguate the longitudinal observations.  As shown in the previous section, we can do this by predicating the capture on the event name:

```
select .subjectkey,
vitals.weight as f32, prefs.ice_cream as bool
where .eventname == "baseline";
```
```
".subjectkey" "vitals.weight" "prefs.ice_cream"
"NDAR_INVABCD1234" 190 true
"NDAR_INVEFGH5678" 130 false
"NDAR_INVIJKL9012" 170 true
```

This captures only the observations from the baseline longitudinal event.  **By explicitly capturing only `.subjectkey` we guarantee there are no duplicate observations by subject.**

### Variables

Thus far we have been testing out hypothesis by capturing weight and ice cream preference.  It does appear as if subjects who like ice cream have higher weights.  However, weight is not a perfect measure of adiposity.  It could be that subjects who consume ice cream regularly are heavier because they become taller and more muscular!

Instead of just looking at weight we will look at body mass index (BMI), a composite index of adiposity calculated from weight and height.  Unfortunately, the data do not have a BMI query.  Fortunately we do have weight and height, and we can compute the BMI according to the formula BMI = 703 * ( weight / height^2 ) assuming units of inches and pounds.  Let's see how variables can help us calculate a BMI.

``` 
let bmi = 703.0 * vitals.weight / (vitals.height * vitals.height);
select where true;
```
```
".subjectkey" ".interview_date" "vitals.height" "vitals.weight" "bmi"
"NDAR_INVABCD1234" "12/25/2018" 75 190 23.745777
...
```

We've introduced a few new concepts here, the most important of which is variables.  Variables are defined using a let statement `let varname = expression;`.  The type of the variable is always inferred from the expression on the right hand side of the `=` and cannot be specified explicitly.  Variables are a type of symbol and can be used in a select statement just like a query.  With variables it is possible to:

* Give queries with long names shorter, more comprehensible names, e.g.

  ```
  let shortname = very_long_and.annoying_query_name as type;
  select shortname where true;
  ```
* Perform a calculation and use the results, e.g.
  
  ```
  let two = the.one as i32 + 1;
  select another.query as type where two == 2;
  ```

> Tip: You can re-use variable names.  The BMI calculation could have been written in two steps:
> 
> ```
> let bmi = 703.0 * vitals.height;
> let bmi = bmi / (vitals.weight * vitals.weight);
> ```

Another new trick we saw is leaving the _what_ of the select statement empty by writing `select where predicate`.  When you do this abcde will automatically generate output for all the symbols used in the code.

The careful reader will also notice that we did not specify the type of `vitals.weight` or `vitals.height` with `as f32` as we did in prior examples.  By using these queries in the context of multiplication and division we allowed abcde to infer that they must be floating point numbers.

> Tip: When performing integer division the part after the decimal point is dropped.  `5.0/2.0` = 2.5, but `5/2` = 2.  When floating point division is desired, be sure to add a decimal point (e.g. `1.0`) or specify the type explicitly (e.g. `1 as f32`).

#### Splitting Code Into Multiple Files

Earlier in the tutorial we saw how to place code inside a file and evaluate that code with `--code-file`.  As our code becomes more complex we might wish to split code into multiple files.  For example, we can create a file that calculates BMI called `bmi.txt`:

```
// bmi.txt
let bmi = 703.0 * vitals.weight / (vitals.height * vitals.height);
```

And then create another file that _includes_ the file `bmi.txt` using the include directive.

```
// code.txt
include "bmi.txt";
select where bmi > 25;
```
```
".subjectkey" ".interview_date" "vitals.height" "vitals.weight" "bmi"
"NDAR_INVIJKL9012" "1/1/2019" 70 180 25.82449
...
```

The `include` statement modeled above is very simple.  It takes the name of a file enclosed by double quotes.  It parses the code in the file and inserts that code into the parent code.  We can observe this directly with the `--verbose` flag:

```
abcde --verbose -d tutorial capture --code-file code.txt
```
```
...
Code parsed as:
include "bmi.txt";
select  where ( bmi > 25 as i32 );
Expanding code... done.
Code expanded as:
let bmi = ( ( 703 as f32 * vitals.weight ) / ( vitals.height * vitals.height ) );
select  where ( bmi > 25 as i32 );
...
```

> Tip: The "main" or top-level code must end with a select statment, however included code can but does not _need_ to have any select statements in it.

> Tip: You can include code recursively.  That is, an included code file can include other code files, etc.

### Missing Data

Suppose we want to perform a more sophisticated analysis taking into account such factors as sex differences or the effect of consumption of healthy foods like salad?  Looking at our sample values using the explore subcommand from the beginning of the tutorial it appears that some of the data is missing.  How can this be?  Let's perform an exploratory capture to get a fuller picture:

```
select vitals.gender as String, prefs.ice_cream as bool,
prefs.salad as bool, prefs.curry as bool where true;
```
```
".subjectkey" ".interview_date" "vitals.gender" "prefs.ice_cream" "prefs.salad" "prefs.curry"
"NDAR_INVABCD1234" "12/25/2018" "M" true false true
"NDAR_INVEFGH5678" "7/4/2018" None false None false
"NDAR_INVEFGH5678" "7/4/2019" None false true None
"NDAR_INVIJKL9012" "1/1/2018" "F" true true None
"NDAR_INVIJKL9012" "1/1/2019" "F" true true true
```

In our capture output we can see several `None` where the data are missing.  Unfortunately, there is no unambiguous way to interepret this.  It looks like NDAR_INVIJKL9012 had never tasted curry before and so did not submit a preference for curry during the first interview.  Then she went out and tried curry, liked it, and did record a response on the second interview.

On the other hand take subject NDAR_INVEFGH5678.  It did not record a response for salad on the first interview.  Had it really never tried salad before, or was it just too lazy to fill out the questionnaire completely?  Did it forget what curry tastes like between the first and second interview?  Did it forget to mark gender twice, or does it identify as non-binary or transgender?

With no crystal ball it is difficult to interpret missing data, so often we will want to exclude it.

```
// Convert gender M/F into a boolean
let male = if vitals.gender as String == "M" { true }
else if vitals.gender == "F" { false };

// Exclude rows with missing values.
select .subjectkey, male, prefs.ice_cream as bool, prefs.salad as bool
where prefs.eventname == "baseline" 
&& male != None && prefs.ice_cream != None && prefs.salad != None;
```
```
".subjectkey" "male" "prefs.ice_cream" "prefs.salad"
"NDAR_INVABCD1234" true true false
"NDAR_INVIJKL9012" false true true
```

The above example demonstrates:

* Compare a symbol to `None` in the predicate of a select statement to include or exclude missing values from the captured output.
* Combine multiple boolean statements with logical and `&&`.  You cal also use logical or `||` and group comparisons by enclosing them in parentheses, e.g. `one && (two || three)`.
* Use an if expression to perform logical branching.  For example:

  ```
  let num = 4;
  let size = if num < 2 { "small" }
  else if num < 5 { "medium" }
  else if num < 10 { "big" }
  else { "huge" };
  // size == "medium"
  ```

### Saving Output(s) to a File

Thus far we have been running the following, which prints the output table to the terminal:

```
abcde -d tutorial capture --code-file code.txt
```

Once we graduate from the tutorial data set the output tables may become very large (thousands of lines).  On the Linux shell you can can redirect the output to a file:

```
abcde -d tutorial capture --code-file code.txt > output.txt
```

However there is a better way, which is to pass an output file name with the `--output` argument:

```
abcde -d tutorial capture --code-file code.txt --output output.txt
```

For convenience, you can shorten `--output` to `-o`.  You can use the argument multiple times, once for each select statement in the code.

```
// code.txt
// gender vs weight table in out1.txt
select vitals.gender as String, vitals.weight as f32 where true;
// ice cream vs weight table in out2.txt
select prefs.ice_cream as bool, vitals.weight as f32 where true;
```
```
abcde -d tutorial capture --code-file code.txt -o out1.txt -o out2.txt
```

`out1.txt`

```
".subjectkey" ".interview_date" "vitals.gender" "vitals.weight"
"NDAR_INVABCD1234" "12/25/2018" "M" 190
"NDAR_INVEFGH5678" "7/4/2018" None 130
"NDAR_INVEFGH5678" "7/4/2019" None 131
"NDAR_INVIJKL9012" "1/1/2018" "F" 170
"NDAR_INVIJKL9012" "1/1/2019" "F" 180
```

`out2.txt`

```
".subjectkey" ".interview_date" "prefs.ice_cream" "vitals.weight"
"NDAR_INVABCD1234" "12/25/2018" true 190
"NDAR_INVEFGH5678" "7/4/2018" false 130
"NDAR_INVEFGH5678" "7/4/2019" false 131
"NDAR_INVIJKL9012" "1/1/2018" true 170
"NDAR_INVIJKL9012" "1/1/2019" true 180
```

## The ABC's of ABCD

The first half of this tutorial covered all of the features of the explore and capture subcommands.  I recommend working through the first half before you read this second half.  After you have read the first half you are already prepared to go directly to working on the ABCD data.  This second half of the tutorial will not cover any new features of the software.  Instead, we will go over examples that apply the skills learned in the first half using real life examples from the ABCD data.

### Getting the Data

Please refer back to the main [README](../README.md) section on getting the data.  As a reminder:

* You will need permission to access the ABCD data through the [Data Use Agreement](https://nda.nih.gov/user/dashboard/data_permissions.html).
* You may already have access to the data on an institutional server.  Data users at Washington University in St. Louis who are part of the NIL can find the data under `/data/Daenerys/ABCD/data/behavioral_data/`.
* If you don't already have access, use the download subcommand of abcde.  You will be prompted for your NDA user name and password.
  
  ```
  mkdir data
  abcde -v -d data download
  ```
  
* For convenience, set the `ABCDE_DATA_DIR` shell environment variable so that you do not have to specify `-d/--data-dir` each time you run abcde.  For brevity, we will assume you have set `ABCDE_DATA_DIR` and omit the `-d` from subsequent examples.

### Getting Information About a Table

The abcde explore subcommand provides information about individual queries, but it (currently) does not provide metadata about entire tables.  You can look up table information easily in your web browser using the NDA's unofficial API.  You can [browse a list of all tables](https://nda.nih.gov/data_dictionary.html?source=ABCD%2BRelease%2B3.0&submission=ALL) or look up information about a specific table by visiting `https://nda.nih.gov/data_structure.html?short_name=table_name`.  For example, to get information about the `abcd_ant01` table visit https://nda.nih.gov/data_structure.html?short_name=abcd_ant01 .

### Going by an Alias

Some fields in ABCD go by more than one name, for example sex vs gender.  These alternative field names are referred to as _aliases_.  To see if a field has an alias, get information about the field using the explore subcommand:

```
abcde explore --info abcd_ant01.sex
```
```
Query: abcd_ant01.sex
Description:
Sex of the subject
Dictionary:
Sex of the subject
Notes:
M = Male; F = Female; O=Other; NR = Not reported
Value range: M;F; O; NR
Aliases: gender
Sample values:
F
M
...
```

Here the name of the field is `sex` and its alias is `gender`.  In the ABCD 2.0 release we didn't have to worry much about aliases.  Sometimes they gave us a hint about the meaning of a field, but otherwise we didn't need to know about them.

Unfortunately, **in ABCD 3.0 names and aliases have been swapped**.  In ABCD 2.0, `abcd_ant01.gender` was the field name and `sex` was the alias.  In ABCD 3.0 this has been swapped for most fields, causing code used in ABCD 2.0 to break.  Oops!

To work around this problem there are plans to make abcde automatically look up queries by name _or_ alias for you.  This feature has not been implemented yet.  For now you must either modify your code to swap name <--> alias or appeal to the [ABCD Data Analysis and Informatics Core](https://abcd-workspace.ucsd.edu/pages/daic.html) to correct these errata in the 3.0 release.

### BMI

To whet our appetite, let's compute BMI as we did in the tutorial data.  A quick exploratory search confirms there is no BMI in the ABCD data.

```
abcde explore --search '(?i)bmi'
```

We do have access to height and weight.

```
abcde explore --search '(?i)height'
```
```
...
Query: abcd_ant01.anthro_1_height_in
Description:
STANDING HEIGHT #1 (in inches)

Query: abcd_ant01.anthro_2_height_in
Description:
STANDING HEIGHT #2 (in inches)

Query: abcd_ant01.anthro_3_height_in
Description:
STANDING HEIGHT #3 (in inches)

Query: abcd_ant01.anthro_height_calc
Description:
Standing Height Average (inches): If three measurements were obtained, the two closest measurements will be averaged. Should the third measurement fall equally between the first two measurements, all three will be averaged.
...
```

At each encounter, a pseudo-average height in inches is computed from 3 separate measurements.  It is this `abcd_ant01.anthro_height_calc` we are interested in.  There is a similar measure for weight in pounds:

```
abcde explore --search '(?i)weight' --table abcd_ant01
```
```
...
Query: abcd_ant01.anthro_weight_calc
Description:
Average Measured Weight (lbs):If three measurements were obtained, the two closest measurements will be averaged. Should the third measurement fall equally between the first two measurements, all three will be averaged.
...
```

There are some additional fields for whether or not the subject is wearing a cast, which would affect his or her weight, but we will ignore this for now.

Recall the formula in inches/pounds is 703 * ( weight / height^2 ).  When expressing this in code, we will use `703.0` to make sure abcde performs floating-point rather than integer arithmetic.

```
// code.txt
let height = abcd_ant01.anthro_height_calc as f32;
let weight = abcd_ant01.anthro_weight_calc as f32;
let bmi = 703.0 * weight / (height * height);
select bmi where true;
```
```
abcde capture --code-file code.txt
```
```
".subjectkey" ".interview_date" "bmi"
"NDAR_INVXXXXXXXX" "10/1/2018" 20.48054
"NDAR_INVXXXXXXXX" "4/22/2018" 22.022085
"NDAR_INVXXXXXXXX" "2/21/2017" 18.234287
...
```
Voilà, a table of BMI for each subject.  (Real subject IDs were obfuscated to maintain confidentiality.)

### Matlab

The aim of the abcde tool is to capture output tables for you to analyze.  ABCDE is a necessary evil, what you really want is to perform some analysis on the captured tables.  There are many excellent software suites for this purpose including [Matlab](https://www.mathworks.com/)/[Octave](https://www.gnu.org/software/octave/), [R](https://www.r-project.org/), etc.  Matlab is widely used at Washington University in St. Louis and in imaging laboratories around the world.  This section will demonstrate how to import data into Matlab.

#### Output Format

ABCDE allows you to fine-tune the format of output tables by specifying command line options.

```
abcde help capture
```
```
...
FLAGS:
        --fmt-matlab     Render output in a format that is comprehensible to Matlab's readtable()
        --skip-header    The first row of output defaults to a header of field names.  Specify this option to skip
                         writing the header

OPTIONS:
        --text-delim <delim>       Delimeter for text (non-numeric) values.
                                   This includes strings and dates [default: "]
        --false <false-str>        How to render boolean false.
                                   Note that boolean values will not have delimeters automatically placed around them
                                   [default: false]
        --header-delim <hdelim>    Delimeter for fields in the header [default: "]
        --missing-text <miss>      How to render missing text values.
                                   Note that missing values will not have delimeters automatically placed around them
                                   [default: None]
        --num-delim <ndelim>       Delimeter for numeric values [default: ]
        --missing-num <nmiss>      How to render missing numeric value.
                                   Note that missing values will not have delimeters automatically placed around them
                                   [default: None]
        --sep <sep>                Column separator [default:  ]
        --true <true-str>          How to render boolean true.
                                   Note that boolean values will not have delimeters automatically placed around them
                                   [default: true]
...
```

As an example of how to use these options, consider that Matlab interprets 1 as true and 0 as false.  When writing an output table for use in Matlab we would therefore include the options:

```
abcde capture ... --true 1 --false 0
```

Matlab is so common that, for convenience, we have provided a special flag `--fmt-matlab` that automatically sets all the options you would typically want.  Generating an output table of BMI for Matlab (see example in the previous section) is as simple as:

```
abcde capture \
--code-file bmi-code.txt \
--output bmi-table.txt \
--fmt-matlab
```

#### Import to Matlab

##### Readtable

We have found that the most straightfoward way to import data tables into Matlab is with the [`readtable`](https://www.mathworks.com/help/matlab/ref/readtable.html) function.  Run `help readtable` in Matlab for more information, but the usage is very simple.  Given an output table from abcde stored in `table.txt`:

```
% Read table into Matlab
table = readtable('table.txt');
```

##### `.` Becomes `_`

Matlab doesn't like having table columns that contain a `.` because they conflict with Matlab's notation for accessing data structures.  The `readtable` function will automatically rename affected columns with an underscore `_` and prepend `x` if the column would then start with an underscore.  For example, `.subjectkey` will become `x_subjectkey` and `foo.bar` will become `foo_bar`.  If you do not like typing `x_subjectkey` you can rename the table column:

```
% Rename column to Subject
table.Properties.VariableNames{'x_subjectkey'} = 'Subject';
```

##### Categorical Data

You may have some table columns that are categorical.  In the Gender section below, the query `abcd_lpds01.demo_gender_id_v2_l` can nominally contain the values 1, 2, 3, 4, or 5.  These numbers do not represent different degrees of transgenderism, i.e. someone who answers 5 is not "more" transgender than someone who answers 1.  Rather, the numbers represent different _categories_ of response, i.e. 1 = cis-male, 2 = cis-female, 3 = trans-male, etc.  Prior to performing statistics you should tell Matlab that the data in this column are categorical:

```
% Make a column categorical
table.abcd_lpds01_demo_gender_id_v2_l = categorical(table.abcd_lpds01_demo_gender_id_v2_l);
```

##### Trimming `.subjectkey`

Some users find it annoying to use the full NDAR_INVXXXXXXXX identifier in `.subjectkey` inasmuch as only the last 8 "X" characters vary between subjects.  If you would like to trim away those first 8 superfluous characters:

```
table.Subject = arrayfun(@(s) {extractAfter(s{1}, 'NDAR_INV')}, table.Subject);
```

##### Statistics

Performing statistical regression is quite complicated and beyond the scope of this tutorial.  If you are interested in frequentist statistics using the generalized liner model (GLM) in Matlab then the [`fitlm`](https://www.mathworks.com/help/stats/fitlm.html) function is a good starting point.

```
% Read more
help fitlm
```

### Logitudinal Data Revisited

ABCD is a longitudinal study containing multiple observations of the same variable for the same subject across time.  No two observations should have the same `.subjectkey` and`.interview_date`, but it is entirely possible (and common) for two observations to **have the same `.subjectkey`**.

> Note: Due to a mistake in the ABCD 3.0 release, some observations actually _do_ have the same `.subjectkey` and `.interview_date`.  These observations can still be disambiguated using `.eventname`.  No two observations have the same combination of all three `.subjectkey`, `.interview_date`, and `.eventname`.

Oftentimes we will want to contrain our data to have a unique `.subjectkey`.  Recall from the first half of this tutorial that we can do this by explicitly capturing the `.subjectkey` but _not_ the `.interview_date` or `.eventname` in a select statement.

```
// Doesn't work :-(
// Capture exactly one height measurement per subject.
select .subjectkey, abcd_ant01.anthroheightcalc as f32 where true;
```
```
".subjectkey" "abcd_ant01.anthroheightcalc"
"NDAR_INVAAAAAAAA" 56.5
"NDAR_INVBBBBBBBB" 56.5
"NDAR_INVCCCCCCCC" 56.5
"NDAR_INVDDDDDDDD" 57.5
"NDAR_INVEEEEEEEE" 56.5
ABCDE encountered an error and had to exit.
Invalid select statement: Duplicate row for subjectkey NDAR_EEEEEEEE.  Hint: Write "select .subjectkey .interview_date" to allow rows with the same subjectkey but a different interview_date.
```

What went wrong?  ABCDE got through the first 5 observations and then encountered a duplicate subject on the 6th observation.  We need to provide a predicate for the select statement to disambiguate these duplicate responses.  As in the tutorial data, we can disambiguate longitudinal data using the `.eventname` field.  In the ABCD data there are (as of the 3.0 release) 3 longitudinal events:

* `baseline_year_1_arm_1` baseline MRI scan
* `1_year_follow_up_y_arm_1` 1st follow-up interview, no associated scan
* `2_year_follow_up_y_arm_1` 2nd follow-up interview, 1st follow-up scan

So to capture exactly one height measurement per subject from the _baseline interview only_ we could do:

```
select .subjectkey, abcd_ant01.anthroheightcalc as f32 where abcd_ant01.eventname == "baseline_year_1_arm_1";
```

### Finding Subjects with MRI Scans

For the example data in the first half of this tutorial we constrained by date.  For example:

```
// Doesn't work :-(
select .subjectkey, ... where .interview_date < 1/1/2019;
```

Unfortunately, the ABCD data were obtained interleaved.  That is, the first subject was brought back for his second interview before the second subject was brought in for her first interview.  Consequently there is no one cutoff date shared among all subjects.

Happily for those of us who are limited to the subset of subjects who have had an MRI scan, each subject has just one scan in the `abcd_mri01` table.  It is therefore possible to capture a table where each row is a unique subject who has had an MRI scan with:

```
select ... where abcd_mri01.mri_info_studydate as Date != None;`
```

For example:

```
// Capture exactly one height measurement per subject.
select .subjectkey, abcd_ant01.anthro_height_calc as f32
where abcd_mri01.mri_info_studydate as Date != None;
```
```
".subjectkey" "abcd_ant01.anthro_height_calc"
"NDAR_INVAAAAAAAA" 56.5
"NDAR_INVBBBBBBBB" 56.5
"NDAR_INVCCCCCCCC" 56.5
"NDAR_INVDDDDDDDD" 57.5
"NDAR_INVEEEEEEEE" 56.5
...
```

If you are not studying the MRI data then there are other tables which (at least for now) contain only unique subject entries.  For example, there are 11875 unique subjects in the demographics table:

```
select .subjectkey where pdem02.interview_age as u32 != None;
```

You can contrast these tables to find out, for example, how many subjects _didn't_ complete an MRI (there are 120 of them):

```
select .subjectkey where pdem02.interview_age as u32 != None
&& abcd_mri01.mri_info_studydate as Date == None;
```

### Gender

This section is generally useful because many researchers will be interested in studying gender directly or as a covariate.  Working through the moderately difficult examples in this section will also solifidy your understanding of the abcde tool and the pitfalls of the ABCD data set.

Sex/gender is a tricky issue in ABCD.  In such a large cohort of mostly pre-pubertal adolescents we are, of course, very interested in unconventionally gendered subjects, e.g. transgender, non-binary gender, etc.  You will find that nearly ever table contains a `gender` field, which is a sort of rough estimation of binary gender.  However, we would like to perform a more detailed exploration of the data.

#### Parent or Child?

Note that some questions in ABCD are asked of the parent and others are asked of the child.  In general, table names or questions with `p` are asked of the parent.  Sometimes these are questions about the child, for example: `abcd_lpksad01.kbi_p_c_trans_l` "Is your child transgender?"  However, beware of questions that are directed wholly at the parent, e.g. `pasr01.asr_q110_p` "I [the parent] with I were of the opposite sex."

#### Queries

You can find most of the relevant gender fields with the following regex.  The output is shortened to the most relevant queries as they pertain to this tutorial.

```
abcde explore --search 'gender.|sex'
```
```
Query: abcd_lpds01.demo_gender_id_v2_l
Description:
What is the child's current gender identity? ¿Cuál es la identidad de género actual de el/la nino(a)?

Query: abcd_lpksad01.kbi_p_c_trans_l
Description:
Is your child transgender? ¿Su niño(a) es transgénero

Query: pdem02.demo_sex_p
Description:
What sex was the child assigned at birth, on the original birth certificate? ¿Qué sexo se le asignó al/la niño(a) cuando nació, en su acta de nacimiento original?

Query: pdem02.demo_gender_id_p
Description:
What is the child's current gender identity? ¿Cuál es la identidad de género actual de el/la niño(a)?

Query: abcd_ppdms01.pubertdev_sex_p
Description:
What sex was your child assigned at birth? ¿Qué sexo le fue asignado a su niño(a) al nacer?

Query: abcd_cbcl01.cbcl_q110_p
Description:
Wishes to be of opposite sex Desea ser del sexo opuesto

Query: abcd_yksad01.ksads_back_trans_id
Description:
Are you transgender?

Query: abcd_ypdms01.pubertdev_sex
Description:
Do you consider yourself male or female?

Query: dibf01.ksads_back_c_trans_p
Description:
Is you/your child transgender?/  ¿Su niño(a) es transgénero
```

That's quite a lot of questions on gender!  We can look up the meanings of the responses in the data dictionary:

```
abcde explore --info abcd_lpds01.demo_gender_id_v2_l
```
```
...
What is the child's current gender identity? ?Cu?l es la identidad de g?nero actual de el/la nino(a)?
Notes:
1 = Male Hombre; 2 = Female Mujer; 3 = Trans male Transexual masculino / hombre transexual; 4 = Trans female Transexual femenino / mujer transexual; 5 = Gender queer Interg?nero (
Value range: 1 ; 2 ; 3 ; 4 ; 5 ; 6 ; 777 ; 999
Aliases: genderqueer
Sample values:
2
1
2
...
```

So, 1 = male, 2 = female, and anything else means not precisely male or female.  The first few subjects all identify as either male or female.

```
abcde explore --info abcd_lpksad01.kbi_p_c_trans_l
```
```
...
Is your child transgender? ?Su ni?o(a) es transg?nero
Notes:
1 = Yes S?; 2 = Maybe/Don't know Tal vez/No s?; 3 = No No; 4 = Decline to answer Niego contestar
Value range: 1 ; 2 ; 3 ; 4
Aliases: kbi_c_trans_p_l,ksads_back_c_trans_p_l
Sample values:
3
3
3
...
```

The most common answer is 3, not transgender.  It is also possible to leave the question blank or explicitly decline to answer with 4.

#### Example Capture

Armed with this knowledge we are able to have a bit of fun.  It would appear that there are roughly 109 parent-child dyads that do not share the same belief about the child's gender identity.  To get a totally accurate count we would need to consider more queries, as explained in the Pitfalls section below.

```
select .subjectkey,
abcd_yksad01.ksads_back_trans_id,
abcd_lpksad01.kbi_p_c_trans_l,
where abcd_yksad01.ksads_back_trans_id != 4
&& abcd_lpksad01.kbi_p_c_trans_l != 4
&& abcd_yksad01.ksads_back_trans_id != abcd_lpksad01.kbi_p_c_trans_l;
```
```
abcde capture --code-file code.txt | wc
    110     330    2588
```

> Tip: Pipe output tables through the `wc` (word count) command.  The first column of output is the number of lines.  Subtract one line for the header and you get the number of observations captured by a query.

#### Pitfalls

There are quite a few similar queries about gender.  On one hand it is good that the ABCD study is exploring gender identify fully by asking about it in different ways.  On the other hand, this poses a problem for us data analysts.  How should we interpret anomalous responses?

For example, observe that 3 parents marked their children as the mysterious 777 gender, which is listed in the dictionary as a valid option but with no explanation of what it means.

```
select .subjectkey where abcd_lpds01.demo_gender_id_v2_l as u32 == 777;
```
```
abcde capture --code-file code.txt | wc
      4       4      71
```

> Recall: Piping output through `wc` counts the numer of lines, words, and characters.

There are also lots of observations in which the question was not answered at all.  Some of this may have to do with different questions being asked at different time points in a longitudinal study, but it is still quite confusing.

```
select where abcd_lpds01.demo_gender_id_v2_l as u32 == None;
```

It is also possible for subjects to answer inconsistently.  For example, a parent could respond to `abcd_lpds01.demo_gender_id_v2_l` as 1 (cis-male) but `abcd_lpksad01.kbi_p_c_trans_l` as 1 (transgender).  Or respond of cis-male for the `abcd_lpds01` table but cis-female for the same question in the `pdem02` table.

How to deal with such inconsistencies, ambiguous responses, etc depends on the specific hypothesis.  The safest and most general option is to treat such inconsistent data as "missing" or `None`.  We will see how to do this in the following section.

### A Formula for Gender

The following code distills the gender questions into binary male/female and transgender.  It checks for inconsistencies and treats those observations as "missing" or `None`.

```
// boolean for male
// true if male
// false if female
// None if transgender, questioning, etc
let male = if ( abcd_lpksad01.kbi_p_c_trans_l == 3
    || abcd_lpksad01.kbi_p_c_trans_l == 4
    || abcd_lpksad01.kbi_p_c_trans_l == None
) && ( dibf01.ksads_back_c_trans_p == 3
    || dibf01.ksads_back_c_trans_p == 4
    || dibf01.ksads_back_c_trans_p == None
) && ( abcd_yksad01.ksads_back_trans_id == 3
    || abcd_yksad01.ksads_back_trans_id == 4
    || abcd_yksad01.ksads_back_trans_id == None
) && ( abcd_cbcl01.cbcl_q110_p == 0 || abcd_cbcl01.cbcl_q110_p == None
) { if ( abcd_lpds01.demo_gender_id_v2_l == 1
        || abcd_lpds01.demo_gender_id_v2_l == 2
        || abcd_lpds01.demo_gender_id_v2_l == None
    ) && ( pdem02.demo_sex_p == 1
        || pdem02.demo_sex_p == 2
        || pdem02.demo_sex_p == None
    ) && ( pdem02.demo_gender_id_p == 1
        || pdem02.demo_gender_id_p == 2
        || pdem02.demo_gender_id_p == None
    ) && ( abcd_ppdms01.pubertdev_sex_p == 1
        || abcd_ppdms01.pubertdev_sex_p == 2
        || abcd_ppdms01.pubertdev_sex_p == None
    ) && ( abcd_ypdms01.pubertdev_sex == 1
        || abcd_ypdms01.pubertdev_sex == 2
        || abcd_ypdms01.pubertdev_sex == None
    ){ if ( abcd_lpds01.demo_gender_id_v2_l == 1
            || abcd_lpds01.demo_gender_id_v2_l == None
        ) && ( pdem02.demo_sex_p == 1
            || pdem02.demo_sex_p == None
        ) && ( pdem02.demo_gender_id_p == 1
            || pdem02.demo_gender_id_p == None
        ) && ( abcd_ppdms01.pubertdev_sex_p == 1
            || abcd_ppdms01.pubertdev_sex_p == None
        ) && ( abcd_ypdms01.pubertdev_sex == 1
            || abcd_ypdms01.pubertdev_sex == None
        ) { if abcd_lpds01.demo_gender_id_v2_l == 1
            || pdem02.demo_sex_p == 1
            || pdem02.demo_gender_id_p == 1
            || abcd_ppdms01.pubertdev_sex_p == 1
            || abcd_ypdms01.pubertdev_sex == 1
            { true }
        }
        else if ( abcd_lpds01.demo_gender_id_v2_l == 2
            || abcd_lpds01.demo_gender_id_v2_l == None
        ) && ( pdem02.demo_sex_p == 2
            || pdem02.demo_sex_p == None
        ) && ( pdem02.demo_gender_id_p == 2
            || pdem02.demo_gender_id_p == None
        ) && ( abcd_ppdms01.pubertdev_sex_p == 2
            || abcd_ppdms01.pubertdev_sex_p == None
        ) && ( abcd_ypdms01.pubertdev_sex == 2
            || abcd_ypdms01.pubertdev_sex == None
        ) { if abcd_lpds01.demo_gender_id_v2_l == 2
            || pdem02.demo_sex_p == 2
            || pdem02.demo_gender_id_p == 2
            || abcd_ppdms01.pubertdev_sex_p == 2
            || abcd_ypdms01.pubertdev_sex == 2
            { false }
        }
    }
};

// true if definitely transgender
// false if definitely cis-gendered
// None if ambiguous or unsure
let transgender = if male != None { false }
else if (
    abcd_lpksad01.kbi_p_c_trans_l == 1
    || abcd_lpksad01.kbi_p_c_trans_l == 4
    || abcd_lpksad01.kbi_p_c_trans_l == None
) && ( dibf01.ksads_back_c_trans_p == 1
    || dibf01.ksads_back_c_trans_p == 4
    || dibf01.ksads_back_c_trans_p == None
) && ( abcd_yksad01.ksads_back_trans_id == 1
    || abcd_yksad01.ksads_back_trans_id == 4
    || abcd_yksad01.ksads_back_trans_id == None
) { if abcd_lpksad01.kbi_p_c_trans_l == 1
    || dibf01.ksads_back_c_trans_p == 1
    || abcd_yksad01.ksads_back_trans_id == 1
    { true }
};
```

Now we can clearly see that there are 6 transgender subjects.

```
select .subjectkey where transgender == true
&& abcd_mri01.mri_info_studydate as Date != None;
```

And an additional 346 subjects who are either questioning their gender or have difficulty filling out forms consistently.

```
select .subjectkey where male == None 
&& abcd_mri01.mri_info_studydate as Date != None;
```

Note that the above derivation of gender matches up pretty well with the canned `gender` field, but there _are_ a non-trivial 197 disagreements.  Ultimately _you_ decide which formula for gender to use in _your_ research.

```
select .subjectkey where
( abcd_lpksad01.gender == "M" && male == None )
|| ( abcd_lpksad01.gender == "M" && male != true )
|| ( abcd_lpksad01.gender == "F" && male == None )
|| ( abcd_lpksad01.gender == "F" && male != false );
```

### Medications

The ABCD subjects take a wide variety of legal medications from acetaminophen to zonisamide.  Many of these medications are psychoactive and therefore of interest to brain scientists.  The ABCD data include:

* What prescription and over-the-counter medications each subject takes
* Whether each medication was taken in the last 24 hours

> Note that the subjects were asked if they took the medication within 24 hours of the `.interview_date`, which is not always the same day as the MRI scan.

Writing code by hand to test if a subject is taking a medication is tedious and error-prone.  This tedium will hopefully be alleviated in future versions of abcde through new language features such as macros and functions.  Until then, there is a separate tool [abcde-rxnorm](../../abcde-rxnorm/README.md) that can automatically generate such code for you.

**See the [abcde-rxnorm README](../../abcde-rxnorm/README.md) for more information.**
