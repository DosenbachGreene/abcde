//! Module for exploring files and queries.

use std::io::BufRead;
use crate::lang::ast::strip;

/// Metadata about a query.
#[derive(Debug, Clone)]
pub struct QueryInfo {
    /// File (without path or extension)
    pub file: String,
    /// Field
    pub field: String,
    /// Description
    pub desc: String,
    /// Column within its file, starting from zero
    pub col: usize,
    /// Additional information from the data structure dictionary
    pub dict_info: Option<DictInfo>,
    /// Sample data, simply parsed as strings
    pub samples: Vec<String>,
}
impl std::fmt::Display for QueryInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Query: {}.{}\n", self.file, self.field)?;
        write!(f, "Description:\n{}\n", self.desc)?;
        if let Some(ref dict_info) = self.dict_info {
            write!(f, "{}", dict_info)?;
        }
        if !self.samples.is_empty() {
            write!(f, "Sample values:\n")?;
        }
        for sample in &self.samples {
            write!(f, "{}\n", sample)?;
        }
        Ok(())
    }
}

/// Additional metadata about a query from the data structure dictionary.
#[derive(Debug, Clone)]
pub struct DictInfo {
    /// Description, typically the same as in `QueryInfo`
    pub desc: String,
    /// Range of possible values
    pub value_range: Option<String>,
    /// Additional notes about this query
    pub notes: Option<String>,
    /// Query fields that refer to the same data
    pub aliases: Option<String>, 
}
impl std::fmt::Display for DictInfo {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Dictionary:\n{}\n", self.desc)?;
        if let Some(ref notes) = self.notes {
            write!(f, "Notes:\n{}\n", notes)?;
        }
        if let Some(ref value_range) = self.value_range {
            write!(f, "Value range: {}\n", value_range)?;
        }
        if let Some(ref aliases) = self.aliases {
            write!(f, "Aliases: {}\n", aliases)?;
        }
        Ok(())
    }
}

/// Object for interrogating a dictionary file.
pub struct DictFile {
    /// Name of file portion of corresponding query (without path or extension)
    pub file: String,
    
    /// Entries in this file.  The key is the field name.
    queries: std::collections::BTreeMap<String, DictInfo>,
}
impl DictFile {
    /// Construct a new DictFile object from the data directory and file name
    /// (without path or extension).  Will read the entire dictionary file and
    /// parse it into a dictionary for which you can look up a DictInfo struct
    /// by field name.  Will automatically look in the dictionary subdirectory
    /// of the data directory for the dictionary files.
    pub fn new(data_dir: &std::path::Path, file: &str) -> Result<DictFile, std::io::Error> {
        // Generate the actual name of the dictionary file.
        let filename = {
            let mut filename = data_dir.to_path_buf();
            filename.push("dictionary");
            filename.push(file);
            filename.set_extension("txt");
            filename
        };
        
        // Read the file line by line.
        let handle = std::fs::File::open(&filename)?;
        let reader = std::io::BufReader::new(handle);
        let mut lines = reader.lines().enumerate();
        
        // Read the header line and figure out which value is in which column.
        // Unfortunately this is needed because dictionary files can contain a
        // variable number of columns.  We need to examine the header to figure
        // out which column is which.
        // 
        // Initialize column indices.
        let mut name_col: Option<usize> = None;
        let mut desc_col: Option<usize> = None;
        let mut range_col: Option<usize> = None;
        let mut notes_col: Option<usize> = None;
        let mut aliases_col: Option<usize> = None;
        // Read header line.
        let line = match lines.next() {
            Some((_, line)) => line?,
            None => return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Missing header line in dictionary file: {}", filename.display()))),
        };
        // Split line on quote delimters and filter out comma separators.
        let line = line.split('"').enumerate().filter_map(|(idx, val)| match idx % 2 == 0 { true => None, false => Some(((idx-1)/2, val)), });
        // Search for column indices of interest.
        for (col, val) in line {
            match val {
                "ElementName" => { name_col = Some(col); },
                "ElementDescription" => { desc_col = Some(col); },
                "ValueRange" => { range_col = Some(col); },
                "Notes" => { notes_col = Some(col); },
                "Aliases" => { aliases_col = Some(col); },
                _ => (), // ignore other columns
            }
        }
        // Unwrap mandatory column indices.
        let name_col = match name_col {
            Some(col) => col,
            None => return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, format!("Missing ElementName in header line in dictionary file: {}", filename.display()))),
        };
        let desc_col = match desc_col {
            Some(col) => col,
            None => return Err(std::io::Error::new(std::io::ErrorKind::InvalidData, format!("Missing ElementDescription in header line in dictionary file: {}", filename.display()))),
        };
        
        // Initialize Dict object.
        let mut this = DictFile{
            file: file.to_string(),
            queries: std::collections::BTreeMap::new(),
        };
        
        // Unfortunately, the NIH dictionary file format apparently allows
        // quoted newlines, e.g.
        //     "This is all one line\neven though there is a newline in it."
        // Therefore one "line" of data may actually be several lines in the
        // file.  As a workaround, we will assume that each complete line of
        // data ends with a double quote '"'.  We will merge physical lines in
        // the file that do not end with a double quote into one virtual line.
        for (linenum, line) in lines.scan(String::new(), |state, (n, s)| {
            // Line number in n counts from zero, but but
            // conventionally we count lines in a file from 1.
            // Therefore add one to n.
            let n = n + 1;
            // Pass through errors.
            match s {
                Ok(s) => {  
                    // Accumulate non-quote-terminated lines into a string.
                    state.push_str(&s);
                    // If the line is now terminated by a quote, return the line.
                    // Otherwise return Some(None).
                    match state.chars().last() {
                        Some(c) => {
                            if c == '"' {
                                // Reset state of scan iterator.
                                let ss = std::mem::take(state);
                                // Return line number and accumulated line.
                                Some(Some((n, Ok(ss))))
                            }
                            else { Some(None) }
                        },
                        None => Some(None)
                    }
                },
                Err(e) => Some(Some((n, Err(e)))),
            }
        })
        // Filter out the None lines.
        .filter_map(|line| line) {            
            // Split the line on the quote delimeters and filter out the
            // comma separators.
            let line = line?;
            let line: Vec<&str> = line.split('"').enumerate().filter_map(|(idx, val)| match idx % 2 == 0 { true => None, false => Some(val), }).collect();
            
            // Get each field out of the line.
            let field = match line.get(name_col) {
                Some(field) => field.to_string(),
                None => return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Missing field name in line {} of dictionary file: {}", linenum, filename.display()))),
            };
            let desc = match line.get(desc_col) {
                Some(desc) => desc.to_string(),
                None => return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Missing description in line {} of dictionary file: {}", linenum, filename.display()))),
            };
            let value_range = match range_col {
                Some(range_col) => match line.get(range_col) {
                    Some(value_range) => match value_range.is_empty() {
                        true => None,
                        false => Some(value_range.to_string()),
                    },
                    None => { eprintln!("range_col = {}, line = {:?}", range_col, line); 
                    return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Missing value range in line {} of dictionary file: {}", linenum, filename.display()))) },
                },
                None => None,
            };
            let notes = match notes_col {
                Some(notes_col) => match line.get(notes_col) {
                    Some(notes) => match notes.is_empty() {
                        true => None,
                        false => Some(notes.to_string()),
                    },
                    None => return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Missing notes in line {} of dictionary file: {}", linenum, filename.display()))),
                },
                None => None,
            };
            let aliases = match aliases_col {
                Some(aliases_col) => match line.get(aliases_col) {
                    Some(aliases) => match aliases.is_empty() {
                        true => None,
                        false => Some(aliases.to_string()),
                    },
                    None => return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Missing aliases in line {} of dictionary file: {}", linenum, filename.display()))),
                },
                None => None,
            };
            
            // Sometimes fields are mistakenly indexed in the dictionary under
            // their aliases.  If this field's alias isn't in the dictionary
            // then we'll go ahead and index a copy under the alias.
            if let Some(ref aliases) = aliases {
                for alias in aliases.split(',') {
                    this.queries.entry(alias.to_string()).or_insert(DictInfo{
                        desc: desc.clone(),
                        value_range: value_range.clone(),
                        notes: notes.clone(),
                        aliases: Some(field.clone())
                    });
                }
            }
            
            // Insert field into dictionary.
            this.queries.insert(field, DictInfo{desc, value_range, notes, aliases});
        }
        
        // Done
        Ok(this)
    }
    
    /// Look up a DictInfo struct by query field name and take it out of the
    /// dictionary.  Returns `None` if the field is not in the dictionary.
    pub fn take(&mut self, query: &str) -> Option<DictInfo> {
        self.queries.remove(query)
    }
    
    /// Look up a DictInfo struct by query field and return a reference to it.
    /// Returns `None` if the field is not in the dictionary.
    pub fn search<'a>(&'a self, query: &str) -> Option<&'a DictInfo> {
        self.queries.get(query)
    }
}

/// Object for interrogating a data file.
pub struct QueryFile {
    /// Name of file (without path or extension)
    pub file: String,
    
    /// Queries in this file.
    pub queries: Vec<QueryInfo>,
    
    /// Actual full name/path of file.
    data_dir: std::path::PathBuf,
    
    /// Buffered line reader for the file.
    lines: std::io::Lines<std::io::BufReader<std::fs::File>>,
}
impl QueryFile {
    /// Construct a new File object from the data directory and file name
    /// (without path or extension).
    pub fn new(data_dir: &std::path::Path, file: &str) -> Result<QueryFile, std::io::Error> {
        // Read the file line by line.
        let handle = std::fs::File::open(make_filename(&data_dir, file))?;
        let reader = std::io::BufReader::new(handle);
        
        // Initialize File object.
        let mut this = QueryFile{
            file: file.to_string(),
            queries: Vec::new(),
            data_dir: data_dir.to_path_buf(),
            lines: reader.lines(),
        };
        
        // Parse the first line in the file into field names and push each field
        // onto the vector of queries.
        let line = match this.lines.next() {
            None => return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Missing field header line in file: {}", make_filename(&data_dir, file).display()))),
            Some(line) => line?,
        };
        // Split on tab separator and strip quotes, which are always present.
        let field_names = line.split('\t').map(|s| strip(s)).enumerate();
        for (col, field) in field_names {
            this.queries.push(QueryInfo{
                file: file.to_string(), // TODO possibly change to an Rc since this involves a lot of cloning
                field: field.to_string(),
                desc: String::new(),
                col: col,
                dict_info: None,
                samples: Vec::new(),
            })
        }
        
        // Parse the sceond line in the file into field descriptions.
        let line = match this.lines.next() {
            None => return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Missing description header line in file: {}", make_filename(&data_dir, file).display()))),
            Some(line) => line?,
        };
        let mut descs = line.split('\t').map(|s| strip(s));
        for query in &mut this.queries.iter_mut() {
            match descs.next() {
                Some(desc) => { query.desc = desc.to_string(); },
                None => { return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Not enough columns in decription header line in file: {}", make_filename(&data_dir, file).display()))); },
            }
        }
        
        // Return constructed object.
        Ok(this)
    }
    
    /// Consume a file and take any queries whose field name or description
    /// matches a regular expression.  Push matching queries onto a vector.
    /// Also see `search()`.
    pub fn take_where_regex(self, output: &mut Vec<QueryInfo>, regex: &regex::Regex) {
        for query in self.queries.into_iter() {
            if regex.is_match(&query.field) || regex.is_match(&query.desc) {
                output.push(query);
            }
        }
    }
    
    /// Search a file for queries whose field name or description matches a
    /// regular expression and push matching queries onto a vector.
    /// Also see `take_where()`.
    pub fn search_regex<'a>(&'a self, output: &mut Vec<&'a QueryInfo>, regex: &regex::Regex) {
        for query in self.queries.iter() {
            if regex.is_match(&query.field) || regex.is_match(&query.desc) {
                output.push(&query);
            }
        }
    }
    
    /// Look up each field in this query file in the data structure dictionary.
    /// Populates QueryInfo.dict_info.
    pub fn dict_lookup(&mut self) -> Result<(), std::io::Error> {
        let mut dict = DictFile::new(&self.data_dir, &self.file)?;
        for query in self.queries.iter_mut() {
            query.dict_info = dict.take(&query.field);
        }
        Ok(())
    }
    
    /// Get `n` samples of data for each field in the file.
    pub fn read_samples(&mut self, n: usize) -> Result<(), std::io::Error> {
        for _ in 0..n {
            match self.lines.next() {
                None => return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Not enough samples in file: {}", make_filename(&self.data_dir, &self.file).display()))),
                Some(line) => self.add_sample(line?)?,
            };
        }
        Ok(())
    }
    
    /// Get up to `n` samples of data for each field in the file.  May actually
    /// get fewer than `n` samples.
    pub fn try_read_samples(&mut self, n: usize) -> Result<(), std::io::Error> {
        for _ in 0..n {
            match self.lines.next() {
                None => return Ok(()), // No more samples to read, return early.
                Some(line) => self.add_sample(line?)?,
            };
        }
        Ok(())
    }
    
    // Add a line of data samples to self. 
    fn add_sample(&mut self, line: String) -> Result<(), std::io::Error> {
        let mut samples = line.split('\t').map(|s| strip(s));
        for query in self.queries.iter_mut() {
            match samples.next() {
                Some(sample) => { query.samples.push(sample.to_string()); },
                None => { return Err(std::io::Error::new(std::io::ErrorKind::UnexpectedEof, format!("Not enough columns in line of file: {}", make_filename(&self.data_dir, &self.file).display()))); },
            }
        }
        Ok(())
    }
}

// Combine the data directory with the file of a query and the standard .txt
// extension to get the filename.
pub fn make_filename(data_dir: &std::path::Path, file: &str) -> std::path::PathBuf {
    let mut filename = data_dir.to_path_buf();
    filename.push(file);
    filename.set_extension("txt");
    filename
}
