//! Functions that perform actions for the generate subcommand.

// Import cmd module from crate.
use crate::cmd;

use std::io::Write;

/// Perform the generate action/command described by `opts`.
/// Uses an asynchronous event model driven by tokio.
#[tokio::main]
pub async fn generate(opts: cmd::opts::GenerateOpts) -> Result<(), cmd::BoxError> {
    // If we were given the drug by name then get the associated RXCUI.
    if opts.generate.rxcuis.is_empty() {
        opts.generate
            .rxcuis
            .push(rxcui_from_drug_name(&opts).await?);
    }

    // Initialize empty list of expanded rxcuis.
    let mut expanded_rxcuis = std::collections::HashMap::new();

    // If the exact flag was passed then we just need to generate a capture for
    // the drugs we were given.  Otherwise we will expand on the list by looking
    // by up the active ingredients and adding all related RXCUIs.
    if opts.generate.exact {
        // Just add the provided drugs to the expanded set.
        for rxcui in &opts.generate.rxcuis {
            expanded_rxcuis.insert(*rxcui, name_of_rxcui(&opts, *rxcui).await?);
        }
    } else {
        // Expand each provided RXCUI into related RXCUIs and add them to the set.
        for rxcui in &opts.generate.rxcuis {
            expanded_rxcuis.extend(expand_rxcui(&opts, *rxcui).await?);
        }
    }

    // Generate the capture code.
    let code: (Vec<_>, Vec<_>) = expanded_rxcuis.into_iter().map( |(rxcui, name)| {
        let pairs: (Vec<_>, Vec<_>) = (1..=15).map( |i|
            match opts.common.release {
                cmd::opts::Release::Two => (
                    format!("( if medsy01.medinv_plus_rxnorm_med{}_p == None || medsy01.medinv_plus_rxnorm_med{}_p != \"{} {}\" {{ false }} else {{ true }} ) || ( if medsy01.medinv_plus_otc_med{}_rxnorm_p == None || medsy01.medinv_plus_otc_med{}_rxnorm_p != \"{} {}\" {{ false }} else {{ true }} )", i, i, rxcui, name, i, i, rxcui, name),
                    format!("( if medsy01.medinv_plus_rxnorm_med{}_p == None || medsy01.medinv_plus_rxnorm_med{}_p != \"{} {}\" {{ false }} else {{ medsy01.medinv_plus_rx_med{}_24_p as bool }} ) || ( if medsy01.medinv_plus_otc_med{}_rxnorm_p == None || medsy01.medinv_plus_otc_med{}_rxnorm_p != \"{} {}\" {{ false }} else {{ medsy01.medinv_plus_otc_med{}_24_p as bool }} )", i, i, rxcui, name, i, i, i, rxcui, name, i)
                ),
                cmd::opts::Release::Three => (
                    format!("( if medsy01.med{}_rxnorm_p == None || medsy01.med{}_rxnorm_p != \"{}\" {{ false }} else {{ true }} ) || ( if medsy01.med_otc_{}_rxnorm_p == None || medsy01.med_otc_{}_rxnorm_p != \"{}\" {{ false }} else {{ true }} )", i, i, rxcui, i, i, rxcui),
                    format!("( if medsy01.med{}_rxnorm_p == None || medsy01.med{}_rxnorm_p != \"{}\" {{ false }} else {{ medsy01.rx_med{}_24 as bool }} ) || ( if medsy01.med_otc_{}_rxnorm_p == None || medsy01.med_otc_{}_rxnorm_p != \"{}\" {{ false }} else {{ medsy01.otc_med{}_24 as bool }} )", i, i, rxcui, i, i, i, rxcui, i)
                ),
            }
        ).unzip();
        (pairs.0.join(" || "), pairs.1.join(" || "))
    }).unzip();
    let code = (code.0.join(" || "), code.1.join(" || "));
    let code = format!("// true if taking, false otherwise\nlet {} = {};\n// true if taken in last 24 hours,\n// false if not taken in last 24 hours,\n// None if no response\nlet {}_24 = {};",
        opts.generate.var_name, code.0,
        opts.generate.var_name, code.1);

    // Write capture code to stdout.
    println!("{}", code);

    Ok(())
}

async fn rxcui_from_drug_name(opts: &cmd::opts::GenerateOpts<'_>) -> Result<u64, cmd::BoxError> {
    match opts.generate.drug_name {
        Some(ref drug_name) => {
            // Formulate API query.
            let url = format!("https://rxnav.nlm.nih.gov/REST/rxcui?name={}", drug_name);

            if opts.common.verbose {
                eprint!("Looking up RXCUI of {} -> {} ...", drug_name, &url);
                std::io::stderr().lock().flush()?;
            }

            // Perform API query.
            let response =
                // Send API request to server and await a response.
                reqwest::get(&url).await?
                // Generate an error if server did not respond OK.
                .error_for_status()?
                // Await download of response body and decode as text.
                .text().await?;

            if opts.common.verbose {
                eprint!(" done.");
            }

            // Deserialize response.
            let response: xml::IdGroupResponse = match quick_xml::de::from_str(&response) {
                Ok(resp) => resp,
                Err(e) => {
                    return Err(cmd::Error {
                        what: format!("Couldn't parse XML response from: {}\n{}", url, e),
                        source: None,
                    }
                    .into())
                }
            };

            if opts.common.verbose {
                eprintln!(" Got RXCUI {}.", response.id_group.rxnorm_id);
            }

            Ok(response.id_group.rxnorm_id)
        }
        None => Err(cmd::Error {
            what: "No drug specified.".to_string(),
            source: None,
        }
        .into()),
    }
}

// Expand an RXCUI into a vector of related RXCUIs and their corresponding names
// by optionally looking up its active ingredient and then searching for all
// RXCUIs related to that ingredient.
async fn expand_rxcui(
    opts: &cmd::opts::GenerateOpts<'_>,
    rxcui: u64,
) -> Result<Vec<(u64, String)>, cmd::BoxError> {
    // Look up the active ingredient unless the user asked us not to.
    let rxcui = match opts.generate.is_ingredients {
        true => rxcui,
        false => ingredient_rxcui(opts, rxcui).await?,
    };

    // Search for relatex rxcuis.
    related_rxcuis(opts, rxcui).await
}

// Get a vector of RXCUIs and their corresponding names related to the RXCUI of an ingredient.
async fn related_rxcuis(
    opts: &cmd::opts::GenerateOpts<'_>,
    rxcui: u64,
) -> Result<Vec<(u64, String)>, cmd::BoxError> {
    // Initialize empty vector of related rxcuis.
    let mut rxcuis = Vec::new();

    // Formulate query depending on whether the provided RXCUI is a single or
    // multiple ingredient.
    let url = match ingredient_is_multiple(opts, rxcui).await? {
        true => format!("https://rxnav.nlm.nih.gov/REST/rxcui/{}/related?tty=BN+BPCK+GPCK+MIN+PIN+SBD+SBDC+SBDF+SBDG+SCD+SCDF+SCDG", rxcui),
        false => format!("https://rxnav.nlm.nih.gov/REST/rxcui/{}/related?tty=BN+BPCK+GPCK+IN+MIN+PIN+SBD+SBDC+SBDF+SBDG+SCD+SCDC+SCDF+SCDG", rxcui),
    };

    if opts.common.verbose {
        eprint!("Looking up related RXCUIs of {} -> {} ...", rxcui, &url);
        std::io::stderr().lock().flush()?;
    }

    // Perform API query.
    let response =
        // Send API request to server and await a response.
        reqwest::get(&url).await?
        // Generate an error if server did not respond OK.
        .error_for_status()?
        // Await download of response body and decode as text.
        .text().await?;

    if opts.common.verbose {
        eprint!(" done.");
    }

    // Deserialize response.
    let response: xml::RelatedGroupResponse = match quick_xml::de::from_str(&response) {
        Ok(resp) => resp,
        Err(e) => {
            return Err(cmd::Error {
                what: format!("Couldn't parse XML response from: {}\n{}", url, e),
                source: None,
            }
            .into())
        }
    };

    // Traverse the response and push each RXCUI onto the vector.
    for concept_group in response.related_group.concept_group {
        for concept_properties in concept_group.concept_properties {
            rxcuis.push((concept_properties.rxcui, concept_properties.name));
        }
    }

    if opts.common.verbose {
        eprintln!(" Found {} related RXCUIs.", rxcuis.len());
    }

    Ok(rxcuis)
}

// Get the active ingredient of an RXCUI.
// If the give RXCUI has more than one ingredient this will return an error.
async fn ingredient_rxcui(
    opts: &cmd::opts::GenerateOpts<'_>,
    rxcui: u64,
) -> Result<u64, cmd::BoxError> {
    // Formulate API query.
    let url = format!(
        "https://rxnav.nlm.nih.gov/REST/rxcui/{}/related?tty=IN",
        rxcui
    );

    if opts.common.verbose {
        eprint!("Looking up ingredient of {} -> {} ...", rxcui, &url);
        std::io::stderr().lock().flush()?;
    }

    // Perform API query.
    let response =
        // Send API request to server and await a response.
        reqwest::get(&url).await?
        // Generate an error if server did not respond OK.
        .error_for_status()?
        // Await download of response body and decode as text.
        .text().await?;

    if opts.common.verbose {
        eprint!(" done.");
    }

    // Deserialize response.
    let response: xml::RelatedGroupResponse = match quick_xml::de::from_str(&response) {
        Ok(resp) => resp,
        Err(e) => {
            return Err(cmd::Error {
                what: format!("Couldn't parse XML response from: {}\n{}", url, e),
                source: None,
            }
            .into())
        }
    };

    // Get the RXCUI of the one single ingredient associated with this drug.
    // If the drug has multiple ingredients then return an error.
    let rxcui = if response.related_group.concept_group.len() > 1 {
        return Err(cmd::Error{
            what: format!("RXCUI {} has multiple ingredients.  Looking up the ingredient for combination drugs is not supported.  Try running with --ingredients or --exact.", rxcui),
            source: None,
        }.into());
    } else {
        match response.related_group.concept_group.into_iter().next() {
            None => return Err(cmd::Error{
                what: format!("RxNorm did not return a conceptGroup when searching for ingredient of RXCUI {}.", rxcui),
                source: None,
            }.into()),
            Some(concept_group) => {
                if concept_group.concept_properties.len() > 1 {
                    return Err(cmd::Error{
                        what: format!("RXCUI {} has multiple ingredients.  Looking up the ingredient for combination drugs is not supported.  Try running with --exact.", rxcui),
                        source: None,
                    }.into());
                }
                else {
                    match concept_group.concept_properties.into_iter().next() {
                        None => return Err(cmd::Error{
                            what: format!("RxNorm did not return a conceptProperties when searching for ingredient of RXCUI {}.", rxcui),
                            source: None,
                        }.into()),
                        Some(concept_properties) => {
                            // Make sure this concept property is the active ingredient.
                            if concept_properties.tty != "IN" {
                                return Err(cmd::Error{
                                    what: format!("RxNorm did not return an ingredient when looking up ingredient of RXCUI {}.  Expected <tty>IN</tty>, got <tty>{}</tty>", rxcui, concept_properties.tty),
                                    source: None,
                                }.into());
                            }
                            else {
                                // This is the RXCUI of the active ingredient.
                                concept_properties.rxcui
                            }
                        }
                    }
                }
            },
        }
    };

    if opts.common.verbose {
        eprintln!(" Got RXCUI {}.", rxcui);
    }

    Ok(rxcui)
}

// Returns true if the provided RXCUI is a multiple ingredient (MIN).
// Returns false if the provided RXCUI is a single ingredient (IN).
// Returns an error if the provided RXCUI is not an ingredient.
async fn ingredient_is_multiple(
    opts: &cmd::opts::GenerateOpts<'_>,
    rxcui: u64,
) -> Result<bool, cmd::BoxError> {
    // Formulate API query.
    let url = format!(
        "https://rxnav.nlm.nih.gov/REST/rxcui/{}/property?propName=tty",
        rxcui
    );

    if opts.common.verbose {
        eprint!("Getting TTY property of RXCUI {} -> {} ...", rxcui, &url);
        std::io::stderr().lock().flush()?;
    }

    // Perform API query.
    let response =
        // Send API request to server and await a response.
        reqwest::get(&url).await?
        // Generate an error if server did not respond OK.
        .error_for_status()?
        // Await download of response body and decode as text.
        .text().await?;

    if opts.common.verbose {
        eprint!(" done.");
    }

    // Deserialize response.
    let response: xml::PropConceptGroupResponse = match quick_xml::de::from_str(&response) {
        Ok(resp) => resp,
        Err(e) => {
            return Err(cmd::Error {
                what: format!("Couldn't parse XML response from: {}\n{}", url, e),
                source: None,
            }
            .into())
        }
    };

    if opts.common.verbose {
        eprintln!(" {}", response.prop_concept_group.prop_concept.prop_value);
    }

    match response.prop_concept_group.prop_concept.prop_value.as_str() {
        "MIN" => Ok(true),
        "IN" => Ok(false),
        prop_value => {
            return Err(cmd::Error {
                what: format!(
                    "RXCUI {} has TTY property {} and is not an ingredient.",
                    rxcui, prop_value
                ),
                source: None,
            }
            .into())
        }
    }
}

// Get the name associated with an RXCUI.
async fn name_of_rxcui(
    opts: &cmd::opts::GenerateOpts<'_>,
    rxcui: u64,
) -> Result<String, cmd::BoxError> {
    // Formulate API query.
    let url = format!("https://rxnav.nlm.nih.gov/REST/rxcui/{}/properties", rxcui);

    if opts.common.verbose {
        eprint!("Getting name of RXCUI {} -> {} ...", rxcui, &url);
        std::io::stderr().lock().flush()?;
    }

    // Perform API query.
    let response =
        // Send API request to server and await a response.
        reqwest::get(&url).await?
        // Generate an error if server did not respond OK.
        .error_for_status()?
        // Await download of response body and decode as text.
        .text().await?;

    if opts.common.verbose {
        eprint!(" done.");
    }

    // Deserialize response.
    let response: xml::PropertiesResponse = match quick_xml::de::from_str(&response) {
        Ok(resp) => resp,
        Err(e) => {
            return Err(cmd::Error {
                what: format!("Couldn't parse XML response from: {}\n{}", url, e),
                source: None,
            }
            .into())
        }
    };

    if opts.common.verbose {
        eprintln!(" {}", response.properties.name);
    }

    Ok(response.properties.name)
}

mod xml {
    //! Structures for deserializing xml with serde.

    // Use serde for deserializing xml.
    use serde::Deserialize;

    // Deserialize bool from Y/N string.
    fn bool_from_yn<'d, D>(deserializer: D) -> Result<bool, D::Error>
    where
        D: serde::Deserializer<'d>,
    {
        match String::deserialize(deserializer)?.as_ref() {
            "Y" => Ok(true),
            "N" => Ok(false),
            other => Err(serde::de::Error::invalid_value(
                serde::de::Unexpected::Str(other),
                &"Y or N",
            )),
        }
    }

    #[derive(Debug, Deserialize)]
    pub struct IdGroup {
        #[serde(rename = "rxnormId")]
        pub rxnorm_id: u64,
    }

    // Typical response will look something like this:
    // <rxnormdata>
    //     <idGroup>
    //         <rxnormId>12345</rxnormId>
    //     </idGroup>
    // </rxnormdata>
    #[derive(Debug, Deserialize)]
    #[serde(rename = "rxnormdata")]
    pub struct IdGroupResponse {
        #[serde(rename = "idGroup")]
        pub id_group: IdGroup,
    }

    #[derive(Debug, Deserialize)]
    pub struct ConceptProperties {
        pub rxcui: u64,
        pub name: String,
        // synonym: serde::de::IgnoredAny, // TODO remove ignored member
        pub tty: String,
        // language: serde::de::IgnoredAny, // TODO remove ignored member
        #[serde(deserialize_with = "bool_from_yn")]
        pub suppress: bool,
        // umlscui: serde::de::IgnoredAny, // TODO remove ignored member
    }

    #[derive(Debug, Deserialize)]
    pub struct ConceptGroup {
        pub tty: String,
        #[serde(rename = "conceptProperties", default)]
        pub concept_properties: Vec<ConceptProperties>,
    }

    #[derive(Debug, Deserialize)]
    pub struct RelatedGroup {
        #[serde(rename = "conceptGroup", default)]
        pub concept_group: Vec<ConceptGroup>,
    }

    // Typical response will look something like this:
    // <rxnormdata>
    //     <relatedGroup>
    //         <rxcui />
    //         <conceptGroup>
    //             <tty>IN</tty>
    //             <conceptProperties>
    //                 <rxcui>29046</rxcui>
    //                 <name>Lisinopril</name>
    //                 <synonym/>
    //                 <tty>IN</tty>
    //                 <language>ENG</language>
    //                 <suppress>N</suppress>
    //                 <umlscui>C0065374</umlscui>
    //             </conceptProperties>
    //         </conceptGroup>
    //     </relatedGroup>
    // </rxnormdata>
    #[derive(Debug, Deserialize)]
    #[serde(rename = "rxnormdata")]
    pub struct RelatedGroupResponse {
        #[serde(rename = "relatedGroup")]
        pub related_group: RelatedGroup,
    }

    #[derive(Debug, Deserialize)]
    pub struct PropConcept {
        #[serde(rename = "propCategory")]
        pub prop_category: String,
        #[serde(rename = "propName")]
        pub prop_name: String,
        #[serde(rename = "propValue")]
        pub prop_value: String,
    }

    #[derive(Debug, Deserialize)]
    pub struct PropConceptGroup {
        #[serde(rename = "propConcept")]
        pub prop_concept: PropConcept,
    }

    // Typical response will look something like this:
    // <rxnormdata>
    //     <propConceptGroup>
    //         <propConcept>
    //             <propCategory>ATTRIBUTES</propCategory>
    //             <propName>TTY</propName>
    //             <propValue>MIN</propValue>
    //         </propConcept>
    //     </propConceptGroup>
    // </rxnormdata>
    #[derive(Debug, Deserialize)]
    #[serde(rename = "rxnormdata")]
    pub struct PropConceptGroupResponse {
        #[serde(rename = "propConceptGroup")]
        pub prop_concept_group: PropConceptGroup,
    }

    // Typical response will look something liks this:
    // <rxnormdata>
    //     <properties>
    //     <rxcui>50121</rxcui>
    //         <name>Fluticasone propionate</name>
    //         <synonym/>
    //         <tty>PIN</tty>
    //         <language>ENG</language>
    //         <suppress>N</suppress>
    //         <umlscui>C0117996</umlscui>
    //     </properties>
    // </rxnormdata>
    #[derive(Debug, Deserialize)]
    #[serde(rename = "rxnormdata")]
    pub struct PropertiesResponse {
        pub properties: ConceptProperties,
    }
}
