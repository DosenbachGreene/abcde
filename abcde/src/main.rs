use std::io::Read;
use abcde::*;

// Split additional command-line-binary-specific code out into additional
// modules/files. 
mod cmd;

// Increase default stack size to 1 GB
// Needed for processing of ineffecient queries by abcde-rxnorm
const STACK_SIZE: usize = 1 * 1024 * 1024 * 1024;

fn main() -> Result<(), BoxError> {
    match std::thread::Builder::new()
        // spawning thread allows us to change stack size at runtime
        .stack_size(STACK_SIZE)
        .spawn(try_main)?
        .join()
	.expect("Failed to join thread") {
        Err(e) => {
            if let Some(e) = e.downcast_ref::<cmd::opts::OptsError>() {
				eprintln!("{}", e);
			}
			else if let Some(e) = e.downcast_ref::<std::io::Error>() {
			    eprintln!("ABCDE encountered an error and had to exit.\n{:?}", e.kind());
			    if let Some(e) = e.get_ref() {
			        eprintln!("{}", e);
			    } 
			}
            else {
                eprintln!("ABCDE encountered an error and had to exit.\n{}", e);
            }
            std::process::exit(1)
        },
        Ok(_) => Ok(()),
    }
}

fn try_main() -> Result<(), BoxError> {
    // Parse command line arguments.
    let cmd_opts = cmd::opts::Opts::from_args()?;
    
    // Make sure the data directory exists.
    if !cmd_opts.common.data_dir.exists() {
        return Err(cmd::Error{
            what: format!("Data directory does not exist: {}", cmd_opts.common.data_dir.display()),
            source: None,
        }.into());
    }
    
    // Branch execution based on subcommand.
    match cmd_opts.cmd {
        cmd::opts::OptCmd::Explore(explore_opts) => cmd::actions::explore(cmd::opts::ExploreOpts{
            common: &cmd_opts.common,
            explore: &explore_opts,
        }),
        cmd::opts::OptCmd::Capture(capture_opts) => cmd::actions::capture(cmd::opts::CaptureOpts{
            common: &cmd_opts.common,
            capture: &capture_opts
        }),
        cmd::opts::OptCmd::Download(mut download_opts) => cmd::actions::download(cmd::opts::DownloadOpts{
            common: &cmd_opts.common,
            download: &mut download_opts,
        }),
    }
}
