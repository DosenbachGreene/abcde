use super::*;

pub struct Types<'a> {
    pub queries: &'a BTreeMap<ast::Query, usize>,
    pub vars: &'a HashMap<ast::Variable, usize>,
    pub types: &'a Vec<Option<ast::Type>>,
}
impl<'a> Types<'a> {
    // Evaluate the type of an expression.
    pub fn evaluate_expr_type(&self, expr: &ast::Expr) -> Result<ast::Type, TypeError> {
        match expr {
            // Recursively descend into expressions containing other expressions.
            ast::Expr::UnaryOp(opcode, operand) => {
                let kind = self.evaluate_expr_type(operand)?;
                evaluate_type_of_unary_operation(*opcode, kind)
            },
            ast::Expr::BinaryOp(lhs, opcode, rhs) => {
                let rhs_kind = self.evaluate_expr_type(rhs)?;
                let lhs_kind = self.evaluate_expr_type(lhs)?;
                evaluate_type_of_binary_operation(lhs_kind, *opcode, rhs_kind)
            },
            ast::Expr::If(i) => self.evaluate_if_type(i),
            ast::Expr::Function(_) => unimplemented!(),

            // Directly deduce types of terminal expressions.
            ast::Expr::Symbol(symbol) => match symbol.name {
                ast::SymbolName::Query(ref query) => {
                    let kind = self.query_type(query);
                    match kind {
                        Some(kind) => Ok(kind),
                        None => Err(TypeError{
                            what: format!("Type of query {} could not be inferred.", query),
                            expr: ast::ExprKind::Symbol(ast::SymbolNameKind::Query),
                            kinds: vec![],
                            hint: Some(format!("Try specifying the type using \"{} as type\".", query))
                        }.into()),
                    }
                },
                ast::SymbolName::Variable(ref var) => {
                    let error = TypeError {
                        what: format!("Undefined variable {}.", var),
                        expr: ast::ExprKind::Symbol(ast::SymbolNameKind::Variable),
                        kinds: vec![],
                        hint: Some(format!("Try defining the variable using \"let {} = expression;\".", var))
                    };
                    match self.vars.get(var) {
                        Some(idx) => match self.types[*idx] {
                            Some(kind) => Ok(kind),
                            None => Err(error),
                        },
                        None => Err(error),
                    }
                },
            }
            ast::Expr::Value(value) => Ok(value.kind()),
        }
    }

    // Evaluate the type to which the expression of an if expression evaluates.
    // Also evaluate types of predicate expression for validity.
    // Recurses into alternative if expressions.
    fn evaluate_if_type(&self, i: &ast::If) -> Result<ast::Type, TypeError> {
        // Make sure predicate expression is boolean.
        if let Some(ref predicate) = i.predicate {
            let kind = self.evaluate_expr_type(predicate)?;
            if kind != ast::Type::Bool {
                return Err(TypeError{
                    what: "If expression requires a boolean predicate.".into(),
                    expr: ast::ExprKind::If,
                    kinds: vec![kind],
                    hint: None,
                });
            }
        }

        // Make sure type of this expression is equal to
        // type of alternative branch.  However, if one of the branches returns
        // a NoneLiteral then assume the type of the other branch.
        let kind = self.evaluate_expr_type(&i.expression)?;
        let kind = if let Some(ref alternative) = i.alternative {
            let alt_kind = self.evaluate_if_type(alternative)?;
            if kind == ast::Type::NoneLiteral { alt_kind }
            else if alt_kind == ast::Type::NoneLiteral { kind }
            else if kind != alt_kind {
                return Err(TypeError{
                    what: "Branches of if expression should have the same type.".into(),
                    expr: ast::ExprKind::If,
                    kinds: vec![kind, alt_kind],
                    hint: None,
                })
            }
            else { kind }
        }
        else { kind };

        // Return type of expression.
        Ok(kind)
    }
    // Look up the type of a query.
    pub fn query_type(&self, query: &ast::Query) -> Option<ast::Type> {
        if query.is_special() {
            query.special_type()
        }
        else {
            self.types[self.queries[query]]
        }
    }

    // Number of queries for which the type is unknown.
    pub fn num_queries_missing_types(&self) -> usize {
        let mut missing: usize = 0;
        for (_, index) in &*self.queries {
            if self.types[*index].is_none() {
                missing += 1;
            }
        }
        missing
    }
}

pub struct ModifyTypes<'a> {
    pub queries: &'a mut BTreeMap<ast::Query, usize>,
    pub vars: &'a mut HashMap<ast::Variable, usize>,
    pub types: &'a mut Vec<Option<ast::Type>>,
}
impl<'a> ModifyTypes<'a> {
    // Project to a Types object.
    pub fn as_types(&self) -> Types {
        Types {
            queries: self.queries,
            vars: self.vars,
            types: self.types,
        }
    }

    // Reset the type of each variable to None.
    // Does not affect query types.
    pub fn reset_vars(&mut self) {
        for (_, index) in &*self.vars {
            self.types[*index] = None;
        }
    }

    // Add a query to the `queries` map and `types` vector.
    pub fn add_query(&mut self, query: &ast::Query, kind: Option<ast::Type>) -> Result<(), InterpreterError> {
        // Infer the type/kind of this query if it is a
        // special query like `.subjectkey`, otherwise
        // get the type explicitly from the code.
        let kind: Option<ast::Type> = match query.file {
            Some(_) => match query.field.as_str() {
                // Forbid using special query with a file.
                "subjectkey" | "interview_date" | "eventname" => return Err(QueryError{
                    what: "Special queries cannot reference a table.".into(),
                    file: query.file.clone(),
                    field: Some(query.field.clone()),
                    hint: Some(format!("Did you mean .{}?", query.field)),
                }.into()),
                _ => kind,
            },
            None => match query.field.as_str() {
                // Make sure this is a special query.  If it is then we actually
                // do not need to (and should not) add it to the type vector, so
                // do nothing.  If it is not a special query then generate an
                // error because the file of the query is missing!
                "subjectkey" | "interview_date" | "eventname" => return Ok(()), // TAG1
                _ => return Err(QueryError{
                    what: "Special query not recognized.".into(),
                    file: query.file.clone(),
                    field: Some(query.field.clone()),
                    hint: Some(format!("Did you mean to specify a table to query with table.{}?", query.field)),
                }.into()),
            },
        };

        // See if the query is already in the map.
        match self.queries.entry(query.clone()) {
            // Not in map.
            std::collections::btree_map::Entry::Vacant(entry) => {
                // Insert the query's vector index into the map.
                entry.insert(self.types.len());

                // Push the type onto the type vector.
                self.types.push(kind);
            }
            // Already in the map.
            std::collections::btree_map::Entry::Occupied(entry) => {
                // Update/check consistency of type.
                if let Some(kind) = kind {
                    let stored_kind = &mut self.types[*(entry.get())];
                    match stored_kind {
                        Some(stored_kind) => {
                            // If type already known, detect conflicting types.
                            let stored_kind = *stored_kind;
                            if kind != stored_kind {
                                return Err(TypeError{
                                    what: format!("Query {} defined with inconsistent types.", query),
                                    expr: ast::ExprKind::Symbol(ast::SymbolNameKind::Query),
                                    kinds: vec![stored_kind, kind],
                                    hint: None,
                                }.into())
                            }
                        }
                        None => {
                            // Store the type for a previously un-typed query.
                            *stored_kind = Some(kind);
                        }
                    }
                }
            }
        }
        Ok(())
    }

    // Checks if a query that is already in the `queries` map and `types` vector
    // has a known type.  If the type was not previously known, update the
    // `types` vector with the proposed type.  If the type was already known
    // then discard the proposed type and do nothing.
    //
    // Assumes query has already been added with `add_query()`.
    // Panics if the query has not been added.
    pub fn update_query_propose_type(&mut self, query: &ast::Query, kind: Option<ast::Type>) {
        // Don't try to update special queries since their type is already known
        // and also is not stored in the queries vector, which means attempting
        // to update them would cause a panic.
        match query.field.as_str() {
            "subjectkey" | "interview_date" | "eventname" => (), // skip special queries
            _ => { // perform update for all other queries
                let idx = self.queries[query];
                let stored_kind = &mut self.types[idx];
                if stored_kind.is_none() {
                       *stored_kind = kind;
                }
            },
        }
    }

    // Walk an AST of expressions call add_query on each visited query.
    pub fn visit_expr_queries_add(&mut self, expr: &ast::Expr) -> Result<(), InterpreterError> {
        match expr {
            // Recursively descend into expressions containing
            // other expressions.
            ast::Expr::BinaryOp(lhs, _, rhs) => {
                self.visit_expr_queries_add(lhs)?;
                self.visit_expr_queries_add(rhs)
            },
            ast::Expr::UnaryOp(_, expr) => self.visit_expr_queries_add(expr),
            ast::Expr::If(i) => self.visit_if_queries_add(i),
            ast::Expr::Function(_) => unimplemented!(),

            // Return on terminal expressions.
            ast::Expr::Symbol(symbol) => match symbol.name {
                // Evaluate the callback on the query.
                ast::SymbolName::Query(ref query) => self.add_query(query, symbol.kind),
                ast::SymbolName::Variable(_) => Ok(()), // ignore
            }
            ast::Expr::Value(_) => Ok(()), // ignore
        }
    }

    // Walk an AST of expressions and try to infer the type of each query.
    // Takes an optional argument `kind` which is the kind of the expression,
    // which may be known or inferred even if we do not yet know the kind of
    // each sub-expression.
    // Call update_query_propose_type() when the type of a query is inferred.
    pub fn visit_expr_queries_update(&mut self, expr: &ast::Expr, kind: Option<ast::Type>) {
        let types = self.as_types();
        match expr {
            // Recursively descend into expressions containing
            // other expressions.
            ast::Expr::BinaryOp(lhs, opcode, rhs) => {
                let opcode = *opcode;

                // Try to infer the types of lhs and rhs.
                let lhs_kind = match types.evaluate_expr_type(rhs) {
                    Ok(rhs_kind) => infer_type_from_binary_operation(opcode, rhs_kind, kind),
                    Err(_) => infer_type_from_operator(opcode, kind),
                };
                let rhs_kind = match types.evaluate_expr_type(lhs) {
                    Ok(lhs_kind) => infer_type_from_binary_operation(opcode, lhs_kind, kind),
                    Err(_) => infer_type_from_operator(opcode, kind),
                };

                // Update types of lhs/rhs if they are queries, otherwise
                // recursively descend into sub-expressions.
                if let ast::Expr::Symbol(ref symbol) = lhs.as_ref() {
                    if let ast::SymbolName::Query(ref query) = symbol.name {
                        self.update_query_propose_type(query, lhs_kind);
                    }
                    else { self.visit_expr_queries_update(lhs, lhs_kind); }
                }
                else { self.visit_expr_queries_update(lhs, lhs_kind); }
                if let ast::Expr::Symbol(ref symbol) = rhs.as_ref() {
                    if let ast::SymbolName::Query(ref query) = symbol.name {
                        self.update_query_propose_type(query, rhs_kind);
                    }
                    else { self.visit_expr_queries_update(rhs, rhs_kind); }
                }
                else { self.visit_expr_queries_update(rhs, rhs_kind); }
            },
            ast::Expr::UnaryOp(opcode, expr) => {
                // Try to figure out the type of the sub-expression.
                let kind = infer_type_from_operator(*opcode, kind);

                // Update type if expression is a query, otherwise recursively
                // descend into sub-expression.
                if let ast::Expr::Symbol(ref symbol) = expr.as_ref() {
                    if let ast::SymbolName::Query(ref query) = symbol.name {
                        self.update_query_propose_type(query, kind);
                    }
                    else { self.visit_expr_queries_update(expr, kind); }
                }
                else { self.visit_expr_queries_update(expr, kind); }
            }
            ast::Expr::If(i) => self.visit_if_queries_update(i, kind),
            ast::Expr::Function(_) => unimplemented!(),

            // We can only infer types from non-terminal expressions, so if
            // we've made it to a terminal expression just return.
            ast::Expr::Symbol(_) => (),
            ast::Expr::Value(_) => (),
        }
    }

    // Walk the AST of an if expression and call `visit_expr_queries_add()` on
    // each visited expression.
    fn visit_if_queries_add(
        &mut self,
        i: &ast::If,
        //f: &mut dyn FnMut(&ast::Query) -> Result<(), InterpreterError>
    ) -> Result<(), InterpreterError> {
        if let Some(ref expr) = i.predicate {
            self.visit_expr_queries_add(expr)?;
        }

        self.visit_expr_queries_add(&i.expression)?;

        if let Some(ref ii) = i.alternative {
            self.visit_if_queries_add(ii)?;
        }

        Ok(())
    }

    // Walk the AST of an if expression, try to infer query type, and call
    // update_query_propose_type() accordingly.
    fn visit_if_queries_update(&mut self, i: &ast::If, kind: Option<ast::Type>) {
        if let Some(ref expr) = i.predicate {
            // Predicate must have boolean type.
            if let ast::Expr::Symbol(ref symbol) = expr.as_ref() {
                if let ast::SymbolName::Query(ref query) = symbol.name {
                    self.update_query_propose_type(query, Some(ast::Type::Bool));
                }
                else {
                    self.visit_expr_queries_update(expr, Some(ast::Type::Bool));
                }
            }
            else {
                self.visit_expr_queries_update(expr, Some(ast::Type::Bool));
            }
        }

        self.visit_expr_queries_update(&i.expression, kind);

        if let Some(ref ii) = i.alternative {
            self.visit_if_queries_update(ii, kind);
        }
    }

    // Called by constructor to update `vars` map and `types` vector with a
    // let statement from the code.
    pub fn update_vars(&mut self, var: &ast::Variable, kind: ast::Type) {
        match self.vars.entry(var.clone()) {
            std::collections::hash_map::Entry::Vacant(entry) => {
                // Insert the variables's vector index into the map.
                entry.insert(self.types.len());

                // Push the type onto the type vector.
                self.types.push(Some(kind));
            }
            std::collections::hash_map::Entry::Occupied(entry) => {
                // Update the type in the type vector.
                self.types[*entry.get()] = Some(kind);
            }
        }
    }
}

fn evaluate_type_of_unary_operation(opcode: ast::Opcode, kind: ast::Type) -> Result<ast::Type, TypeError> {
    match opcode {
        ast::Opcode::NOT => match kind {
            ast::Type::Bool =>
                Ok(ast::Type::Bool),
            ast::Type::U32 |
            ast::Type::I32 |
            ast::Type::F32 |
            ast::Type::String |
            ast::Type::Date |
            ast::Type::NoneLiteral =>
                Err(TypeError{what: "type is not boolean".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![kind], hint: None}),
        },
        ast::Opcode::AND |ast::Opcode::OR | ast::Opcode::EQ | ast::Opcode::NE |
        ast::Opcode::LT | ast::Opcode::GT | ast::Opcode::LE | ast::Opcode::GE |
        ast::Opcode::PLUS | ast::Opcode::MINUS |
        ast::Opcode::MUL | ast::Opcode::DIV
            => panic!("Called evaluate_type_of_unary_operation with binary operator {}.", opcode),
    }
}

fn evaluate_type_of_binary_operation(lhs: ast::Type, opcode: ast::Opcode, rhs: ast::Type) -> Result<ast::Type, TypeError> {
    match opcode {
        ast::Opcode::AND | ast::Opcode::OR => match (lhs, rhs) {
            (ast::Type::Bool, ast::Type::Bool) =>
                Ok(ast::Type::Bool),
            (ast::Type::U32, ast::Type::U32) |
            (ast::Type::I32, ast::Type::I32) |
            (ast::Type::F32, ast::Type::F32) |
            (ast::Type::String, ast::Type::String) |
            (ast::Type::Date, ast::Type::Date) |
            (ast::Type::NoneLiteral, ast::Type::NoneLiteral) |
            (ast::Type::U32, ast::Type::I32) | (ast::Type::I32, ast::Type::U32) |
            (ast::Type::U32, ast::Type::F32) | (ast::Type::F32, ast::Type::U32) |
            (ast::Type::I32, ast::Type::F32) | (ast::Type::F32, ast::Type::I32) |
            (ast::Type::Bool, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::U32) |
            (ast::Type::I32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::I32) |
            (ast::Type::F32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::F32) |
            (ast::Type::String, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::String) |
            (ast::Type::Date, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::Date) |
            (ast::Type::Bool, ast::Type::U32) | (ast::Type::U32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::I32) | (ast::Type::I32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::F32) | (ast::Type::F32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::String) | (ast::Type::String, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::Date) | (ast::Type::Date, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::String) | (ast::Type::String, ast::Type::U32) |
            (ast::Type::U32, ast::Type::Date) | (ast::Type::Date, ast::Type::U32) |
            (ast::Type::I32, ast::Type::String) | (ast::Type::String, ast::Type::I32) |
            (ast::Type::I32, ast::Type::Date) | (ast::Type::Date, ast::Type::I32) |
            (ast::Type::F32, ast::Type::String) | (ast::Type::String, ast::Type::F32) |
            (ast::Type::F32, ast::Type::Date) | (ast::Type::Date, ast::Type::F32) |
            (ast::Type::String, ast::Type::Date) | (ast::Type::Date, ast::Type::String) =>
                Err(TypeError{what: "types are not boolean".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs, rhs], hint: None}),
        },
        ast::Opcode::EQ | ast::Opcode::NE => match (lhs, rhs) {
            (ast::Type::Bool, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::U32) |
            (ast::Type::I32, ast::Type::I32) |
            (ast::Type::F32, ast::Type::F32) |
            (ast::Type::String, ast::Type::String) |
            (ast::Type::Date, ast::Type::Date) |
            (ast::Type::NoneLiteral, ast::Type::NoneLiteral) |
            (ast::Type::U32, ast::Type::I32) | (ast::Type::I32, ast::Type::U32) |
            (ast::Type::U32, ast::Type::F32) | (ast::Type::F32, ast::Type::U32) |
            (ast::Type::I32, ast::Type::F32) | (ast::Type::F32, ast::Type::I32) |
            (ast::Type::Bool, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::U32) |
            (ast::Type::I32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::I32) |
            (ast::Type::F32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::F32) |
            (ast::Type::String, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::String) |
            (ast::Type::Date, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::Date) =>
                Ok(ast::Type::Bool),
            (ast::Type::Bool, ast::Type::U32) | (ast::Type::U32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::I32) | (ast::Type::I32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::F32) | (ast::Type::F32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::String) | (ast::Type::String, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::Date) | (ast::Type::Date, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::String) | (ast::Type::String, ast::Type::U32) |
            (ast::Type::U32, ast::Type::Date) | (ast::Type::Date, ast::Type::U32) |
            (ast::Type::I32, ast::Type::String) | (ast::Type::String, ast::Type::I32) |
            (ast::Type::I32, ast::Type::Date) | (ast::Type::Date, ast::Type::I32) |
            (ast::Type::F32, ast::Type::String) | (ast::Type::String, ast::Type::F32) |
            (ast::Type::F32, ast::Type::Date) | (ast::Type::Date, ast::Type::F32) |
            (ast::Type::String, ast::Type::Date) | (ast::Type::Date, ast::Type::String) =>
                Err(TypeError{what: "incompatible types for comparison".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs, rhs], hint: None}),
        },
        ast::Opcode::GT | ast::Opcode::LT | ast::Opcode::GE | ast::Opcode::LE => match(lhs, rhs) {
            (ast::Type::U32, ast::Type::U32) |
            (ast::Type::I32, ast::Type::I32) |
            (ast::Type::F32, ast::Type::F32) |
            (ast::Type::String, ast::Type::String) |
            (ast::Type::Date, ast::Type::Date) |
            (ast::Type::U32, ast::Type::I32) | (ast::Type::I32, ast::Type::U32) |
            (ast::Type::U32, ast::Type::F32) | (ast::Type::F32, ast::Type::U32) |
            (ast::Type::I32, ast::Type::F32) | (ast::Type::F32, ast::Type::I32) =>
                Ok(ast::Type::Bool),
            (ast::Type::Bool, ast::Type::Bool) |
            (ast::Type::NoneLiteral, ast::Type::NoneLiteral) |
            (ast::Type::Bool, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::U32) |
            (ast::Type::I32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::I32) |
            (ast::Type::F32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::F32) |
            (ast::Type::String, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::String) |
            (ast::Type::Date, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::Date) |
            (ast::Type::Bool, ast::Type::U32) | (ast::Type::U32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::I32) | (ast::Type::I32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::F32) | (ast::Type::F32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::String) | (ast::Type::String, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::Date) | (ast::Type::Date, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::String) | (ast::Type::String, ast::Type::U32) |
            (ast::Type::U32, ast::Type::Date) | (ast::Type::Date, ast::Type::U32) |
            (ast::Type::I32, ast::Type::String) | (ast::Type::String, ast::Type::I32) |
            (ast::Type::I32, ast::Type::Date) | (ast::Type::Date, ast::Type::I32) |
            (ast::Type::F32, ast::Type::String) | (ast::Type::String, ast::Type::F32) |
            (ast::Type::F32, ast::Type::Date) | (ast::Type::Date, ast::Type::F32) |
            (ast::Type::String, ast::Type::Date) | (ast::Type::Date, ast::Type::String) =>
                Err(TypeError{what: "incompatible types for ordering".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs, rhs], hint: None}),
        },
        ast::Opcode::PLUS => match(lhs, rhs) {
            (ast::Type::U32, ast::Type::U32) =>
                Ok(ast::Type::U32),
            (ast::Type::I32, ast::Type::I32) |
            (ast::Type::I32, ast::Type::U32) | (ast::Type::U32, ast::Type::I32) =>
                Ok(ast::Type::I32),
            (ast::Type::F32, ast::Type::F32) |
            (ast::Type::F32, ast::Type::U32) | (ast::Type::U32, ast::Type::F32) |
            (ast::Type::F32, ast::Type::I32) | (ast::Type::I32, ast::Type::F32) =>
                Ok(ast::Type::F32),
            (ast::Type::String, ast::Type::String) =>
                Ok(ast::Type::String),
            (ast::Type::Bool, ast::Type::Bool) |
            (ast::Type::Date, ast::Type::Date) |
            (ast::Type::NoneLiteral, ast::Type::NoneLiteral) |
            (ast::Type::Bool, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::U32) |
            (ast::Type::I32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::I32) |
            (ast::Type::F32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::F32) |
            (ast::Type::String, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::String) |
            (ast::Type::Date, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::Date) |
            (ast::Type::Bool, ast::Type::U32) | (ast::Type::U32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::I32) | (ast::Type::I32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::F32) | (ast::Type::F32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::String) | (ast::Type::String, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::Date) | (ast::Type::Date, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::String) | (ast::Type::String, ast::Type::U32) |
            (ast::Type::U32, ast::Type::Date) | (ast::Type::Date, ast::Type::U32) |
            (ast::Type::I32, ast::Type::String) | (ast::Type::String, ast::Type::I32) |
            (ast::Type::I32, ast::Type::Date) | (ast::Type::Date, ast::Type::I32) |
            (ast::Type::F32, ast::Type::String) | (ast::Type::String, ast::Type::F32) |
            (ast::Type::F32, ast::Type::Date) | (ast::Type::Date, ast::Type::F32) |
            (ast::Type::String, ast::Type::Date) | (ast::Type::Date, ast::Type::String) =>
                Err(TypeError{what: "incompatible types for addition".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs, rhs], hint: None}),
        },
        ast::Opcode::MINUS | ast::Opcode::MUL | ast::Opcode::DIV => match(lhs, rhs) {
            (ast::Type::U32, ast::Type::U32) =>
                Ok(ast::Type::U32),
            (ast::Type::I32, ast::Type::I32) |
            (ast::Type::I32, ast::Type::U32) | (ast::Type::U32, ast::Type::I32) =>
                Ok(ast::Type::I32),
            (ast::Type::F32, ast::Type::F32) |
            (ast::Type::F32, ast::Type::U32) | (ast::Type::U32, ast::Type::F32) |
            (ast::Type::F32, ast::Type::I32) | (ast::Type::I32, ast::Type::F32) =>
                Ok(ast::Type::F32),
            (ast::Type::Bool, ast::Type::Bool) |
            (ast::Type::String, ast::Type::String) |
            (ast::Type::Date, ast::Type::Date) |
            (ast::Type::NoneLiteral, ast::Type::NoneLiteral) |
            (ast::Type::Bool, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::U32) |
            (ast::Type::I32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::I32) |
            (ast::Type::F32, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::F32) |
            (ast::Type::String, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::String) |
            (ast::Type::Date, ast::Type::NoneLiteral) | (ast::Type::NoneLiteral, ast::Type::Date) |
            (ast::Type::Bool, ast::Type::U32) | (ast::Type::U32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::I32) | (ast::Type::I32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::F32) | (ast::Type::F32, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::String) | (ast::Type::String, ast::Type::Bool) |
            (ast::Type::Bool, ast::Type::Date) | (ast::Type::Date, ast::Type::Bool) |
            (ast::Type::U32, ast::Type::String) | (ast::Type::String, ast::Type::U32) |
            (ast::Type::U32, ast::Type::Date) | (ast::Type::Date, ast::Type::U32) |
            (ast::Type::I32, ast::Type::String) | (ast::Type::String, ast::Type::I32) |
            (ast::Type::I32, ast::Type::Date) | (ast::Type::Date, ast::Type::I32) |
            (ast::Type::F32, ast::Type::String) | (ast::Type::String, ast::Type::F32) |
            (ast::Type::F32, ast::Type::Date) | (ast::Type::Date, ast::Type::F32) |
            (ast::Type::String, ast::Type::Date) | (ast::Type::Date, ast::Type::String) =>
                Err(TypeError{what: "types are not numeric".into(), expr: ast::ExprKind::BinaryOp(opcode), kinds: vec![lhs, rhs], hint: None}),
        },
        ast::Opcode::NOT => panic!("Called evaluate_type_of_binary_operation() with unary operator NOT \"!\"."),
    }
}

// Try to infer the type of an operand from the operator and optionally the value produced by the operation.  Returns
// `Some(ast::Type)` if successful, `None` if the type could not be inferred.
// If unsuccessful for a binary operation, try calling
// `infer_type_from_binary_operation()` with the type of the other operand.
fn infer_type_from_operator(opcode: ast::Opcode, value: Option<ast::Type>) -> Option<ast::Type> {
    match opcode {
        // The unary NOT `!` operator only takes a boolean operand.
        ast::Opcode::NOT => Some(ast::Type::Bool),
        // Lazy boolean operators only take boolean operands.
        ast::Opcode::OR => Some(ast::Type::Bool),
        ast::Opcode::AND => Some(ast::Type::Bool),
        ast::Opcode::PLUS => match value {
            Some(kind) => match kind {
                ast::Type::U32 => Some(ast::Type::U32),
                ast::Type::I32 => Some(ast::Type::I32),
                ast::Type::F32 => Some(ast::Type::F32),
                ast::Type::String => Some(ast::Type::String),
                ast::Type::Bool | ast::Type::Date | ast::Type::NoneLiteral => None,
            },
            None => None,
        },
        ast::Opcode::MINUS => match value {
            Some(kind) => match kind {
                ast::Type::U32 => Some(ast::Type::U32),
                ast::Type::I32 => Some(ast::Type::I32),
                ast::Type::F32 => Some(ast::Type::F32),
                ast::Type::Bool | ast::Type::String |
                ast::Type::Date | ast::Type::NoneLiteral => None,
            },
            None => None,
        },
        ast::Opcode::MUL | ast::Opcode::DIV => match value {
            Some(kind) => match kind {
                ast::Type::U32 => Some(ast::Type::U32),
                ast::Type::I32 => Some(ast::Type::I32),
                ast::Type::F32 => Some(ast::Type::F32),
                ast::Type::Bool | ast::Type::String |
                ast::Type::Date | ast::Type::NoneLiteral => None,
            },
            None => Some(ast::Type::F32),
        },
        // We can't infer the type for any other operators without knowing the
        // type of the other operand.
        ast::Opcode::EQ => None,
        ast::Opcode::NE => None,
        ast::Opcode::GT => None,
        ast::Opcode::GE => None,
        ast::Opcode::LT => None,
        ast::Opcode::LE => None,
    }
}

// Try to infer the type of one binary operand from the operator and the type of
// the other binary operand.  Assumes all binary operations are commutative.
// Returns `Some(ast::Type)` if successful, `None` if the type could not be
// inferred.
//
// Panics if called with a unary operand.
fn infer_type_from_binary_operation(opcode: ast::Opcode, operand: ast::Type, value: Option<ast::Type>)
-> Option<ast::Type> {
    // First to infer with operator and operand.
    let kind = match opcode {
        // Both arguments to a lazy boolean operator must be bool.
        ast::Opcode::OR | ast::Opcode::AND => Some(ast::Type::Bool),
        // Fuzz arguments to comparison operators.
        ast::Opcode::EQ | ast::Opcode::NE => match operand {
            // Booleans can only be compared to other booleans.
            ast::Type::Bool => Some(ast::Type::Bool),
            // Integer types will probably be compared to i32.
            ast::Type::U32 | ast::Type::I32 => Some(ast::Type::I32),
            // Floats will probably be compared to other floats.
            ast::Type::F32 => Some(ast::Type::F32),
            // Strings can only be compared to strings.
            ast::Type::String => Some(ast::Type::String),
            // Dates can only be compared to dates.
            ast::Type::Date => Some(ast::Type::Date),
            // Can compare any type to a None literal, so can't infer type.
            ast::Type::NoneLiteral => None,
        },
        // Relative ordering operations.
        ast::Opcode::GT | ast::Opcode::LT |
        ast::Opcode::GE | ast::Opcode::LE => match operand {
            // Integers will probably be ordered against other integers.
            ast::Type::U32 | ast::Type::I32 => Some(ast::Type::I32),
            // Floats will probably be ordered against other floats.
            ast::Type::F32 => Some(ast::Type::F32),
            // Strings can only be ordered against strings.
            ast::Type::String => Some(ast::Type::String),
            // Dates can only be ordered against dates.
            ast::Type::Date => Some(ast::Type::Date),
            // Booleans can't be ordered against other booleans, and
            // can't order against a None literal, so can't infer type.
            ast::Type::Bool | ast::Type::NoneLiteral => None,
        },
        // We can use PLUS `+` on strings but not `-`, so type inference is a
        // little different for these two operators.
        ast::Opcode::PLUS =>  match operand {
            // An integer will probably be added to another integer.
            ast::Type::U32 | ast::Type::I32 => Some(ast::Type::I32),
            // Floats will probably be added to other floats.
            ast::Type::F32 => Some(ast::Type::F32),
            // Strings can only be added to strings.
            ast::Type::String => Some(ast::Type::String),
            // Remaining types can't be added.
            ast::Type::Bool | ast::Type::Date | ast::Type::NoneLiteral => None,
        },
        // Remaining numeric operators.
        ast::Opcode::MINUS | ast::Opcode::MUL | ast::Opcode::DIV => {
            match operand {
                // An i32 will probably be added to another i32.
                ast::Type::I32 => Some(ast::Type::I32),
                // A u32 will also probably be added/promoted to an i32.
                ast::Type::U32 => Some(ast::Type::I32),
                // Floats will probably be added to other floats.
                ast::Type::F32 => Some(ast::Type::F32),
                // Can't operate on remaining types.
                ast::Type::Bool | ast::Type::String |
                ast::Type::Date | ast::Type::NoneLiteral => None,
            }
        }
        ast::Opcode::NOT => panic!("Called infer_type_from_binary_operation() on a unary operator."),
    };

    match kind {
        Some(kind) => Some(kind),
        None => infer_type_from_operator(opcode, value),
    }
}
