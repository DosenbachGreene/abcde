use super::ast;

/// Two rows in data with the same subjectkey.
#[derive(Debug)]
pub struct DuplicateError {
    pub subjectkey: String,
}
impl std::fmt::Display for DuplicateError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Duplicate row for subjectkey {}.  Hint: Write \"select .subjectkey .interview_date\" to allow rows with the same subjectkey but a different interview_date.", self.subjectkey) 
    }
}
impl std::error::Error for DuplicateError { }

/// Invalid query.
#[derive(Debug)]
pub struct QueryError {
    pub what: String,
    pub file: Option<String>,
    pub field: Option<String>,
    pub hint: Option<String>,
}
impl std::fmt::Display for QueryError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Error for query")?;
        if self.file.is_some() || self.field.is_some() {
            write!(f, " ")?;
        }
        if let Some(file) = &self.file {
            if self.field.is_some() {
                write!(f, "{}", file)?;
            }
            else {
                write!(f, "of file {}", file)?;
            }
        }
        if let Some(field) = &self.field {
            write!(f, ".{}", field)?;
        }
        write!(f, ": {}", self.what)?;
        if let Some(hint) = &self.hint {
            write!(f, " Hint: {}", hint)?;
        }
        Ok(())
    }
}
impl std::error::Error for QueryError { }

/// Select statement error.
#[derive(Debug)]
pub struct SelectError {
    pub what: String,
}
impl std::fmt::Display for SelectError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.what)
    }
}
impl std::error::Error for SelectError { }

/// Error evaluating a type.
#[derive(Debug)]
pub struct TypeError {
    /// Description of why the error occured.
    pub what: String,
    /// The expression that could not be evaluated
    pub expr: ast::ExprKind,
    /// Vector of types involved.
    /// The type that caused the error should be listed first.
    pub kinds: Vec<ast::Type>,
    /// Give the user a hint on how to remedy the problem.
    pub hint: Option<String>,
}
impl std::fmt::Display for TypeError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f,"Could not evaluate type of {}", self.expr)?;
        if let Some(kind) = self.kinds.get(0) {
            write!(f, " with argument type(s) {}", kind)?;
            for kind in &self.kinds[1..] {
                write!(f, ", {}", kind)?;
            }
        }
        write!(f, " because: {}", self.what)?;
        if let Some(ref hint) = self.hint {
            write!(f, " Hint: {}", hint)?;
        }
        Ok(())
    }
}
impl std::error::Error for TypeError { }

/// Enum containing errors that might be encountered by the interpreter.
#[derive(Debug)]
pub enum InterpreterError {
    /// Error parsing the AST.
    ParseError(ast::ParseError),
    
    /// Error involving types.
    TypeError(TypeError),
    
    /// Invalid query.
    QueryError(QueryError),
    
    /// Select error.
    SelectError(SelectError),
    
    // Input/output error.
    IOError(std::io::Error),
    
    /// Duplicate rows with same subjectkey and no interview_date.
    DuplicateError(DuplicateError),
}
// Implement From trait to convert from each error type into the enum.
impl From<ast::ParseError> for InterpreterError {
    fn from(error: ast::ParseError) -> Self {
        Self::ParseError(error)
    }
}
impl From<TypeError> for InterpreterError {
    fn from(error: TypeError) -> Self {
        Self::TypeError(error)
    }
}
impl From<QueryError> for InterpreterError {
    fn from(error: QueryError) -> Self {
        Self::QueryError(error)
    }
}
impl From<SelectError> for InterpreterError {
    fn from(error: SelectError) -> Self {
        Self::SelectError(error)
    }
}
impl From<std::io::Error> for InterpreterError{
    fn from(error: std::io::Error) -> Self {
        Self::IOError(error)
    }
}
impl From<DuplicateError> for InterpreterError {
    fn from(error: DuplicateError) -> Self {
        Self::DuplicateError(error)
    } 
}
// Implement the Error trait on the enum itself.
impl std::fmt::Display for InterpreterError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::ParseError(e) => write!(f, "Parsing error: {}", e),
            Self::TypeError(e) => write!(f, "Type error: {}", e),
            Self::QueryError(e) => write!(f, "Invalid query: {}", e),
            Self::SelectError(e) => write!(f, "Invalid select statement: {}", e),
            Self::IOError(e) => write!(f, "IO error of kind {:?}: {}", e.kind(), e),
            Self::DuplicateError(e) => write!(f, "Invalid select statement: {}", e),
        }?;
        if let Some(e) = <Self as std::error::Error>::source(self) {
            write!(f, "\nBecause: {}", e)?;
        }
        Ok(())
    }
}
impl std::error::Error for InterpreterError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match &self {
            Self::ParseError(e) => e.source(),
            Self::TypeError(e) => e.source(),
            Self::QueryError(e) => e.source(),
            Self::SelectError(e) => e.source(),
            Self::IOError(e) => e.source(),
            Self::DuplicateError(e) => e.source(),
        }
    }
}
