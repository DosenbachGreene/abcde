use std::io::Read;
use crate::lang::ast::IntoCode;
use super::*;

/// Structure representing code in which all macros and include statements have
/// been expanded into literal code.
pub struct ExpandedCode {
    /// Abstract syntax tree of code *after* expansion
    pub(super) code: ast::Code,
}

impl ExpandedCode {
    /// Create a new instance of ExpandedCode from an abstract syntax tree.
    pub fn new<Code>(code: Code) -> Result<ExpandedCode, InterpreterError>
    where Code: ast::IntoCode {
        Ok(ExpandedCode{code: expand(code.into_code()?)?})
    }
    
    /// Inspect the expanded abstract syntax tree (AST).
    pub fn code(&self) -> &ast::Code {
        &self.code
    }
}

// Recursively search an AST for include statements, parse the included file as
// code, and insert it into the parent abstract syntax tree.
fn expand(mut code: ast::Code) -> Result<ast::Code, InterpreterError> {
    let mut i = 0;
    while i < code.len() {
        if let ast::Stmt::Include(ref file) = code[i] {
            // Read code from included file.
            let mut included_code = String::new();
            let included_code = {
                let mut f = std::fs::File::open(file)?;
                f.read_to_string(&mut included_code)?;
                &included_code
            };
            
            // Parse and recusrively expand code into
            // ast::Code abstract syntax tree.
            let included_code = expand(included_code.into_code()?)?;
            
            // Allocate space for leaf AST in parent AST.
            code.reserve(included_code.len());
            
            // Insert leaf AST into parent AST.
            let mut included_code = included_code.into_iter();
            if let Some(stmt) = included_code.next() {
                code[i] = stmt;
                for stmt in included_code {
                    i = i + 1;
                    code.insert(i, stmt);
                }
            }
            
        }
        else { i = i + 1; }
    }
    Ok(code)
}

/// Trait implemented by objects that we can
/// attempt to converted into expanded code.
pub trait IntoExpandedCode {
    /// Convert argument into ValidCode struct.
    fn into_expanded_code(self) -> Result<ExpandedCode, InterpreterError>;
}
impl IntoExpandedCode for ExpandedCode {
    fn into_expanded_code(self) -> Result<ExpandedCode, InterpreterError> {
        Ok(self)
    }
}
impl<T: ast::IntoCode> IntoExpandedCode for T {
    fn into_expanded_code(self) -> Result<ExpandedCode, InterpreterError> {
        ExpandedCode::new(self)
    }
}
