// Language module
pub mod lang;

// Explorer module
pub mod explorer;

/// BoxError type, see https://github.com/rust-lang/rfcs/pull/2820
pub type BoxError = std::boxed::Box<dyn
	std::error::Error   // must implement Error to satisfy ?
	+ std::marker::Send // needed for threads
	+ std::marker::Sync // needed for threads
>;
