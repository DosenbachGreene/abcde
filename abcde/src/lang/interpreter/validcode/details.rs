// Private iImplementation details for ValidCode.

use super::*;

// Private methods of ValidCode.
impl ValidCode {
    // Main validation method, which calls the other methods below.
    // Initializes internal items with type information if successful.
    // Returns an error if the code is not valid.
    pub(super) fn validate(&mut self) -> Result<(), InterpreterError> {
        //// At this point the code has been parsed into an abstract syntax
        //// tree (AST).  Before we begin evaluating the AST we need to figure
        //// figure out the types of all the symbols in the AST.  We also would
        //// like to perform some checks to make sure that the types are valid
        //// (e.g. we aren't trying to add a bool to a u32) before we actually
        //// begin evaluation.
        ////
        //// How we keep track of query types and variable types is
        //// fundamentally different.
        //// - Queries can only have one type throughout the entire code,
        ////   whereas variables can be shadowed by subsequent let statements
        ////   and therefore can have different types at different points
        ////   throughout the code.
        //// - Variable types are inferred from the right hand side of the let
        ////   statement and cannot be declared explicitly.  Queries can be
        ////   typed explicitly using the "query as type" syntax, but when a
        ////   query is not typed explicitly then we will try to infer its type
        ////   from the context in which it is used (e.g. the query in
        ////   "if query { true }" must have boolean type).

        // Object for evaluating and updating types.
        let mut modify_types = types::ModifyTypes {
            queries: &mut self.queries,
            vars: &mut self.vars,
            types: &mut self.types,
        };

        //// 1st Pass ////
        //// Walk the AST and store the type of each explicitly-typed query.
        //// This is so that explicit types will take precedence over inferred
        //// types.
        ////
        //// While we are at it, count the number of select statements.
        // The last statement must be a select statement.
        if let Some(stmt) = &self.code.last() {
            if stmt.kind() != ast::StmtKind::Select {
                return Err(SelectError{what: "Last statement must be a select statement.".into()}.into());
            }
        }
        else {
            return Err(SelectError{what: "Code is empty.  Last statement must be a select statement.".into()}.into());
        }
        // Walk the AST.
        for stmt in &self.code {
            match stmt {
                ast::Stmt::Select(select_stmt) => {
                    // Increment the count of select statements.
                    self.select_count += 1;

                    // Iterate over vector of symbols X in `select X where ...`
                    // and add each quuery to the queries map and type vector.
                    for symbol in &select_stmt.symbols {
                        match symbol.name {
                            ast::SymbolName::Query(ref query) => modify_types.add_query(query, symbol.kind)?,
                            ast::SymbolName::Variable(_) => (), // ignore
                        }
                    }

                    // Make sure the user didn't select interview_date without
                    // also selecting subject_key.
                    let special_fields = ContainsSpecialFields::from_symbols(&select_stmt.symbols);
                    if !special_fields.is_allowed() {
                        return Err(SelectError{what: format!("Invalid combination of special keys in select statement: {}", &select_stmt.symbols.iter().map(|s| s.to_string()).collect::<Vec<String>>().join(", "))}.into());
                    }

                    // Walk through the expression X in `select ... where X` and
                    // add each query.
                    modify_types.visit_expr_queries_add(&select_stmt.predicate)?;
                },
                ast::Stmt::Let(let_stmt) => {
                    // Walk the the expression X in `let ... = X` and add
                    // each query.
                    modify_types.visit_expr_queries_add(&let_stmt.rhs)?;
                }
                ast::Stmt::Comment(_) => (), // ignore
                ast::Stmt::Include(_) => panic!("Validator encoutered an include statement that was not expanded."),
            }
        }
        // There must be at least one select statement.
        if self.select_count == 0 {
            return Err(SelectError{what: "Code must contain at least one select statement.".into()}.into());
        }
        // If there are no queries that is an error.
        if modify_types.queries.is_empty() {
            return Err(QueryError{
                what: "No queries were specified.".into(),
                file: None,
                field: None,
                hint: None,
            }.into());
        }
        // Add special queries .subjectkey and .interview_date to the queries
        // map and type vector because we are going to try and perform those
        // queries implicitly even if the user didn't ask for them explicitly.
        // proj.add_query(
        //     &ast::Query{
        //         file: None,
        //         field: "subjectkey".into(),
        //     },
        //     Some(ast::Type::String)
        // )?;
        // proj.add_query(
        //     &ast::Query{
        //         file: None,
        //         field: "interview_date".into(),
        //     },
        //     Some(ast::Type::Date)
        // )?; // TODO remove


        //// Intermediate Passes ////
        //// Try to infer the types for any queries that were not explicitly
        //// typed.  Iteratively walk the AST and try to evaluate the types of
        //// each expression.  Try to infer the types of queries from the
        //// expressions they are used in and surrounding types.  As we infer
        //// the types of additional queries, we may be able to evaluate the
        //// types of additional expressions that were dependent on the types
        //// of those queries.  Continue iterating until we can no longer infer
        //// any new query types.
        {
            let missing = modify_types.as_types().num_queries_missing_types();
            if missing > 0 {
                let mut missing_old = missing;
                loop {
                    // Walk AST.
                    for stmt in &self.code {
                        match stmt {
                            ast::Stmt::Select(select_stmt) => {
                                // If the predicate is a query then that query
                                // must me boolean.  Otherwise walk the AST of
                                // the predicate.
                                if let ast::Expr::Symbol(ref symbol) = select_stmt.predicate.as_ref() {
                                    if let ast::SymbolName::Query(ref query) = symbol.name {
                                        modify_types.update_query_propose_type(query, Some(ast::Type::Bool));
                                    }
                                    else { modify_types.visit_expr_queries_update(&select_stmt.predicate, Some(ast::Type::Bool)); }
                                }
                                else { modify_types.visit_expr_queries_update(&select_stmt.predicate, Some(ast::Type::Bool)); }
                            },
                            ast::Stmt::Let(let_stmt) => {
                                // Walk the AST of the rhs to infer query types.
                                modify_types.visit_expr_queries_update(&let_stmt.rhs, None);

                                // Try to evaluate the rhs and update variable
                                // type vector if successful.
                                if let Ok(kind) = modify_types.as_types().evaluate_expr_type(&let_stmt.rhs) {
                                    modify_types.update_vars(&let_stmt.lhs, kind);
                                }
                            },
                            ast::Stmt::Comment(_) => (), // ignore
                            ast::Stmt::Include(_) => panic!("Validator encoutered an include statement that was not expanded."),
                        }
                    }

                    // Reset the variable type vector.
                    modify_types.reset_vars();

                    // If we have inferred all the missing types,
                    // or if we cannot infer any more missing types,
                    // then break the loop.
                    let missing = modify_types.as_types().num_queries_missing_types();
                    if missing == 0 || missing == missing_old { break; }
                    else { missing_old = missing; }
                }
            }
        }
        // At this point we need to know the types of all the queries.
        // If we do not then generate an error.
        // Also need to make sure queried files exist.
        for (query, index) in &*modify_types.as_types().queries {
            if modify_types.as_types().types[*index].is_none() {
                return Err(TypeError{
                    what: format!("No type specified for query {}.", query),
                    expr: ast::ExprKind::Symbol(ast::SymbolNameKind::Query),
                    kinds: vec![],
                    hint: Some(format!("Try specifying the type using \"{} as type\".", query))
                }.into());
            }
        }

        //// Final Pass ////
        //// Now that we know the type of every query, we should be able to
        //// evaluate the type of each dependent variable in the code.  During
        //// this pass we will also verify that all types are consistent with
        //// the rules of the language.
        for stmt in &self.code {
            match stmt {
                ast::Stmt::Let(let_stmt) => {
                    let kind = modify_types.as_types().evaluate_expr_type(&let_stmt.rhs)?;
                    modify_types.update_vars(&let_stmt.lhs, kind);
                },
                ast::Stmt::Select(select_stmt) => {
                    // Make sure the predicate evaluates to true.
                    {
                        let kind = modify_types.as_types().evaluate_expr_type(&select_stmt.predicate)?;
                        if kind != ast::Type::Bool {
                            return Err(SelectError{what: format!("Predicate of select statement must be boolean but instead has conflicting type: {}", kind)}.into());
                        }
                    }

                    // Make sure the symbols in the select statement exist and have valid types.
                    for symbol in &select_stmt.symbols {
                        match symbol.name {
                            ast::SymbolName::Query(ref query) => {
                                if modify_types.as_types().query_type(query) == None {
                                    return Err(TypeError{
                                         what: format!("Type of query {} could not be inferred.", query),
                                        expr: ast::ExprKind::Symbol(ast::SymbolNameKind::Query),
                                        kinds: vec![],
                                        hint: Some(format!("Try specifying the type using \"{} as type\".", query))
                                    }.into());
                                }
                            }
                            ast::SymbolName::Variable(ref var) => {
                                // Make sure the variable was defined.
                                let index = match modify_types.as_types().vars.get(var) {
                                    Some(index) => *index,
                                    None => return Err(TypeError {
                                            what: format!("Undefined variable {}.", var),
                                            expr: ast::ExprKind::Symbol(ast::SymbolNameKind::Variable),
                                            kinds: vec![],
                                            hint: Some(format!("Try defining the variable using \"let {} = expression;\".", var))
                                        }.into()),
                                };

                                // Make sure the variable has a valid type.
                                if modify_types.as_types().types[index] == None {
                                    return Err(TypeError {
                                        what: format!("Undefined variable {}.", var),
                                        expr: ast::ExprKind::Symbol(ast::SymbolNameKind::Variable),
                                        kinds: vec![],
                                        hint: Some(format!("Try defining the variable using \"let {} = expression;\".", var))
                                    }.into());
                                }
                            }
                        }
                    }
                },
                ast::Stmt::Comment(_) => (), // ignore
                ast::Stmt::Include(_) => panic!("Validator encoutered an include statement that was not expanded."),
            }
        }

        //// Done! ////
        //// Now ready to evaluate the code.
        Ok(())
    }
}
