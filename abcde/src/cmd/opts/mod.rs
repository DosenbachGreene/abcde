//! Module for command line parsing.

// Actual parsing is done in submodule using structopt crate.
mod structopt;

/// Structure for parsing all command line options using structopt.
pub use self::structopt::Opts;

/// Errors that may occur during command line parsing.
pub use self::structopt::OptsError;

/// Enumeration over subcommands.
pub use self::structopt::OptCmd;

/// Type of json to render.
pub use self::structopt::JsonType;

/// Convenience structure combining common arguments with those for the capture
/// subcommand.
pub struct CaptureOpts<'a> {
    pub common: &'a self::structopt::CommonOpts,
    pub capture: &'a self::structopt::CaptureOpts,
}

/// Convenience structure combining common arguments with those for the explore
/// subcommand.
pub struct ExploreOpts<'a> {
    pub common: &'a self::structopt::CommonOpts,
    pub explore: &'a self::structopt::ExploreOpts,
}

/// Convenience structure combining common arguments with those for the download
/// subcommand.
pub struct DownloadOpts<'a> {
    pub common: &'a self::structopt::CommonOpts,
    pub download: &'a mut self::structopt::DownloadOpts,
}
