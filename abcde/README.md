# ABCDE

ABCDE stands for [ABCD](https://www.addictionresearch.nih.gov/abcd-study) Boolean Capture Data Explorer.  It is a command-line driven data tool designed for the ABCD dataset and written in the Rust programming language.  **You can learn more about it in the [tutorial](./tutorial/README.md).** With abcde you can explore the available data like this:

```
abcde explore --search height
```
```
Query: abcd_ant01.anthro_1_height_in
Description:
STANDING HEIGHT #1 (in inches)

Query: abcd_ant01.anthro_2_height_in
Description:
STANDING HEIGHT #2 (in inches)

Query: abcd_ant01.anthro_3_height_in
Description:
STANDING HEIGHT #3 (in inches)

Query: abcd_ant01.anthro_height_calc
Description:
Standing Height Average (inches): If three measurements were obtained, the two closest measurements will be averaged. Should the third measurement fall equally between the first two measurements, all three will be averaged.
```

And generate output tables containing the data you are interested in like this:

```
abcde capture --code '
let height = abcd_ant01.anthro_height_calc as f32;
let weight = abcd_ant01.anthro_weight_calc as f32;
let bmi = 703.0 * weight / (height * height);
select bmi where true;'
```
```
".subjectkey" ".interview_date" "bmi"
"NDAR_INVAAAAAAAA" "10/1/2018" 20.48054
"NDAR_INVBBBBBBBB" "4/22/2018" 22.022085
"NDAR_INVCCCCCCCC" "2/21/2017" 18.234287
...
```

## Installation

### Install Rust

ABCDE is written in the Rust programming language.  You will need to [install Rust](https://www.rust-lang.org/tools/install) in order to compile it.  Per-user installation is the preferred method of installation, is very easy, and typically involves running just one line of shell script.  Either follow the [installation instructions here](https://www.rust-lang.org/tools/install) or refer to the steps below for a Linux system.

1. Download and execute the Rust installer.
   
   ```
   curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
   ```
   
2. Bash is the de-facto shell on Linux.  If you use the bash shell then you are done and should skip to the next section.
   
   Washington University in St. Louis (WUSTL) users in the NIL are configured to use the C shell (csh) by default and must manually add Rust to their `PATH`.
   
   ```
   echo setenv PATH '$HOME/.cargo/bin:$PATH' >> ~/.lin.cshrc
   source ~/.lin.cshrc
   ```

### Download ABCDE

Navigate to the directory you like to keep your source code in and then clone the code using [git](https://git-scm.com/).  Many users choose to keep source code in the `src` directory under their home directory.

```
mkdir -p ~/src
cd src
git clone https://gitlab.com/DosenbachGreene/abcde.git
```

### Compile ABCDE

Compiling is easy with Rust's cargo tool.

```
cd ~/src/abcde
cargo build --release
```

> Tip: Cargo builds a "debug" version of abcde by default.  The debug version is useful for troubleshooting, but the "release" version is faster.  Most users will want to compile with the `--release` flag.

### Install ABCDE

After compiling abcde the binary executable will be located at:

```
~/src/abcde/target/release/abcde
```

Just as many users keep their source code in `~/src`, it is commonplace to keep binaries under `~/bin`.  Create the directory and add it to your path if you have not done so previously.

* Bash shell (most Linux users):
  
  ```
  mkdir -p ~/bin
  echo 'export PATH=$HOME/bin:$PATH' >> ~/.profile
  source ~/.profile
  ```
  
* C shell (WUSTL NIL users):
  
  ```
  mkdir -p ~/bin
  echo 'setenv PATH $HOME/bin:$PATH' >> ~/.lin.cshrc
  source ~/.lin.cshrc
  ```

Once you have configured a binary directory, either install a copy of the abcde binary there or create a symlink.  Creating a symlink is recommended.

```
ln -s $HOME/src/abcde/target/release/abcde ~/bin/abcde
```

### Updating

If you have followed the steps above then you can update to the very latest version of abcde at any time.

```
cd ~/src/abcde
git pull
cargo build --release
```

## Getting the Data

To use abcde you will need access to the ABCD non-imaging data.  You will need permission to access the ABCD data through the [Data Use Agreement](https://nda.nih.gov/user/dashboard/data_permissions.html).  Once you have permission your options are to:

* Ask your system administrator if you already have access to the data on an institutional server.  Data users at Washington University in St. Louis who are part of the NIL can find the data under `/data/Daenerys/ABCD/data/behavioral_data/`.
	- The latest ABCD 3.0 release (with longitudinal data) is in `/data/Daenerys/ABCD/data/behavioral_data/3.0`
	- The older ABCD 2.0 release is in `/data/Daenerys/ABCD/data/behavioral_data/2.0`
* Make a directory in which to store the data (in this example, `data`) and use the download subcommand of abcde.  You will be prompted for your NDA user name and password.
  
  ```
  mkdir data
  abcde -v -d data download
  ```
  
* Manually download the ABCDstudyNDA package from the [NDA](https://nda.nih.gov/user/dashboard/packages.html?dataset=Adolescent+Brain+Cognitive+Development+Study+%28ABCD%29&type=shared_packages) (NIMH Data Archive).  You will also need to crate a `dictionary` subdirectory and manually downlad the individual dictionary files into it using the [Data Dictionary API](https://nda.nih.gov/api/datadictionary/).

Once you have access to the data, you will find it convenient to set the `ABCDE_DATA_DIR` environment variable.  Once you set this environment variable you will no longer have to specify the location of the data each time with `-d` or `--data-dir`.

* Bash shell (most Linux users):
  
  ```
  echo 'export ABCDE_DATA_DIR=/data/Daenerys/ABCD/data/behavioral_data/3.0' >> ~/.profile
  source ~/.profile
  ```
  
* C shell (WUSTL NIL users):
  
  ```
  echo 'setenv ABCDE_DATA_DIR /data/Daenerys/ABCD/data/behavioral_data/3.0' >> ~/.lin.cshrc
  source ~/.lin.cshrc
  ```

## Getting Help

ABCDE is distributed with a [tutorial](./tutorial/README.md), which you can work through even if you don't have access to the ABCD non-imaging data yet.

As a quick reference, you can also use the abcde help subcommand.

```
abcde help
```

Please be aware ABCDE is still under active development.  You may encouter bugs.  Contact the author Benjamin Kay at [benjamin.kay@wustl.edu](mailto:benjamin.kay@wustl.edu), who will be happy to help you and squash bugs.

## Also See

The data for the ABCD study are [curated by the University of California San Diego](https://abcd-workspace.ucsd.edu/NDAR/index.php).  They have a [REDCap database](https://abcd-rc.ucsd.edu/redcap/) for accessing the data, an interactive [data dictionary](https://abcd-workspace.ucsd.edu/NDAR/), and an excellent web-based tool for data exploration called [DEAP](https://deap.nimhda.org/) that is worth checking out!

## Credits

Many thanks to collaborators in the Greene and [Dosenbach](https://dosenbachlab.wustl.edu/) labs for helping to debug this software.

Support was provided in part by an NIH grant: NINDS 5R25NS090978-05.